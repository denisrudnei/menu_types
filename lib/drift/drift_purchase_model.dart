import 'package:drift/drift.dart';

@DataClassName('purchase_model')
class DriftPurchaseModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get createdAt =>
      dateTime().clientDefault(() => DateTime.now().toLocal())();
  RealColumn get totalAmount => real().withDefault(const Constant(0))();
  RealColumn get totalPrice => real().withDefault(const Constant(0))();
  TextColumn get type => text().withDefault(const Constant('NORMAL'))();
  IntColumn get userId => integer().nullable()();
  IntColumn get operatorId => integer()();
  TextColumn get status => text().withDefault(const Constant('REQUIRED'))();
  TextColumn get origin => text().withDefault(const Constant('POS'))();
  TextColumn get posId => text()();
}
