import 'package:drift/drift.dart';

@DataClassName('product_model')
class DriftProductModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
  TextColumn get description => text().withDefault(const Constant(''))();
  TextColumn get primaryImage => text().withDefault(const Constant(''))();
  RealColumn get price => real().withDefault(const Constant(0))();
  RealColumn get amount => real().withDefault(const Constant(0))();
  TextColumn get barcode => text().withDefault(const Constant(''))();
  DateTimeColumn get deletedAt => dateTime().nullable()();
}
