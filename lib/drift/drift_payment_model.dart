import 'package:drift/drift.dart';

@DataClassName('payment_model')
class DriftPaymentModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get type => text().withDefault(const Constant('MONEY'))();
  RealColumn get value => real()();
  RealColumn get paid => real()();
  RealColumn get change => real()();
  IntColumn get purchaseId => integer()();
}
