import 'package:drift/drift.dart';

@DataClassName('phone_model')
class DriftPhoneModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get number => text()();
  TextColumn get description => text()();
  IntColumn get userId => integer()();
}
