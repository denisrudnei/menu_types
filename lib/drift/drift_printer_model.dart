import 'package:drift/drift.dart';

class DriftPrinterModel extends Table {
  TextColumn get id => text().customConstraint('UNIQUE')();
  TextColumn get name => text()();
  TextColumn get manufacturer => text()();
  TextColumn get model => text()();
  TextColumn get type => text()();
  TextColumn get path => text()();
  TextColumn get installedInId => text()();
  DateTimeColumn get createdAt => dateTime()();
  DateTimeColumn get updatedAt => dateTime()();
  DateTimeColumn get deletedAt => dateTime().nullable()();
}
