import 'package:drift/drift.dart';

@DataClassName('history_product_model')
class DriftHistoryProductModel extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get productId => integer()();
  IntColumn get purchaseId => integer()();
  TextColumn get dataName => text()();
  TextColumn get dataBarcode => text()();
  TextColumn get dataDescription => text()();
  RealColumn get dataAmount => real()();
  RealColumn get dataPrice => real()();
}
