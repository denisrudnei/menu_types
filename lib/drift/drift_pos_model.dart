import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

class DriftPOSModel extends Table {
  var uuid = const Uuid();
  TextColumn get id =>
      text().customConstraint('UNIQUE').clientDefault(() => uuid.v4())();
  TextColumn get name => text()();
  TextColumn get hostname => text().nullable()();
  DateTimeColumn get createdAt => dateTime()();
  DateTimeColumn get updatedAt => dateTime()();
  DateTimeColumn get deletedAt => dateTime().nullable()();
}
