enum LoadLogType { INFO, WARNING, ERROR }

extension ParseToString on LoadLogType {
  String toShortString() {
    return toString().split('.').last;
  }
}
