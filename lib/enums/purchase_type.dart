enum PurchaseType { DELIVERY, NORMAL, RESTAURANT_ORDER }

extension ParseToString on PurchaseType {
  String toShortString() {
    return toString().split('.').last;
  }
}
