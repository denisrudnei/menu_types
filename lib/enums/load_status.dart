import 'package:flutter/material.dart';

enum LoadStatus { loading, idle, loaded, error }

extension ParseToString on LoadStatus {
  String toShortString() {
    return toString().split('.').last;
  }
}

Color getColor(LoadStatus status) {
  switch (status) {
    case LoadStatus.error:
      return Colors.red;
    case LoadStatus.idle:
      return Colors.white;
    case LoadStatus.loaded:
      return Colors.green;
    case LoadStatus.loading:
      return Colors.yellow;
  }
}

IconData getConnectionStatusIcon(LoadStatus status) {
  switch (status) {
    case LoadStatus.error:
      return Icons.error_outline;
    case LoadStatus.loading:
      return Icons.downloading_outlined;
    case LoadStatus.idle:
      return Icons.done;
    case LoadStatus.loaded:
      return Icons.download_done_outlined;
  }
}
