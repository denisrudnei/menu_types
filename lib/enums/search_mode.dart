enum SearchMode { inPurchase, inTable }

extension ParseToString on SearchMode {
  String toShortString() {
    return toString().split('.').last;
  }
}
