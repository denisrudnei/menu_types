enum PurchaseOrigin { ECOMMERCE, POS, ESTABLISHMENT_TABLE }

extension ParseToString on PurchaseOrigin {
  String toShortString() {
    return toString().split('.').last;
  }
}
