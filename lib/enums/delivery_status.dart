enum DeliveryStatus {
  REQUIRED,
  IN_PRODUCTION,
  DELIVERY_PROCESS,
  DELIVERED,
  FINISHED,
}

extension ParseToString on DeliveryStatus {
  String toShortString() {
    return toString().split('.').last;
  }
}
