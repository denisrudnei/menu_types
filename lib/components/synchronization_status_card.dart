import 'package:flutter/material.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/util/format_utils.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:provider/provider.dart';

class SynchronizationStatusCard extends StatefulWidget {
  const SynchronizationStatusCard({
    Key? key,
    required this.service,
    this.color = Colors.white,
  }) : super(key: key);

  final Color? color;
  final LoadServiceInterface service;

  @override
  _SynchronizationStatusCardState createState() =>
      _SynchronizationStatusCardState();
}

class _SynchronizationStatusCardState extends State<SynchronizationStatusCard> {
  @override
  Widget build(BuildContext context) {
    var loadModel = context.read<LoadModel>();
    String defaultText = loadModel.lastSuccessfulUpdate == null
        ? 'Nunca realizado'
        : 'Última carga em ${getDateAndHour(loadModel.lastSuccessfulUpdate!)}';
    return Container(
      decoration: BoxDecoration(
        color: widget.color ?? Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Icon(
              widget.service.icon.icon,
              size: 56,
              color: widget.service.icon.color,
            ),
          ),
          if (widget.service.isDownloading)
            Expanded(
              child: RepaintBoundary(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Center(
                    child: LinearProgressIndicator(
                      value: widget.service.percentage / 100,
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          const Divider(),
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      widget.service.text.isEmpty
                          ? defaultText
                          : widget.service.text,
                    ),
                    if (widget.service.deletingText.isNotEmpty) ...[
                      const SizedBox(height: 5),
                      Text(widget.service.deletingText),
                    ]
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
