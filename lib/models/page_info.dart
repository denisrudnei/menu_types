class PageInfo {
  int page;
  int pages;
  PageInfo({
    required this.page,
    required this.pages,
  });

  factory PageInfo.fromMap(Map<String, dynamic> map) {
    int page = int.parse(map['page'].toString());
    int pages = int.parse(map['pages'].toString());
    return PageInfo(
      page: page <= pages ? page : 1,
      pages: pages > 0 ? pages : 1,
    );
  }
}
