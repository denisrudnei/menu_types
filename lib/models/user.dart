import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/models/address.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/util/my_database.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  int? id;
  String name;
  String email;
  String image;
  bool active = false;
  late String password;
  String tempPassword = '';

  DateTime updatedAt = DateTime.now();
  DateTime? deletedAt;
  List<Phone> phones = [];
  List<Address> addresses = [];

  User({
    required this.name,
    required this.email,
    required this.image,
    this.password = '',
  }) : super();

  factory User.fromMap(Map<String, dynamic> map) {
    User user = User(
      name: map['name'],
      email: map['email'],
      image: map['image'],
    );
    user.id = int.parse(map['id'].toString());
    return user;
  }

  factory User.fromDrift(user_model user) {
    return User(
      name: user.name,
      email: user.email,
      image: user.image,
    )..id = user.id;
  }

  factory User.fromPostgres(Map<String, Map<String, dynamic>> result) {
    return User(
      email: result['user']!['email'],
      name: result['user']!['name'],
      image: result['user']!['image'] ?? '',
      password: result['user']!['password'],
    )..id = result['user']!['id'];
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
