// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) => Address(
      id: json['id'] as String,
      zipCode: json['zipCode'] as String,
      country: json['country'] as String,
      city: json['city'] as String,
      street: json['street'] as String,
      number: json['number'] as String,
      state: json['state'] as String,
      fullName: json['fullName'] as String,
    );

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'id': instance.id,
      'zipCode': instance.zipCode,
      'country': instance.country,
      'city': instance.city,
      'street': instance.street,
      'number': instance.number,
      'state': instance.state,
      'fullName': instance.fullName,
    };
