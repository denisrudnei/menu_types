import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/enums/product_type.dart';
import 'package:menu_types/util/my_database.dart';

part 'product.g.dart';

@JsonSerializable()
class Product {
  Product({
    required this.id,
    required this.name,
    required this.price,
    required this.primaryImage,
    required this.description,
    this.amount = 0,
    this.barcode = '',
    this.deletedAt,
  }) : super();

  final int id;
  String name = "";
  double price = 0;
  String primaryImage = "";
  String description = "";
  String barcode = "";

  double amount = 0;
  double discount = 0;
  List<ProductType> type = [];
  DateTime? deletedAt;

  double get total => amount * ((price / 100) * (100 - discount));

  bool get isNegative => amount < 0;

  DateTime? imageUpdatedAt;

  factory Product.fromMap(Map<String, dynamic> map) {
    Product product = Product(
      id: int.parse(map['id'].toString()),
      name: map['name'],
      price: double.tryParse(map['price'].toString()) ?? 0,
      primaryImage: map['primaryImage'],
      description: map['description'] ?? '',
      amount: double.tryParse('amount'.toString()) ?? 0,
      barcode: map['barcode'] ?? '',
    );
    if (map['type'] != null) {
      product.type = (map['type'] as List<dynamic>)
          .map((e) => $enumDecode(_$ProductTypeEnumMap, e))
          .toList();
    }
    product.amount = double.tryParse(map['amount'].toString()) ?? 0;
    if (map['imageUpdatedAt'] != null) {
      product.imageUpdatedAt = DateTime.parse(map['imageUpdatedAt']);
    }
    return product;
  }

  getTypeAsPostgresString() {
    return '{${type.join(',')}}';
  }

  factory Product.clone(Product from) {
    Product product = Product(
      id: from.id,
      name: from.name,
      price: from.price,
      primaryImage: from.primaryImage,
      description: from.description,
      barcode: from.barcode,
    );
    return product;
  }

  factory Product.fromHistoryDrift(history_product_model product) {
    return Product(
      id: product.id,
      name: product.dataName,
      amount: product.dataAmount,
      price: product.dataPrice,
      primaryImage: '',
      description: product.dataDescription,
    );
  }

  factory Product.fromDrift(product_model product) {
    return Product(
      id: product.id,
      name: product.name,
      price: product.price,
      primaryImage: product.primaryImage,
      description: product.description,
    );
  }

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
