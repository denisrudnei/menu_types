import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_service/printable_document.dart';

abstract class PrinterServiceInterface {
  Future<void> print(Printer printer, PrintableDocument document);
}
