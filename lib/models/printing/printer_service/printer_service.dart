import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer_service/implementations/network_thermal_printer_service.dart';
import 'package:menu_types/models/printing/printer_service/implementations/thermal_printer_service.dart';
import 'package:menu_types/models/printing/printer_service/printable_document.dart';
import 'package:menu_types/models/printing/printer_service/printer_service_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/printing/print_util.dart';

class PrinterService implements PrinterServiceInterface {
  PrinterServiceInterface? implementation;
  PrintUtil util;
  PrinterModel model;

  PrinterService({
    required this.util,
    required this.model,
  });

  @override
  Future<void> print(Printer printer, PrintableDocument document) async {
    defineImplementation(printer);
    if (implementation == null) throw Exception('Not implemented');
    implementation?.print(printer, document);
  }

  void defineImplementation(Printer printer) {
    switch (printer.type) {
      case PrinterType.THERMAL:
        implementation = ThermalPrinterService(
          util: util,
          printerModel: model,
        );
        break;
      case PrinterType.NETWORK:
        implementation = NetworkThermalPrinterService(
          util: util,
          printerModel: model,
        );
        break;
      default:
        LoggerUtil.logger
            .i('Failed to load implementation for ${printer.type}');
        break;
    }
  }
}
