import 'dart:io';

import 'package:menu_types/models/printing/printer_service/printable_document.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_service/printer_service_interface.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/pdf/pdf_purchase_util.dart';

class LaserPrinterService implements PrinterServiceInterface {
  @override
  Future<void> print(Printer printer, PrintableDocument document) async {
    List<int> bytes = [];
    switch (document.runtimeType) {
      case Purchase:
        bytes = await PdfPurchaseUtil.generateForPurchase(document as Purchase);
        break;
    }
    await File(printer.path).writeAsBytes(bytes);
  }
}
