import 'dart:io';

import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer_service/printable_document.dart';
import 'package:menu_types/models/printing/printer_service/printer_service_interface.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/printing/print_util.dart';

class NetworkThermalPrinterService implements PrinterServiceInterface {
  PrintUtil util;
  PrinterModel printerModel;
  NetworkThermalPrinterService({
    required this.util,
    required this.printerModel,
  });
  @override
  Future<void> print(Printer printer, PrintableDocument document) async {
    List<int> bytes = [];
    switch (document.runtimeType) {
      case Purchase:
        bytes = await util.getBytesFromPurchase(document as Purchase);
        break;
    }

    LoggerUtil.logger.i('Needs a path');

    var ipAndPort = printer.path.split(':');

    if (ipAndPort.length == 2) {
      String ip = ipAndPort[0];
      int port = int.parse(ipAndPort[1]);
      try {
        var socket = await Socket.connect(ip, port);
        socket.add(bytes);
        await socket.close();
      } catch (e) {
        LoggerUtil.logger.e(e);
        throw Exception(e);
      }
    } else {
      throw Exception('Invalid address, use the IP and Port separated by ":"');
    }
  }
}
