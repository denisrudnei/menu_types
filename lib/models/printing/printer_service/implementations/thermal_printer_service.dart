import 'dart:io';

import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer_service/printable_document.dart';
import 'package:menu_types/models/printing/printer_service/printer_service_interface.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/printing/print_util.dart';

class ThermalPrinterService implements PrinterServiceInterface {
  PrintUtil util;
  PrinterModel printerModel;
  ThermalPrinterService({
    required this.util,
    required this.printerModel,
  });
  @override
  Future<void> print(Printer printer, PrintableDocument document) async {
    List<int> bytes = [];
    switch (document.runtimeType) {
      case Purchase:
        bytes = await util.getBytesFromPurchase(document as Purchase);
        break;
    }

    await File(printer.path).writeAsBytes(bytes);
  }
}
