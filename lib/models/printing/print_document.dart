import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/enums/print_request_type.dart';
import 'package:menu_types/enums/printer_status.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:uuid/uuid.dart';

part 'print_document.g.dart';

@JsonSerializable()
class PrintDocument {
  String id = const Uuid().v4();
  late POS origin;
  late Printer printer;
  late PrintRequestType type;
  late DateTime date;
  late PrinterStatus status;

  PrintDocument({
    required this.origin,
    required this.printer,
    required this.type,
    required this.status,
    required this.date,
  });

  factory PrintDocument.fromJson(Map<String, dynamic> json) =>
      _$PrintDocumentFromJson(json);

  Map<String, dynamic> toJson() => _$PrintDocumentToJson(this);
}
