// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'print_document.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrintDocument _$PrintDocumentFromJson(Map<String, dynamic> json) =>
    PrintDocument(
      origin: POS.fromJson(json['origin'] as Map<String, dynamic>),
      printer: Printer.fromJson(json['printer'] as Map<String, dynamic>),
      type: $enumDecode(_$PrintRequestTypeEnumMap, json['type']),
      status: $enumDecode(_$PrinterStatusEnumMap, json['status']),
      date: DateTime.parse(json['date'] as String),
    )..id = json['id'] as String;

Map<String, dynamic> _$PrintDocumentToJson(PrintDocument instance) =>
    <String, dynamic>{
      'id': instance.id,
      'origin': instance.origin,
      'printer': instance.printer,
      'type': _$PrintRequestTypeEnumMap[instance.type],
      'date': instance.date.toIso8601String(),
      'status': _$PrinterStatusEnumMap[instance.status],
    };

const _$PrintRequestTypeEnumMap = {
  PrintRequestType.purchase: 'purchase',
  PrintRequestType.order: 'order',
};

const _$PrinterStatusEnumMap = {
  PrinterStatus.waitingPrint: 'waitingPrint',
  PrinterStatus.printing: 'printing',
  PrinterStatus.error: 'error',
  PrinterStatus.paused: 'paused',
};
