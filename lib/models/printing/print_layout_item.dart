import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout.dart';
import 'package:menu_types/models/purchase.dart';

class PrintLayoutItem with PrinterItem {
  final String id;

  final int position;

  PrintLayout? mainLayout;

  final String type;

  PrintLayoutItem({
    required this.id,
    required this.position,
    required this.type,
    this.mainLayout,
  });

  factory PrintLayoutItem.fromMap(Map<String, dynamic> map) {
    PrintLayoutItem printLayoutItem = PrintLayoutItem(
      id: map['id'],
      type: map['type'],
      position: int.tryParse(map['position']) ?? 1,
    );

    if (map['mainLayout'] != null) {
      printLayoutItem.mainLayout = PrintLayout.fromMap(map['mainLayout']);
    }
    return printLayoutItem;
  }
}

mixin PrinterItem {
  Future<Widget> printText(Purchase purchase) async {
    return const Text('Not implemented');
  }

  Future<List<int>> getData(Purchase purchase) async {
    return [];
  }
}
