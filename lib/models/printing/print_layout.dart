import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/print_layout/cut_item.dart';
import 'package:menu_types/models/print_layout/empty_line_item.dart';
import 'package:menu_types/models/print_layout/header_item.dart';
import 'package:menu_types/models/print_layout/line_item.dart';
import 'package:menu_types/models/print_layout/payment_info_item.dart';
import 'package:menu_types/models/print_layout/product_table_item.dart';
import 'package:menu_types/models/print_layout/purchase_information.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrintLayout extends Equatable {
  String id;
  String name;
  List<PrintLayoutItem> items = [];
  PrintLayout({required this.id, required this.name});

  factory PrintLayout.fromMap(Map<String, dynamic> map) {
    PrintLayout layout = PrintLayout(
      id: map['id'].toString(),
      name: map['name'].toString(),
    );

    if (map['items'] != null) {
      for (var item in map['items']) {
        switch (item['type']) {
          case 'HeaderItem':
            layout.items.add(HeaderItem.fromMap(item));
            break;
          case 'CutItem':
            layout.items.add(CutItem.fromMap(item));
            break;
          case 'EmptyLineItem':
            layout.items.add(EmptyLineItem.fromMap(item));
            break;
          case 'LineItem':
            layout.items.add(LineItem.fromMap(item));
            break;
          case 'ProductTable':
            layout.items.add(ProductTableItem.fromMap(item));
            break;
          case 'PaymentItem':
            layout.items.add(PaymentInfoItem.fromMap(item));
            break;
          case 'PurchaseInformation':
            layout.items.add(PurchaseInformation.fromMap(item));
            break;
          default:
            layout.items.add(
              PrintLayoutItem(
                id: item['id'],
                position: item['position'],
                type: item['type'],
              ),
            );
            break;
        }
      }
    }
    return layout;
  }

  Future<Widget> getLayout(Purchase purchase) async {
    List<Widget> content = [];
    for (var item in items) {
      var newWidget = await item.printText(purchase);
      content.add(newWidget);
    }
    return Column(
      children: content,
    );
  }

  Future<List<int>> getData(Purchase purchase) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);
    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontB);

    for (var item in items) {
      bytes += await item.getData(purchase);
    }

    bytes += generator.emptyLines(2);

    return bytes;
  }

  Future<void> print(Purchase purchase) async {
    var prefs = await SharedPreferences.getInstance();
    final path = prefs.getString('printerPath') ?? '';

    File(path).writeAsBytesSync(await getData(purchase));
  }

  @override
  List<Object?> get props => [id, name];
}
