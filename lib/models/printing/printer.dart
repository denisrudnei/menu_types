import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/enums/printer_status.dart';
import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:uuid/uuid.dart';

part 'printer.g.dart';

@JsonSerializable()
class Printer extends Equatable {
  String id = const Uuid().v4();
  late String name;
  late String manufacturer;
  late String model;
  late String path;
  late POS installedIn;
  late PrinterStatus status;
  late PrinterType type;

  Printer({
    String? id,
    required this.name,
    required this.manufacturer,
    required this.model,
    required this.path,
    required this.installedIn,
    this.type = PrinterType.THERMAL,
    this.status = PrinterStatus.waitingPrint,
  }) {
    this.id = id ?? const Uuid().v4();
  }

  copyWith({
    String? id,
    String? name,
    String? manufacturer,
    String? model,
    String? path,
    POS? installedIn,
  }) {
    return Printer(
      id: id ?? this.id,
      name: name ?? this.name,
      manufacturer: manufacturer ?? this.manufacturer,
      model: model ?? this.model,
      path: path ?? this.path,
      installedIn: installedIn ?? this.installedIn,
    );
  }

  static Future<Printer> fromDrift(
    MyDatabase db,
    DriftPrinterModelData data,
  ) async {
    return Printer(
      id: data.id,
      name: data.name,
      manufacturer: data.manufacturer,
      model: data.model,
      path: data.path,
      type:
          PrinterType.values.firstWhere((element) => element.name == data.type),
      installedIn: await POSRepositorySqlite(db: db).getPOSById(
        data.installedInId,
      ) as POS,
    );
  }

  factory Printer.fromJson(Map<String, dynamic> json) =>
      _$PrinterFromJson(json);

  Map<String, dynamic> toJson() => _$PrinterToJson(this);

  @override
  List<Object> get props => [id, name];
}
