import 'dart:async';
import 'dart:collection';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:menu_types/models/printing/print_document.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrinterModel extends ChangeNotifier {
  String path = "";
  Uint8List? data;
  bool printWhenNewOrderArrives = false;
  bool usePrinterManager = false;
  List<Printer> selectedPrinters = [];
  bool printCompanyLogo = false;
  final String _printCompanyLogo = 'printCompanyLogo';

  Printer? printerForPurchase;
  Printer? printerForDelivery;

  String printerForPurchaseId = '';
  String printerForDeliveryId = '';

  List<Printer> _printers = [];

  final Queue<PrintDocument> _documents = Queue();

  Queue<PrintDocument> get documents => _documents;

  final StreamController _printController = StreamController();

  UnmodifiableListView<Printer> get printers => UnmodifiableListView(_printers);

  Future<void> init() async {
    var _prefs = await SharedPreferences.getInstance();
    path = _prefs.getString('printerPath') ?? '';

    printerForPurchaseId = _prefs.getString('printerForPurchase') ?? '';
    printerForDeliveryId = _prefs.getString('printerForDelivery') ?? '';

    printWhenNewOrderArrives =
        _prefs.getBool('printWhenNewOrderArrives') ?? false;

    usePrinterManager = _prefs.getBool('usePrinterManager') ?? false;
  }

  setPath(String path) {
    SharedPreferences.getInstance().then((_prefs) {
      this.path = path;
      _prefs.setString('printerPath', path);
      notifyListeners();
    });
  }

  void setPrintData(Uint8List data) {
    this.data = data;
    notifyListeners();
  }

  Future<void> setPrintWhenNewOrderArrives(bool shouldPrint) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool('printWhenNewOrderArrives', shouldPrint);
    printWhenNewOrderArrives = shouldPrint;
    notifyListeners();
  }

  void setPrinters(List<Printer> printers) async {
    var prefs = await SharedPreferences.getInstance();

    printerForPurchaseId = prefs.getString('printerForPurchase') ?? '';
    printerForDeliveryId = prefs.getString('printerForDelivery') ?? '';

    _printers = printers;

    if (printerForPurchaseId.isNotEmpty &&
        _printers.map((e) => e.id).toList().contains(printerForPurchaseId)) {
      printerForPurchase =
          _printers.firstWhere((element) => element.id == printerForPurchaseId);
    }

    if (printerForDeliveryId.isNotEmpty &&
        _printers.map((e) => e.id).toList().contains(printerForDeliveryId)) {
      printerForDelivery =
          _printers.firstWhere((element) => element.id == printerForDeliveryId);
    }

    notifyListeners();
  }

  void addPrinter(Printer printer) {
    _printers.add(printer);
    notifyListeners();
  }

  void removePrinter(Printer printer) {
    _printers.remove(printer);
    notifyListeners();
  }

  void addDocument(PrintDocument document) {
    _documents.add(document);
    _printController.add(document);
    notifyListeners();
  }

  void removeDocument(PrintDocument document) {
    _documents.remove(document);
    notifyListeners();
  }

  Stream<void> process() {
    return _printController.stream;
  }

  void processFirst(AsyncCallback callback) async {
    await callback.call();
    _documents.removeFirst();
    notifyListeners();
  }

  void setPrinterForPurchase(Printer printer) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('printerForPurchase', printer.id);
    printerForPurchase = printer;
    notifyListeners();
  }

  void setPrinterForDelivery(Printer printer) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('printerForDelivery', printer.id);
    printerForDelivery = printer;
    notifyListeners();
  }

  void setUsePrinterManager(bool usePrinterManager) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool('usePrinterManager', usePrinterManager);
    this.usePrinterManager = usePrinterManager;
    notifyListeners();
  }

  void toggleSelectedPrinter(Printer printer) {
    if (selectedPrinters.contains(printer)) {
      selectedPrinters.remove(printer);
    } else {
      selectedPrinters.add(printer);
    }
    notifyListeners();
  }

  void setSelectedPrinters(List<Printer> printers) {
    selectedPrinters = printers.toList();
    notifyListeners();
  }

  void clearSelectedPrinter() {
    selectedPrinters.clear();
    notifyListeners();
  }

  Future<void> setPrintCompanyLogo(bool printCompanyLogo) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool(_printCompanyLogo, printCompanyLogo);
    this.printCompanyLogo = printCompanyLogo;
    notifyListeners();
  }
}
