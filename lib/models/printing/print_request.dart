import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/enums/print_request_type.dart';

part 'print_request.g.dart';

@JsonSerializable()
class PrintRequest {
  late PrintRequestType type;
  List<int> data;
  Printer printer;
  late POS origin;

  PrintRequest({
    required this.type,
    required this.data,
    required this.printer,
    required this.origin,
  });

  factory PrintRequest.fromJson(Map<String, dynamic> json) =>
      _$PrintRequestFromJson(json);

  Map<String, dynamic> toJson() => _$PrintRequestToJson(this);
}
