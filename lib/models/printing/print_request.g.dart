// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'print_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrintRequest _$PrintRequestFromJson(Map<String, dynamic> json) => PrintRequest(
      type: $enumDecode(_$PrintRequestTypeEnumMap, json['type']),
      data: (json['data'] as List<dynamic>).map((e) => e as int).toList(),
      printer: Printer.fromJson(json['printer'] as Map<String, dynamic>),
      origin: POS.fromJson(json['origin'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PrintRequestToJson(PrintRequest instance) =>
    <String, dynamic>{
      'type': _$PrintRequestTypeEnumMap[instance.type],
      'data': instance.data,
      'printer': instance.printer,
      'origin': instance.origin,
    };

const _$PrintRequestTypeEnumMap = {
  PrintRequestType.purchase: 'purchase',
  PrintRequestType.order: 'order',
};
