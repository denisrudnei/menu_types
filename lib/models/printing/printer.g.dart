// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'printer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Printer _$PrinterFromJson(Map<String, dynamic> json) => Printer(
      id: json['id'] as String?,
      name: json['name'] as String,
      manufacturer: json['manufacturer'] as String,
      model: json['model'] as String,
      path: json['path'] as String,
      installedIn: POS.fromJson(json['installedIn'] as Map<String, dynamic>),
      type: $enumDecodeNullable(_$PrinterTypeEnumMap, json['type']) ??
          PrinterType.THERMAL,
      status: $enumDecodeNullable(_$PrinterStatusEnumMap, json['status']) ??
          PrinterStatus.waitingPrint,
    );

Map<String, dynamic> _$PrinterToJson(Printer instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'manufacturer': instance.manufacturer,
      'model': instance.model,
      'path': instance.path,
      'installedIn': instance.installedIn,
      'status': _$PrinterStatusEnumMap[instance.status],
      'type': _$PrinterTypeEnumMap[instance.type],
    };

const _$PrinterTypeEnumMap = {
  PrinterType.THERMAL: 'THERMAL',
  PrinterType.LASER: 'LASER',
  PrinterType.LABEL: 'LABEL',
  PrinterType.NETWORK: 'NETWORK',
  PrinterType.BLUETOOTH: 'BLUETOOTH',
  PrinterType.BUILT_IN: 'BUILT_IN',
};

const _$PrinterStatusEnumMap = {
  PrinterStatus.waitingPrint: 'waitingPrint',
  PrinterStatus.printing: 'printing',
  PrinterStatus.error: 'error',
  PrinterStatus.paused: 'paused',
};
