// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) => Product(
      id: json['id'] as int,
      name: json['name'] as String,
      price: (json['price'] as num).toDouble(),
      primaryImage: json['primaryImage'] as String,
      description: json['description'] as String,
      amount: (json['amount'] as num?)?.toDouble() ?? 0,
      barcode: json['barcode'] as String? ?? '',
      deletedAt: json['deletedAt'] == null
          ? null
          : DateTime.parse(json['deletedAt'] as String),
    )
      ..discount = (json['discount'] as num).toDouble()
      ..type = (json['type'] as List<dynamic>)
          .map((e) => $enumDecode(_$ProductTypeEnumMap, e))
          .toList()
      ..imageUpdatedAt = json['imageUpdatedAt'] == null
          ? null
          : DateTime.parse(json['imageUpdatedAt'] as String);

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'price': instance.price,
      'primaryImage': instance.primaryImage,
      'description': instance.description,
      'barcode': instance.barcode,
      'amount': instance.amount,
      'discount': instance.discount,
      'type': instance.type.map((e) => _$ProductTypeEnumMap[e]).toList(),
      'deletedAt': instance.deletedAt?.toIso8601String(),
      'imageUpdatedAt': instance.imageUpdatedAt?.toIso8601String(),
    };

const _$ProductTypeEnumMap = {
  ProductType.ECOMMERCE: 'ECOMMERCE',
  ProductType.RESTAURANT: 'RESTAURANT',
  ProductType.POS: 'POS',
};
