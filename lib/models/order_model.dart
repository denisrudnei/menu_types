import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/models/purchase.dart';

class OrderModel extends ChangeNotifier {
  final List<Purchase> _purchases = [];
  List<Purchase> _selected = [];

  UnmodifiableListView<Purchase> get requested =>
      UnmodifiableListView(_purchases.where(
        (purchase) => purchase.status == DeliveryStatus.REQUIRED,
      ));

  UnmodifiableListView<Purchase> get inProduction =>
      UnmodifiableListView(_purchases.where(
        (purchase) => purchase.status == DeliveryStatus.IN_PRODUCTION,
      ));

  UnmodifiableListView<Purchase> get deliveryProcess =>
      UnmodifiableListView(_purchases.where(
        (purchase) => purchase.status == DeliveryStatus.DELIVERY_PROCESS,
      ));

  bool get showAlert => _purchases
      .where(
        (purchase) => purchase.status == DeliveryStatus.REQUIRED,
      )
      .isNotEmpty;

  UnmodifiableListView<Purchase> get selected =>
      UnmodifiableListView(_selected);

  void addPurchase(Purchase purchase) {
    if (_purchases.where((element) => element.id == purchase.id).isEmpty) {
      _purchases.add(purchase);
      notifyListeners();
    }
  }

  void updateStatus(int id, DeliveryStatus status) {
    int index = _purchases.indexWhere((element) => element.id == id);
    if (index != -1) {
      _purchases[index].status = status;
      notifyListeners();
    }
  }

  void updateStatusForOrders(List<int> ids, DeliveryStatus status) {
    for (var id in ids) {
      int index = _purchases.indexWhere((element) => element.id == id);
      if (index != -1) {
        _purchases[index].status = status;
      }
    }
    notifyListeners();
  }

  void remove(Purchase purchase) {
    _purchases.remove(purchase);
    notifyListeners();
  }

  void clear() {
    _purchases.clear();
    notifyListeners();
  }

  void setSelected(List<Purchase> selected) {
    _selected = selected.toList();
    notifyListeners();
  }

  void addSelected(Purchase purchase) {
    _selected.add(purchase);
    notifyListeners();
  }

  void removeSelected(Purchase purchase) {
    _selected.remove(purchase);
    notifyListeners();
  }

  void toggleSelected(Purchase purchase) {
    if (_selected.contains(purchase)) {
      _selected.remove(purchase);
    } else {
      _selected.add(purchase);
    }
    notifyListeners();
  }
}
