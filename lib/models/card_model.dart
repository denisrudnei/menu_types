import 'package:flutter/material.dart';

class CardModel extends ChangeNotifier {
  String selected = "";

  void setSelected(String selected) {
    this.selected = selected;
    notifyListeners();
  }
}
