import 'package:flutter/material.dart';
import 'package:menu_types/enums/search_mode.dart';

class SearchModeModel extends ChangeNotifier {
  SearchMode mode = SearchMode.inPurchase;

  void setSearchMode(SearchMode mode) {
    this.mode = mode;
    notifyListeners();
  }
}
