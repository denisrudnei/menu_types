import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/my_database.dart';

part 'payment.g.dart';

@JsonSerializable()
class Payment {
  String id;
  String type;
  double value = 0;
  double paid = 0;
  double change = 0;
  Purchase? purchase;

  Payment({
    required this.id,
    required this.type,
    this.value = 0,
    this.paid = 0,
    this.change = 0,
    this.purchase,
  });

  Payment.empty({
    this.id = '',
    this.type = 'Empty',
  });

  factory Payment.grouped(List<Payment> payments) {
    return Payment(
      id: 'grouped',
      type: 'Grouped',
      value: payments.fold<double>(
          0, (previousValue, element) => previousValue += element.value),
      paid: payments.fold<double>(
          0, (previousValue, element) => previousValue += element.paid),
      change: payments.fold<double>(
          0, (previousValue, element) => previousValue += element.change),
    );
  }

  factory Payment.fromMap(Map<String, dynamic> map) {
    return Payment(
      id: map['id'],
      type: map['type'],
      value: double.tryParse(map['value'].toString()) ?? 0,
      paid: double.tryParse(map['paid'].toString()) ?? 0,
      change: double.tryParse(map['change'].toString()) ?? 0,
    );
  }

  factory Payment.fromDrift(payment_model payment) {
    return Payment(
      id: payment.id.toString(),
      type: payment.type,
      value: payment.value,
      paid: payment.paid,
      change: payment.change,
    );
  }

  factory Payment.fromJson(Map<String, dynamic> json) =>
      _$PaymentFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentToJson(this);
}
