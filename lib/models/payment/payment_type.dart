class PaymentType {
  final String value;
  final String text;

  PaymentType({required this.value, required this.text});

  factory PaymentType.fromMap(Map<String, dynamic> map) {
    return PaymentType(
      value: map['value'],
      text: map['text'],
    );
  }
}
