import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/payment/payment_type.dart';

class PaymentTypesModel extends ChangeNotifier {
  List<PaymentType> _paymentTypes = [];

  UnmodifiableListView<PaymentType> get paymentTypes =>
      UnmodifiableListView(_paymentTypes);

  void add(PaymentType paymentType) {
    _paymentTypes.add(paymentType);
    notifyListeners();
  }

  void setAll(List<PaymentType> types) {
    _paymentTypes = types.toList();
    notifyListeners();
  }

  void remove(PaymentType paymentType) {
    _paymentTypes.remove(paymentType);
    notifyListeners();
  }

  void clear() {
    _paymentTypes.clear();
    notifyListeners();
  }
}
