import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BackgroundModel extends ChangeNotifier {
  final String _defaultBackgroundsFolder = 'defaultBackgroundsFolder';
  final String _defaultImage = 'defaultImage';
  String? defaultBackgroundsFolder = '';
  String defaultImage = '';
  List<String> images = [];

  Future<void> init() async {
    var prefs = await SharedPreferences.getInstance();
    defaultBackgroundsFolder = prefs.getString(_defaultBackgroundsFolder) ?? '';
    defaultImage = prefs.getString(_defaultImage) ?? '';
    notifyListeners();
  }

  Future<void> setBackgroundFolder(String folder) async {
    defaultBackgroundsFolder = folder;
    var prefs = await SharedPreferences.getInstance();
    await prefs.setString(_defaultBackgroundsFolder, folder);
    notifyListeners();
  }

  void setImagesPath(List<String> images) {
    this.images = images;
    notifyListeners();
  }

  void addImagePath(String image) {
    images.add(image);
    notifyListeners();
  }

  Future<void> setDefaultImage(String image) async {
    defaultImage = image;
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(_defaultImage, image);
    notifyListeners();
  }

  removeImage(String image) {
    images.remove(image);
    notifyListeners();
  }
}
