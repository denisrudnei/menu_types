import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/util/my_database.dart';

part 'phone.g.dart';

@JsonSerializable()
class Phone {
  String id = '';
  String number;
  String description;

  Phone({this.id = '', required this.number, required this.description});

  factory Phone.fromMap(Map<String, dynamic> map) {
    return Phone(
      id: map['id'],
      description: map['description'],
      number: map['number'],
    );
  }

  factory Phone.fromDrift(phone_model model) {
    return Phone(
      id: model.id.toString(),
      number: model.number,
      description: model.description,
    );
  }

  factory Phone.fromJson(Map<String, dynamic> json) => _$PhoneFromJson(json);

  Map<String, dynamic> toJson() => _$PhoneToJson(this);
}
