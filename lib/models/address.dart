import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable()
class Address {
  String id;
  String zipCode;
  String country;
  String city;
  String street;
  String number;
  String state;
  String fullName;

  Address({
    required this.id,
    required this.zipCode,
    required this.country,
    required this.city,
    required this.street,
    required this.number,
    required this.state,
    required this.fullName,
  });

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}
