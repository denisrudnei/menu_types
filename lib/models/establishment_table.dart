import 'package:menu_types/models/purchase.dart';

class EstablishmentTable {
  String id = "";
  bool inUse;
  final String name;
  Purchase? activeOrder;

  bool hasNotifications;
  EstablishmentTable({
    required this.inUse,
    required this.name,
    this.id = "",
    this.hasNotifications = false,
  });

  factory EstablishmentTable.fromMap(Map<String, dynamic> map) {
    var table = EstablishmentTable(
      id: map['id'],
      inUse: map['inUse'],
      name: map['name'],
    );
    if (map['activeOrder'] != null) {
      table.activeOrder = Purchase.fromMap(map['activeOrder']);
    }

    return table;
  }
}
