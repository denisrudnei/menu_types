import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_origin.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer_service/printable_document.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/repository/db/postgres/user_repository_postgres.dart';

part 'purchase.g.dart';

@JsonSerializable()
class Purchase implements PrintableDocument {
  int? id;
  List<Product> products = [];
  Payment payment = Payment.empty();
  double get totalPrice => products.fold<double>(
      0, (previousValue, element) => previousValue + element.total);
  int get totalAmount => products.fold<int>(
      0, (previousValue, element) => previousValue + element.amount.toInt());

  POS? pos;
  DateTime createdAt = DateTime.now();
  DeliveryStatus status = DeliveryStatus.REQUIRED;
  User? operator;
  User? user;
  List<Payment> payments = [];
  PurchaseType type = PurchaseType.NORMAL;
  PurchaseOrigin origin = PurchaseOrigin.POS;

  Purchase({
    this.id,
    this.pos,
    this.operator,
    this.user,
    List<Product>? products,
    List<Payment>? payments,
    this.origin = PurchaseOrigin.POS,
    this.type = PurchaseType.NORMAL,
    this.status = DeliveryStatus.REQUIRED,
    DateTime? created,
  }) {
    this.products = products ?? this.products;
    this.payments = payments ?? this.payments;
    createdAt = created ?? DateTime.now();
  }

  Purchase copyWith({
    int? id,
    List<Product>? products,
    POS? pos,
    User? operator,
    User? user,
    DateTime? createdAt,
    List<Payment>? payments,
    PurchaseOrigin? origin,
    PurchaseType? type,
  }) {
    return Purchase(
      id: id ?? this.id,
      pos: pos ?? this.pos,
      products: products ?? this.products,
      operator: operator ?? this.operator,
      user: user ?? this.user,
      created: createdAt ?? this.createdAt,
      payments: payments ?? this.payments,
      origin: origin ?? this.origin,
      type: type ?? this.type,
    );
  }

  factory Purchase.fromMap(Map<String, dynamic> map) {
    Purchase purchase = Purchase();
    purchase.id = int.parse(map['id']);

    purchase.createdAt = DateTime.parse(map['createdAt']);

    if (map['pos'] != null) {
      purchase.pos = POS.fromMap(map['pos']);
    }

    var operatorData = map['operator'];

    User operator = User(
      name: operatorData['name'],
      email: operatorData['email'],
      image: operatorData['image'],
    );
    var userData = map['user'];
    if (userData != null) {
      User user = User(
        name: userData['name'],
        email: userData['email'],
        image: userData['image'],
      );
      purchase.user = user;
    }

    purchase.operator = operator;

    for (var historyProduct in map['products']) {
      var productData = historyProduct['data'];
      Product product = Product(
        id: int.parse(historyProduct['productId']),
        name: productData['name'],
        price: double.parse(productData['price'].toString()),
        primaryImage: productData['primaryImage'] ?? '',
        description: productData['description'] ?? '',
      );
      product.amount = double.tryParse(productData['amount'].toString()) ?? 0;
      purchase.products.add(product);
    }

    if (map['payments'] != null) {
      for (var item in map['payments']) {
        purchase.payments.add(Payment.fromMap(item));
      }
    }

    purchase.payment = Payment.grouped(purchase.payments);

    return purchase;
  }

  static Future<Purchase> fromPostgres(
    Map<String, Map<String, dynamic>> row,
    UserRepositoryPostgres userRepository,
    POSRepositoryPostgres posRepository,
  ) async {
    return Purchase(
      id: row['purchase']!['id'],
      created: row['purchase']!['createdAt'],
      user: row['purchase']!['userId'] != null
          ? await userRepository.getOne(row['purchase']!['userId'])
          : null,
      operator: row['purchase']!['operatorId'] != null
          ? await userRepository.getOne(row['purchase']!['operatorId'])
          : null,
      type: PurchaseType.values.firstWhere(
        (element) => element.toShortString() == row['purchase']!['type'],
      ),
      status: DeliveryStatus.values.firstWhere(
        (element) => element.toShortString() == row['purchase']!['status'],
      ),
      origin: PurchaseOrigin.values.firstWhere(
        (element) => element.toShortString() == row['purchase']!['origin'],
      ),
      pos: await posRepository.getPOSById(row['purchase']!['posId']),
    );
  }

  factory Purchase.fromJson(Map<String, dynamic> json) =>
      _$PurchaseFromJson(json);

  Map<String, dynamic> toJson() => _$PurchaseToJson(this);
}
