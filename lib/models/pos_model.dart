import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/pos.dart';
import 'package:shared_preferences/shared_preferences.dart';

class POSModel extends ChangeNotifier {
  final List<POS> _pos = [];
  String? selected;

  void init() {
    SharedPreferences.getInstance().then((prefs) {
      selected = prefs.getString('pos');
    });
  }

  UnmodifiableListView<POS> get pos => UnmodifiableListView(_pos);

  void add(POS pos) {
    _pos.add(pos);
    notifyListeners();
  }

  void remove(POS pos) {
    _pos.remove(pos);
    notifyListeners();
  }

  void clear() {
    _pos.clear();
    notifyListeners();
  }

  void setSelected(String selected) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('pos', selected);
      this.selected = selected;
      notifyListeners();
    });
  }

  POS getSelected() {
    return _pos.firstWhere((pos) => pos.id == selected);
  }
}
