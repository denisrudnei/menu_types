import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchasesModel extends ChangeNotifier {
  Purchase? actualPurchase;

  Purchase beginPurchase({Purchase? purchase}) {
    if (actualPurchase == null) {
      actualPurchase = purchase ?? Purchase();
      notifyListeners();
    }

    return actualPurchase!;
  }

  void endPurchase() {
    actualPurchase = null;
    notifyListeners();
  }

  List<Purchase> _purchases = [];

  bool selectMany = false;
  bool requestQuantity = false;
  bool showItemsInPurchase = false;

  List<Purchase> selected = [];
  bool searchOnlyByBarcode = false;
  final String _searchOnlyByBarcode = 'searchOnlyByBarcode';

  Future init() async {
    var prefs = await SharedPreferences.getInstance();
    selectMany = prefs.getBool('selectMany') ?? false;
    requestQuantity = prefs.getBool('requestQuantity') ?? false;
    showItemsInPurchase = prefs.getBool('showItemsInPurchase') ?? false;
    searchOnlyByBarcode = prefs.getBool(_searchOnlyByBarcode) ?? false;
  }

  void setPurchases(List<Purchase> purchases) {
    _purchases = purchases;
    notifyListeners();
  }

  void addPurchase(Purchase purchase) {
    _purchases.add(purchase);
    notifyListeners();
  }

  UnmodifiableListView<Purchase> get purchases =>
      UnmodifiableListView(_purchases);

  void add(Product product, {double quantity = 1}) {
    var index = actualPurchase?.products
            .indexWhere((element) => element.id == product.id) ??
        -1;

    if (index != -1) {
      actualPurchase?.products[index].amount += quantity;
    } else {
      var toAdd = Product.clone(product);
      toAdd.amount = quantity;
      actualPurchase?.products.add(toAdd);
    }

    notifyListeners();
  }

  void subtract(Product product, {double quantity = 1}) {
    var index = actualPurchase?.products
            .indexWhere((element) => element.id == product.id) ??
        -1;

    if (index != -1) {
      if (actualPurchase?.products[index].amount == 1) {
        removeProduct(actualPurchase!.products[index]);
      } else {
        actualPurchase?.products[index].amount -= quantity;
      }
    } else {
      product.amount += quantity;
      var toAdd = Product.clone(product);
      toAdd.amount = quantity;
      actualPurchase?.products.add(toAdd);
    }

    notifyListeners();
  }

  void removeProduct(Product product) {
    actualPurchase?.products.remove(product);
    notifyListeners();
  }

  void update(Product product, double quantity) {
    var index = actualPurchase?.products.indexWhere(
          (element) => element.id == product.id,
        ) ??
        -1;

    if (index != -1) {
      actualPurchase?.products[index].amount = quantity;
    }

    notifyListeners();
  }

  void updateStatus(DeliveryStatus status) {
    actualPurchase?.status = status;
    notifyListeners();
  }

  void updateDiscount(Product product, double discount) {
    var index = actualPurchase?.products.indexWhere(
          (element) => element.id == product.id,
        ) ??
        -1;
    if (index != -1) {
      actualPurchase?.products[index].discount = discount;
      notifyListeners();
    }
  }

  void clear() {
    actualPurchase?.products.clear();
    notifyListeners();
  }

  void setUser(User user) {
    actualPurchase?.user = user;
    notifyListeners();
  }

  void setOperator(User operator) {
    actualPurchase?.operator = operator;
    notifyListeners();
  }

  void removeUser() {
    actualPurchase?.user = null;
    notifyListeners();
  }

  void addPayment(Payment payment) {
    actualPurchase?.payments.add(payment);
    notifyListeners();
  }

  void removePayment(Payment payment) {
    actualPurchase?.payments.remove(payment);
    notifyListeners();
  }

  void setPayment(List<Payment> payments) {
    actualPurchase?.payments = payments;
    notifyListeners();
  }

  void setSelectedMany(bool selectMany) {
    SharedPreferences.getInstance().then((value) {
      this.selectMany = selectMany;
      value.setBool('selectMany', selectMany);
      notifyListeners();
    });
  }

  void setRequestQuantity(bool requestQuantity) {
    SharedPreferences.getInstance().then((value) {
      this.requestQuantity = requestQuantity;
      value.setBool('requestQuantity', requestQuantity);
      notifyListeners();
    });
  }

  void setShowItemsInPurchase(bool showItemsInPurchase) {
    SharedPreferences.getInstance().then((value) {
      this.showItemsInPurchase = showItemsInPurchase;
      value.setBool('showItemsInPurchase', showItemsInPurchase);
      notifyListeners();
    });
  }

  void setSelected(List<Purchase> selected) {
    this.selected = selected;
    notifyListeners();
  }

  void addSelected(Purchase purchase) {
    selected.add(purchase);
    notifyListeners();
  }

  void removeSelected(Purchase purchase) {
    selected.remove(purchase);
    notifyListeners();
  }

  void toggleSelected(Purchase purchase) {
    if (selected.map((e) => e.id).contains(purchase.id)) {
      selected.remove(purchase);
    } else {
      selected.add(purchase);
    }
    notifyListeners();
  }

  bool isSelected(Purchase purchase) {
    return selected.map((e) => e.id).contains(purchase.id);
  }

  void setSearchOnlyByBarcode(bool searchOnlyByBarcode) async {
    this.searchOnlyByBarcode = searchOnlyByBarcode;
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool(_searchOnlyByBarcode, searchOnlyByBarcode);
    notifyListeners();
  }
}
