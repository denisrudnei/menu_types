import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/page_info.dart';
import 'package:menu_types/models/product.dart';

class ProductsModel extends ChangeNotifier {
  List<Product> _products = [];
  List<String> types = [];
  List<String> selectedTypes = [];
  UnmodifiableListView<Product> get products => UnmodifiableListView(_products);
  PageInfo pageInfo = PageInfo(page: 1, pages: 0);

  int totalSearched = 0;

  void setProducts(List<Product> products) {
    _products = products;
    notifyListeners();
  }

  void remove(Product product) {
    var index =
        searchedProducts.indexWhere((element) => element.id == product.id);
    if (index != -1) {
      searchedProducts[index].amount += product.amount;
      notifyListeners();
    }
  }

  void addQuantity(Product product, double quantity) {
    var index = searchedProducts.indexWhere(
      (element) => element.id == product.id,
    );

    if (index != -1) {
      searchedProducts[index].amount += quantity;
      notifyListeners();
    }
  }

  void removeQuantity(Product product, double quantity) {
    var index = searchedProducts.indexWhere(
      (element) => element.id == product.id,
    );
    if (index != -1) {
      searchedProducts[index].amount -= quantity;
      notifyListeners();
    }
  }

  void update(Product product, double quantity) {
    var indexSearched = searchedProducts.indexWhere(
      (element) => element.id == product.id,
    );

    if (indexSearched != -1) {
      searchedProducts[indexSearched].amount += (product.amount - quantity);
    }
    notifyListeners();
  }

  void addProduct(Product product) {
    _products.add(product);
    notifyListeners();
  }

  void setPageInfo(PageInfo pageInfo) {
    this.pageInfo = pageInfo;
    notifyListeners();
  }

  final List<Product> searchedProducts = [];
  int page = 1;
  int pages = 1;
  bool get hasNextPage => page < pages;

  void setPage(int page) {
    this.page = page;
    notifyListeners();
  }

  void setPages(int pages) {
    this.pages = pages;
    notifyListeners();
  }

  void setTotalSearched(int totalSearched) {
    this.totalSearched = totalSearched;
    notifyListeners();
  }

  void setSearched(List<Product> products) {
    searchedProducts.clear();
    searchedProducts.addAll(products);
    for (var searched in searchedProducts) {
      var index = _products.indexWhere((element) => element.id == searched.id);
      if (index != -1) {
        searched.amount -= _products[index].amount;
      }
    }
    notifyListeners();
  }

  void setTypes(List<String> types) {
    this.types = types;
    notifyListeners();
  }

  void toggleType(String type) {
    if (selectedTypes.contains(type)) {
      selectedTypes.removeWhere((element) => element == type);
    } else {
      selectedTypes.add(type);
    }
    notifyListeners();
  }

  void setSelectedTypes(List<String> selectedTypes) {
    this.selectedTypes = selectedTypes;
    notifyListeners();
  }
}
