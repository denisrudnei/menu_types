import 'package:menu_types/models/user.dart';

class UserNotification {
  final String id;
  final String content;
  final String origin;
  final DateTime date;
  bool read;
  final User user;

  UserNotification({
    required this.id,
    required this.content,
    required this.origin,
    required this.date,
    required this.read,
    required this.user,
  });

  factory UserNotification.fromMap(Map<String, dynamic> map) {
    var user = User.fromMap(map['user']);
    return UserNotification(
      id: map['id'],
      content: map['content'],
      origin: map['origin'],
      user: user,
      read: map['read'],
      date: DateTime.parse(map['date']),
    );
  }
}
