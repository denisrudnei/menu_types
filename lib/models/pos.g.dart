// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

POS _$POSFromJson(Map<String, dynamic> json) => POS(
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
    )
      ..id = json['id'] as String
      ..name = json['name'] as String
      ..hostname = json['hostname'] as String
      ..purchases = (json['purchases'] as List<dynamic>)
          .map((e) => Purchase.fromJson(e as Map<String, dynamic>))
          .toList()
      ..loadLogs = (json['loadLogs'] as List<dynamic>)
          .map((e) => LoadLog.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$POSToJson(POS instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'hostname': instance.hostname,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'purchases': instance.purchases,
      'loadLogs': instance.loadLogs,
    };
