import 'package:flutter/material.dart';

class InnerReportsModel extends ChangeNotifier {
  Widget current = const Text('Not set');

  void setCurrent(Widget widget) {
    current = widget;
    notifyListeners();
  }
}
