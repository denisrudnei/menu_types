// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'load_log.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoadLog _$LoadLogFromJson(Map<String, dynamic> json) => LoadLog(
      id: json['id'] as String,
      message: json['message'] as String,
      pos: POS.fromJson(json['pos'] as Map<String, dynamic>),
      type: $enumDecodeNullable(_$LoadLogTypeEnumMap, json['type']) ??
          LoadLogType.INFO,
    )..createdAt = DateTime.parse(json['createdAt'] as String);

Map<String, dynamic> _$LoadLogToJson(LoadLog instance) => <String, dynamic>{
      'id': instance.id,
      'message': instance.message,
      'pos': instance.pos,
      'type': _$LoadLogTypeEnumMap[instance.type],
      'createdAt': instance.createdAt.toIso8601String(),
    };

const _$LoadLogTypeEnumMap = {
  LoadLogType.INFO: 'INFO',
  LoadLogType.WARNING: 'WARNING',
  LoadLogType.ERROR: 'ERROR',
};
