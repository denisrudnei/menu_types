import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VolumeModel extends ChangeNotifier {
  double mainVolume = 1 / 25;

  Future init() async {
    var prefs = await SharedPreferences.getInstance();
    mainVolume = prefs.getDouble('mainVolume') ?? 1 / 25;
    notifyListeners();
  }

  setMainVolume(double volume) {
    SharedPreferences.getInstance().then((value) {
      value.setDouble('mainVolume', volume);
      mainVolume = volume;
      notifyListeners();
    });
  }
}
