import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SiteSettingsModel extends ChangeNotifier {
  Image logo = Image.asset('assets/images/not-found.jpg', fit: BoxFit.cover);
  String companyName = "";
  String cnpj = "";
  String address = "";

  Future init() async {
    var prefs = await SharedPreferences.getInstance();
    companyName = prefs.getString('companyName') ?? '';
    cnpj = prefs.getString('cnpj') ?? '';
    address = prefs.getString('address') ?? '';
    notifyListeners();
  }

  void setLogo(Image logo) {
    this.logo = logo;
    notifyListeners();
  }

  void setCompanyName(String name) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('companyName', name);
      companyName = name;
      notifyListeners();
    });
  }

  void setCnpj(String cnpj) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('cnpj', cnpj);
      this.cnpj = cnpj;
      notifyListeners();
    });
  }

  void setAddress(String address) {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('address', address);
      this.address = address;
      notifyListeners();
    });
  }
}
