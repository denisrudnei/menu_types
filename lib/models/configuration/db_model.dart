import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DbModel extends ChangeNotifier {
  String url = "0.0.0.0";
  String name = "pdv";
  String username = "postgres";
  String password = "postgres";
  int port = 5432;

  String text = '';

  String fileText = '';

  bool isDownloadingFiles = false;

  LoadStatus status = LoadStatus.idle;

  List<LoadServiceInterface> services = [];

  MyDatabase db;

  Future<void> init() async {
    var prefs = await SharedPreferences.getInstance();
    url = prefs.getString('db-url') ?? url;
    name = prefs.getString('db-name') ?? name;
    port = prefs.getInt('db-port') ?? port;
    username = prefs.getString('db-username') ?? username;
    password = prefs.getString('db-password') ?? password;
  }

  DbModel({required this.db}) {
    init();
  }

  void setUrl(String url) {
    this.url = url;
    notifyListeners();
  }

  void setName(String name) {
    this.name = name;
    notifyListeners();
  }

  void setText(String text) {
    this.text = text;
    notifyListeners();
  }

  void setFileText(String fileText) {
    this.fileText = fileText;
    notifyListeners();
  }

  void setIsDownloadingFiles(bool isDownloadingFiles) {
    this.isDownloadingFiles = isDownloadingFiles;
    notifyListeners();
  }

  void setPort(int port) {
    this.port = port;
    notifyListeners();
  }

  void setUsername(String username) {
    this.username = username;
    notifyListeners();
  }

  void setPassword(String password) {
    this.password = password;
    notifyListeners();
  }

  void setStatus(LoadStatus status) {
    this.status = status;
    notifyListeners();
  }

  void setServices(List<LoadServiceInterface> services) {
    this.services = services;
    notifyListeners();
  }

  void addService(LoadServiceInterface service) {
    services.add(service);
    notifyListeners();
  }

  void updateService(LoadServiceInterface service) {
    var index = services
        .indexWhere((element) => element.runtimeType == service.runtimeType);
    if (index != -1) {
      services[index] = service;
      text = service.text;
      notifyListeners();
    }
  }

  void save() {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('db-url', url);
      prefs.setString('db-name', name);
      prefs.setInt('db-port', port);
      prefs.setString('db-username', username);
      prefs.setString('db-password', password);
    });
  }

  void setDb(MyDatabase db) {
    this.db = db;
    notifyListeners();
  }
}
