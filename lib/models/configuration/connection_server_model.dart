import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:menu_types/enums/connection_status.dart';
import 'package:menu_types/models/auth_model.dart';
import 'package:menu_types/repository/auth_repository.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

String searchInServerFirstString = 'searchInServerFirst';

class ConnectionServerModel extends ChangeNotifier {
  final AuthModel authModel;
  final AuthRepository authRepository;
  String urlBase = "";
  String username = "";
  String password = "";
  String graphqlConnection = "";
  String subscriptionConnection = "";
  String token = "";

  bool searchInServerFirst = false;
  bool isConfigured = false;
  final String _isConfigured = 'isConfigured';

  ConnectionStatus status = ConnectionStatus.OFFLINE;

  ConnectionServerModel({
    required this.authModel,
    required this.authRepository,
  });

  Future<void> initInfo() async {
    var prefs = await SharedPreferences.getInstance();
    urlBase = prefs.getString('urlBase') ?? '';

    username = prefs.getString('username') ?? '';
    password = prefs.getString('password') ?? '';

    graphqlConnection = prefs.getString('graphql') ?? '';
    subscriptionConnection = prefs.getString('subscription') ?? '';
    token = prefs.getString('token') ?? '';

    searchInServerFirst = prefs.getBool(searchInServerFirstString) ?? false;

    isConfigured = prefs.getBool(_isConfigured) ?? false;

    notifyListeners();
  }

  void setUrlBase(String url) {
    SharedPreferences.getInstance().then((prefs) {
      urlBase = url;
      prefs.setString('urlBase', url);
      notifyListeners();
    });
  }

  void setUsername(String username, {bool remember = false}) {
    SharedPreferences.getInstance().then((prefs) {
      this.username = username;
      if (remember) {
        prefs.setString('username', username);
      }
      notifyListeners();
    });
  }

  void setPassword(String password) {
    this.password = password;
    notifyListeners();
  }

  void forgetMe() {
    SharedPreferences.getInstance().then((value) {
      value.setString('username', '');
      username = '';
      value.setString('password', '');
      password = '';
      notifyListeners();
    });
  }

  void setGraphqlConnection(String connection) {
    SharedPreferences.getInstance().then((prefs) {
      graphqlConnection = connection;
      prefs.setString('graphql', connection);
      notifyListeners();
    });
  }

  void setSubscriptionConnection(String connection) {
    SharedPreferences.getInstance().then((prefs) {
      subscriptionConnection = connection;
      prefs.setString('subscription', connection);
      notifyListeners();
    });
  }

  void setToken(String newToken) {
    SharedPreferences.getInstance().then((prefs) {
      token = newToken;
      prefs.setString('token', newToken);
      notifyListeners();
    });
  }

  void setConnectionStatus(ConnectionStatus newStatus) {
    status = newStatus;
    notifyListeners();
  }

  void setSearchInServerFirst(bool search) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool(searchInServerFirstString, search);
    searchInServerFirst = search;
    notifyListeners();
  }

  void toggleSearchInServerFirst() async {
    var prefs = await SharedPreferences.getInstance();
    searchInServerFirst = !searchInServerFirst;
    prefs.setBool(searchInServerFirstString, searchInServerFirst);
    notifyListeners();
  }

  void setIsConfigured(bool isConfigured) async {
    var prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_isConfigured, isConfigured);
    this.isConfigured = isConfigured;
    notifyListeners();
  }

  Future<void> readConfigurationFromJson(File file) async {
    try {
      var content = await file.readAsString();
      var parsed = jsonDecode(content);
      setGraphqlConnection(parsed['graphql'] ?? '');
      setSubscriptionConnection(parsed['subscriptions'] ?? '');
      setToken(parsed['token'] ?? '');
    } on Exception catch (error) {
      LoggerUtil.logger.e(error);
      Future.error(error);
    }
  }

  Future validateConfigurationByLogin(String username, String password) async {
    Uri url = Uri();
    if (urlBase.endsWith('/graphql')) {
      setUrlBase(urlBase.replaceAll('/graphql', '/'));
    }
    try {
      url = Uri.parse(
        (urlBase.endsWith('/')
                ? urlBase.substring(0, urlBase.length - 1)
                : urlBase) +
            '/api/config/info',
      );
    } on Exception catch (e) {
      LoggerUtil.logger.e(e);
      throw Exception(e);
    } catch (e) {
      rethrow;
    }

    LoggerUtil.logger.i('Connecting to $url');

    try {
      var response = await http.post(
        url,
        body: jsonEncode(
          {
            'username': username,
            'password': password,
          },
        ),
        headers: {
          'content-type': 'application/json',
        },
      );

      if (response.statusCode == 401) {
        throw Exception('Unauthenticated');
      }
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);

        setUsername(username);
        setPassword(password);

        setToken(json['token']);

        setGraphqlConnection(json['graphql']);
        setSubscriptionConnection(json['subscriptions']);

        status = ConnectionStatus.CONNECTING;

        try {
          var user = await authRepository.login(
            username,
            password,
          );

          authModel.setUser(user);

          authModel.addUser(username);

          authModel.setIsLoggingIn(false);
        } catch (e) {
          authModel.setIsLoggingIn(false);
          LoggerUtil.logger.e(e);
          rethrow;
        }
      }
      authModel.setIsLoggingIn(false);
    } catch (e) {
      LoggerUtil.logger.e(e);
      authModel.setIsLoggingIn(false);
      rethrow;
    }
  }
}
