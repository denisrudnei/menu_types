import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/enums/load_log_type.dart';
import 'package:menu_types/models/pos.dart';

part 'load_log.g.dart';

@JsonSerializable()
class LoadLog {
  String id = "";
  String message = "";
  POS pos;
  LoadLogType type;
  DateTime createdAt = DateTime.now();

  LoadLog({
    required this.id,
    required this.message,
    required this.pos,
    this.type = LoadLogType.INFO,
  });

  factory LoadLog.fromMap(Map<String, dynamic> map) {
    LoadLog loadLog = LoadLog(
      id: map['id'],
      message: map['message'],
      pos: POS.fromMap(map['pos']),
    );
    return loadLog;
  }

  factory LoadLog.fromJson(Map<String, dynamic> json) =>
      _$LoadLogFromJson(json);

  Map<String, dynamic> toJson() => _$LoadLogToJson(this);
}
