import 'package:json_annotation/json_annotation.dart';
import 'package:menu_types/models/load_log.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:uuid/uuid.dart';

part 'pos.g.dart';

@JsonSerializable()
class POS {
  String id = const Uuid().v4();
  String name = "";
  String hostname = "";
  DateTime? createdAt;
  DateTime? updatedAt;

  List<Purchase> purchases = [];
  List<LoadLog> loadLogs = [];

  POS({
    this.createdAt,
    this.updatedAt,
  }) {
    createdAt ??= DateTime.now();
    updatedAt ??= DateTime.now();
  }

  factory POS.fromMap(Map<String, dynamic> map) {
    var pos = POS();
    pos.id = map['id'];
    pos.name = map['name'];
    pos.hostname = map['hostname'];
    pos.createdAt = DateTime.parse(map['createdAt']);
    pos.updatedAt = DateTime.parse(map['updatedAt']);
    return pos;
  }

  factory POS.fromDrift(DriftPOSModelData data) {
    return POS()
      ..id = data.id
      ..name = data.name
      ..createdAt = data.createdAt
      ..updatedAt = data.updatedAt;
  }

  POS.fromPostgres(Map<String, Map<String, dynamic>> row) {
    id = row['pos']!['id'];
    name = row['pos']!['name'];
    hostname = row['pos']!['hostname'];
    createdAt = row['pos']!['createdAt'];
    updatedAt = row['pos']!['updatedAt'];
  }

  factory POS.fromJson(Map<String, dynamic> json) => _$POSFromJson(json);

  Map<String, dynamic> toJson() => _$POSToJson(this);
}
