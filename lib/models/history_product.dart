import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';

class HistoryProduct {
  String id;
  int productId;
  Product data;
  Purchase purchase;

  HistoryProduct({
    required this.id,
    required this.productId,
    required this.data,
    required this.purchase,
  });
}
