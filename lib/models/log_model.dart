import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/load_log.dart';

class LogModel extends ChangeNotifier {
  final List<LoadLog> _loadLogs = [];
  String logText = '';

  UnmodifiableListView<LoadLog> get loadLogs => UnmodifiableListView(_loadLogs);

  void addLoadLog(LoadLog loadLog) {
    _loadLogs.add(loadLog);
    notifyListeners();
  }

  void setLogText(String logText) {
    this.logText = logText;
    notifyListeners();
  }

  void removeLoadLog(LoadLog loadLog) {
    _loadLogs.remove(loadLog);
    notifyListeners();
  }

  void clear() {
    _loadLogs.clear();
    notifyListeners();
  }
}
