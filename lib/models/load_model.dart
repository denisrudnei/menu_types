import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoadModel extends ChangeNotifier {
  final String _lastSuccessfulUpdate = 'lastSuccessfulUpdate';
  DateTime? lastSuccessfulUpdate;
  bool get firstLoad => lastSuccessfulUpdate == null;

  LoadModel() {
    init();
  }

  Future<void> init() async {
    var prefs = await SharedPreferences.getInstance();
    String? lastSuccessfulUpdateString = prefs.getString(_lastSuccessfulUpdate);
    if (lastSuccessfulUpdateString != null) {
      lastSuccessfulUpdate = DateTime.parse(lastSuccessfulUpdateString);
    }
  }

  Future<void> setLastSuccessfulUpdate(DateTime date) async {
    var prefs = await SharedPreferences.getInstance();
    lastSuccessfulUpdate = date;
    prefs.setString(_lastSuccessfulUpdate, date.toIso8601String());
    notifyListeners();
  }
}
