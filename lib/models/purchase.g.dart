// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'purchase.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Purchase _$PurchaseFromJson(Map<String, dynamic> json) => Purchase(
      id: json['id'] as int?,
      pos: json['pos'] == null
          ? null
          : POS.fromJson(json['pos'] as Map<String, dynamic>),
      operator: json['operator'] == null
          ? null
          : User.fromJson(json['operator'] as Map<String, dynamic>),
      user: json['user'] == null
          ? null
          : User.fromJson(json['user'] as Map<String, dynamic>),
      products: (json['products'] as List<dynamic>?)
          ?.map((e) => Product.fromJson(e as Map<String, dynamic>))
          .toList(),
      payments: (json['payments'] as List<dynamic>?)
          ?.map((e) => Payment.fromJson(e as Map<String, dynamic>))
          .toList(),
      origin: $enumDecodeNullable(_$PurchaseOriginEnumMap, json['origin']) ??
          PurchaseOrigin.POS,
      type: $enumDecodeNullable(_$PurchaseTypeEnumMap, json['type']) ??
          PurchaseType.NORMAL,
      status: $enumDecodeNullable(_$DeliveryStatusEnumMap, json['status']) ??
          DeliveryStatus.REQUIRED,
    )
      ..payment = Payment.fromJson(json['payment'] as Map<String, dynamic>)
      ..createdAt = DateTime.parse(json['createdAt'] as String);

Map<String, dynamic> _$PurchaseToJson(Purchase instance) => <String, dynamic>{
      'id': instance.id,
      'products': instance.products,
      'payment': instance.payment,
      'pos': instance.pos,
      'createdAt': instance.createdAt.toIso8601String(),
      'status': _$DeliveryStatusEnumMap[instance.status],
      'operator': instance.operator,
      'user': instance.user,
      'payments': instance.payments,
      'type': _$PurchaseTypeEnumMap[instance.type],
      'origin': _$PurchaseOriginEnumMap[instance.origin],
    };

const _$PurchaseOriginEnumMap = {
  PurchaseOrigin.ECOMMERCE: 'ECOMMERCE',
  PurchaseOrigin.POS: 'POS',
  PurchaseOrigin.ESTABLISHMENT_TABLE: 'ESTABLISHMENT_TABLE',
};

const _$PurchaseTypeEnumMap = {
  PurchaseType.DELIVERY: 'DELIVERY',
  PurchaseType.NORMAL: 'NORMAL',
  PurchaseType.RESTAURANT_ORDER: 'RESTAURANT_ORDER',
};

const _$DeliveryStatusEnumMap = {
  DeliveryStatus.REQUIRED: 'REQUIRED',
  DeliveryStatus.IN_PRODUCTION: 'IN_PRODUCTION',
  DeliveryStatus.DELIVERY_PROCESS: 'DELIVERY_PROCESS',
  DeliveryStatus.DELIVERED: 'DELIVERED',
  DeliveryStatus.FINISHED: 'FINISHED',
};
