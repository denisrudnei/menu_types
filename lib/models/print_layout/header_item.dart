import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HeaderItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type = 'HaderItem';

  @override
  int position;

  bool storeName = false;
  bool address = false;
  bool cnpj = false;

  HeaderItem({
    required this.id,
    required this.type,
    required this.position,
    required this.cnpj,
    required this.address,
    required this.storeName,
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  static HeaderItem fromMap(Map<String, dynamic> map) {
    return HeaderItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
      storeName: map['storeName'],
      address: map['address'],
      cnpj: map['cnpj'],
    );
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    var prefs = await SharedPreferences.getInstance();
    String companyName = prefs.getString('companyName') ?? '';
    String cnpj = prefs.getString('cnpj') ?? '';
    String address = prefs.getString('address') ?? '';
    List<Widget> children = [];
    Column header = Column(children: children);

    if (storeName) {
      children.add(Text(
        companyName,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      ));
    }

    if (this.cnpj) {
      children.add(Text(cnpj));
    }

    if (this.address) {
      children.add(Text(address));
    }

    return header;
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    var prefs = await SharedPreferences.getInstance();
    String companyName = prefs.getString('companyName') ?? '';
    String cnpj = prefs.getString('cnpj') ?? '';
    String address = prefs.getString('address') ?? '';
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);
    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontB);

    if (storeName) {
      bytes += generator.row([
        PosColumn(width: 1),
        PosColumn(
          text: companyName,
          width: 10,
          styles: const PosStyles(
            bold: true,
            align: PosAlign.center,
            height: PosTextSize.size2,
          ),
        ),
        PosColumn(width: 1),
      ]);
      bytes += generator.emptyLines(1);
    }

    if (this.address) {
      bytes += generator.row([
        PosColumn(width: 1),
        PosColumn(
          text: address,
          width: 10,
          styles: const PosStyles(
            align: PosAlign.center,
          ),
        ),
        PosColumn(width: 1),
      ]);
    }

    if (this.cnpj) {
      bytes += generator.row([
        PosColumn(width: 1),
        PosColumn(
          text: 'CNPJ: $cnpj',
          width: 10,
          styles: const PosStyles(
            align: PosAlign.center,
          ),
        ),
        PosColumn(width: 1),
      ]);
    }

    return bytes;
  }
}
