import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/format_utils.dart';

class PaymentInfoItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type;

  @override
  int position;

  PaymentInfoItem({
    required this.id,
    required this.type,
    required this.position,
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  factory PaymentInfoItem.fromMap(Map<String, dynamic> map) {
    return PaymentInfoItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);
    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontB);

    for (var item in purchase.payments) {
      bytes += generator.row([
        PosColumn(
          text: '${item.type}:',
          width: 6,
          styles: const PosStyles(
            align: PosAlign.left,
            bold: false,
          ),
        ),
        PosColumn(
          text: getCurrency(item.value),
          width: 6,
          styles: const PosStyles(
            align: PosAlign.right,
            bold: false,
          ),
        )
      ]);
    }

    bytes += generator.row([
      PosColumn(
        text: 'Total: ',
        width: 6,
        styles: const PosStyles(
          align: PosAlign.left,
          bold: true,
        ),
      ),
      PosColumn(
        text: getCurrency(purchase.totalPrice),
        width: 6,
        styles: const PosStyles(
          align: PosAlign.right,
          bold: true,
        ),
      )
    ]);

    for (var item in purchase.payments) {
      bytes += generator.row([
        PosColumn(
          text: item.type,
          width: 6,
          styles: const PosStyles(
            align: PosAlign.left,
          ),
        ),
        PosColumn(
          text: getCurrency(item.value),
          width: 6,
          styles: const PosStyles(
            align: PosAlign.right,
          ),
        )
      ]);
    }

    return bytes;
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    List<Widget> children = [];

    for (var item in purchase.payments) {
      children.add(
        Row(
          children: [
            Text(
              '${item.type}:',
            ),
            const Spacer(),
            Text(getCurrency(item.value)),
          ],
        ),
      );
    }

    children.add(
      Row(
        children: [
          const Text(
            'Total: ',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const Spacer(),
          Text(
            getCurrency(getTotal(purchase)),
          ),
        ],
      ),
    );
    return Column(
      children: children,
    );
  }

  double getTotal(Purchase purchase) {
    return purchase.payments
        .fold<double>(0, (value, payment) => value += payment.value);
  }
}
