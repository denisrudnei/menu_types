import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/format_utils.dart';

class ProductTableItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type;

  @override
  int position;

  String printType = 'NORMAL';

  ProductTableItem({
    required this.id,
    required this.type,
    required this.position,
    this.printType = 'NORMAL',
  }) : super(
          id: id,
          position: position,
          type: type,
        );

  factory ProductTableItem.fromMap(Map<String, dynamic> map) {
    return ProductTableItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
      printType: map['printType'],
    );
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    return Table(
      columnWidths: const {
        0: FlexColumnWidth(1),
        1: FlexColumnWidth(6),
        3: FlexColumnWidth(3),
      },
      border: TableBorder.all(width: 1),
      children: [
        const TableRow(
          children: [
            TableCell(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Center(child: Text('#')),
              ),
            ),
            TableCell(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Center(child: Text('Descrição')),
              ),
            ),
            TableCell(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Center(child: Text('VAL')),
              ),
            ),
          ],
        ),
        for (var product in purchase.products)
          TableRow(
            children: [
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(child: Text(product.id.toString())),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(child: Text(product.name)),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Center(
                    child: Text(getCurrency(product.price)),
                  ),
                ),
              ),
            ],
          )
      ],
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontB);

    bytes += generator.hr(ch: '-');

    bytes += generator.row([
      PosColumn(
        text: '#',
        width: 1,
        styles: const PosStyles(align: PosAlign.left),
      ),
      PosColumn(
          text: 'Descricao',
          width: 9,
          styles: const PosStyles(align: PosAlign.center)),
      PosColumn(
        text: 'VAL',
        width: 2,
        styles: const PosStyles(align: PosAlign.right),
      ),
    ]);

    bytes += generator.hr(ch: '-');

    for (var i = 0; i < purchase.products.length; i++) {
      bytes += generator.row([
        PosColumn(
          text: (i + 1).toString(),
          styles: const PosStyles(align: PosAlign.left),
          width: 1,
        ),
        PosColumn(
          text: purchase.products[i].name,
          styles: const PosStyles(align: PosAlign.center),
          width: 8,
        ),
        PosColumn(
          text: getCurrency(purchase.products[i].price),
          styles: const PosStyles(align: PosAlign.right),
          width: 3,
        ),
      ]);
    }

    return bytes;
  }
}
