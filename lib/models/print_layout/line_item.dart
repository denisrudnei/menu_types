import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';

class LineItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type;

  @override
  int position;

  String character = '-';

  LineItem({
    required this.id,
    required this.type,
    required this.position,
    this.character = '-',
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  factory LineItem.fromMap(Map<String, dynamic> map) {
    return LineItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
      character: map['character'],
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    List<int> bytes = [];
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    bytes += generator.setGlobalFont(PosFontType.fontB);

    bytes += generator.hr(ch: character);

    return bytes;
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    return Text(
      ''.padLeft(120, character),
      overflow: TextOverflow.clip,
      softWrap: false,
    );
  }
}
