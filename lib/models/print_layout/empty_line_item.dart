import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';

class EmptyLineItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;
  @override
  String type;
  @override
  int position;
  int numberOfLines = 1;

  EmptyLineItem({
    required this.id,
    required this.type,
    required this.position,
    required this.numberOfLines,
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  factory EmptyLineItem.fromMap(Map<String, dynamic> map) {
    return EmptyLineItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
      numberOfLines: map['numberOfLines'],
    );
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    List<Widget> children = [];
    for (var i = 0; i < numberOfLines; i++) {
      children.add(
        Stack(
          alignment: AlignmentDirectional.center,
          children: const [
            Divider(
              thickness: 3,
              height: 2,
              color: Colors.black45,
            ),
            Text(
              'Linha em branco',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );
    }
    return Column(
      children: children,
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    List<int> bytes = [];
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    bytes += generator.emptyLines(numberOfLines);

    return bytes;
  }
}
