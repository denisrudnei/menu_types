import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/format_utils.dart';

class PurchaseInformation extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type;

  @override
  int position;

  PurchaseInformation({
    required this.id,
    required this.type,
    required this.position,
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  factory PurchaseInformation.fromMap(Map<String, dynamic> map) {
    return PurchaseInformation(
      id: map['id'],
      type: map['type'],
      position: map['position'],
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    List<int> bytes = [];

    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    bytes += generator.reset();
    bytes += generator.setGlobalFont(PosFontType.fontB);

    bytes += generator.text(
      'Operador: ${purchase.operator?.name}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.text(
      'PDV: ${purchase.pos?.id}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.text(
      'Venda: ${purchase.id}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );

    bytes += generator.text(
      'Data: ${getDateAndHour(purchase.createdAt.toLocal())}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.hr(ch: '-');

    if (purchase.user != null) {
      bytes += generator.emptyLines(2);
      bytes += generator.text(
        'CPF: CPF DO CLIENTE',
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      );
      bytes += generator.text(
        'Cliente: ${purchase.user?.name ?? 'Nome'}',
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      );
    }

    return bytes;
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    List<Widget> children = [];

    children.addAll(
      [
        Center(
          child:
              Text('Operador ${purchase.operator?.name ?? 'Nome do operador'}'),
        ),
        Center(
          child: Text('PDV: ${purchase.pos?.id ?? 'PDV'}'),
        ),
        Center(
          child: Text('Venda: ${purchase.id ?? 'Número da venda'}'),
        ),
        Center(
          child: Text('Data: ${getDateAndHour(purchase.createdAt.toLocal())}'),
        ),
        Text(
          ''.padLeft(120, '-'),
          overflow: TextOverflow.clip,
          softWrap: false,
        )
      ],
    );

    if (purchase.user != null) {
      children.addAll(
        [
          const Center(child: Text('CPF: CPF DO CLIENTE')),
          Center(
            child: Text('Cliente: ${purchase.user?.name ?? 'Nome do cliente'}'),
          ),
        ],
      );
    }
    return Column(
      children: children,
    );
  }
}
