import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrintLayoutModel extends ChangeNotifier {
  List<PrintLayout> _layouts = [];
  UnmodifiableListView<PrintLayout> get layouts =>
      UnmodifiableListView(_layouts);
  bool showStoreName = false;
  bool showAddress = false;
  bool showCnpj = false;

  final String _printLayoutForDelivery = 'printLayoutForDelivery';
  final String _printLayoutForPurchase = 'printLayoutForPurchase';

  PrintLayout? printLayoutForPurchase;
  PrintLayout? printLayoutForDelivery;

  String printLayoutForPurchaseId = '';
  String printLayoutForDeliveryId = '';

  Future<void> init() async {
    var _prefs = await SharedPreferences.getInstance();
    printLayoutForPurchaseId = _prefs.getString(_printLayoutForPurchase) ?? '';
    printLayoutForDeliveryId = _prefs.getString(_printLayoutForDelivery) ?? '';
  }

  void add(PrintLayout layout) {
    if (!containsLayout(layout.id)) {
      _layouts.add(layout);
    }
    notifyListeners();
  }

  Future<void> setPrintLayouts(List<PrintLayout> layouts) async {
    _layouts = layouts.toList();
    var prefs = await SharedPreferences.getInstance();

    printLayoutForPurchaseId = prefs.getString(_printLayoutForPurchase) ?? '';
    printLayoutForDeliveryId = prefs.getString(_printLayoutForDelivery) ?? '';

    if (containsLayout(printLayoutForPurchaseId)) {
      printLayoutForPurchase = getById(printLayoutForPurchaseId);
    }

    if (containsLayout(printLayoutForDeliveryId)) {
      printLayoutForDelivery = getById(printLayoutForDeliveryId);
    }

    notifyListeners();
  }

  bool containsLayout(String id) {
    return _layouts.map((e) => e.id).toList().contains(id);
  }

  PrintLayout? getById(String id) {
    return _layouts.firstWhere((element) => element.id == id);
  }

  void setPrintLayoutForPurchase(PrintLayout? layout) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(_printLayoutForPurchase, layout?.id ?? '');
    printLayoutForPurchase = layout;
    notifyListeners();
  }

  void setPrintLayoutForDelivery(PrintLayout? layout) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString(_printLayoutForDelivery, layout?.id ?? '');
    printLayoutForDelivery = layout;
    notifyListeners();
  }

  void remove(PrintLayout layout) {
    _layouts.remove(layout);
    notifyListeners();
  }

  void clear() {
    _layouts.clear();
    notifyListeners();
  }

  void removeById(String id) {
    _layouts.removeWhere((layout) => layout.id == id);
    notifyListeners();
  }

  void setShowStoreName(bool showStoreName) {
    this.showStoreName = showStoreName;
    notifyListeners();
  }

  void setShowAddress(bool showAddress) {
    this.showAddress = showAddress;
    notifyListeners();
  }

  void setShowCnpj(bool showCnpj) {
    this.showCnpj = showCnpj;
    notifyListeners();
  }

  void addItem(String id, PrintLayoutItem item) {
    var index = _layouts.indexWhere((element) => element.id == id);
    if (index != -1) {
      _layouts[index].items.add(item);
      notifyListeners();
    }
  }

  void setPosition(
    String id,
    int oldPosition,
    int newPosition,
  ) {
    var index = _layouts.indexWhere((element) => element.id == id);
    if (index != -1) {
      if (oldPosition < newPosition) {
        int end = newPosition - 1;
        var item = _layouts[index].items.elementAt(oldPosition);
        int i = 0;
        int local = oldPosition;

        do {
          _layouts[index].items[local] = _layouts[index].items[++local];
          i++;
        } while (i < end - oldPosition);
        _layouts[index].items[end] = item;
      }

      if (oldPosition > newPosition) {
        var item = _layouts[index].items[oldPosition];
        for (int i = oldPosition; i > newPosition; i--) {
          _layouts[index].items[i] = _layouts[index].items[i - 1];
        }
        _layouts[index].items[newPosition] = item;
      }

      notifyListeners();
    }
  }

  void removeItem(String id, String item) {
    var index = _layouts.indexWhere((element) => element.id == id);
    if (index != -1) {
      var itemIndex =
          _layouts[index].items.indexWhere((element) => element.id == item);
      if (itemIndex != -1) {
        _layouts[index].items.removeAt(itemIndex);
        notifyListeners();
      }
    }
  }
}
