import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/models/printing/print_layout_item.dart';
import 'package:menu_types/models/purchase.dart';

class CutItem extends PrintLayoutItem with PrinterItem {
  @override
  String id;

  @override
  String type;

  @override
  int position;

  CutItem({
    required this.id,
    required this.type,
    required this.position,
  }) : super(
          id: id,
          type: type,
          position: position,
        );

  factory CutItem.fromMap(Map<String, dynamic> map) {
    return CutItem(
      id: map['id'],
      type: map['type'],
      position: map['position'],
    );
  }

  @override
  Future<List<int>> getData(Purchase purchase) async {
    List<int> bytes = [];
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    bytes += generator.cut(mode: PosCutMode.partial);

    return bytes;
  }

  @override
  Future<Widget> printText(Purchase purchase) async {
    return Stack(
      alignment: AlignmentDirectional.center,
      children: const [
        Divider(
          thickness: 3,
          height: 2,
          color: Colors.red,
        ),
        Text(
          'Corte',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
