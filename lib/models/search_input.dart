class SearchInput {
  final String? name;
  final String? category;
  final double? minPrice;
  final double? maxPrice;
  final List<String> type;

  const SearchInput({
    required this.name,
    required this.category,
    required this.minPrice,
    required this.maxPrice,
    this.type = const [],
  });

  factory SearchInput.empty() {
    return const SearchInput(
      name: null,
      category: null,
      minPrice: null,
      maxPrice: null,
      type: [],
    );
  }

  SearchInput copyWith({
    String? name,
    String? category,
    double? minPrice,
    double? maxPrice,
    List<String>? type,
  }) {
    return SearchInput(
      name: name ?? this.name,
      category: category ?? this.category,
      minPrice: minPrice ?? this.minPrice,
      maxPrice: maxPrice ?? this.maxPrice,
      type: type ?? this.type,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {};
    if (name != null) result['name'] = name;
    if (minPrice != null) result['minPrice'] = minPrice;
    if (maxPrice != null) result['maxPrice'] = maxPrice;
    result['type'] = type;
    return result;
  }
}
