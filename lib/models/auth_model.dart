import 'package:flutter/material.dart';
import 'package:menu_types/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthModel extends ChangeNotifier {
  User? loggedUser;
  bool get isLogged => loggedUser != null;
  bool rememberMe = false;
  bool isLoggingIn = false;

  List<String> users = [];

  Future init() async {
    var prefs = await SharedPreferences.getInstance();
    users = prefs.getStringList('users') ?? [];
    rememberMe = prefs.getBool('rememberMe') ?? false;
    notifyListeners();
  }

  void setRememberMe(bool remember) {
    SharedPreferences.getInstance().then((value) {
      value.setBool('rememberMe', remember);
      rememberMe = remember;
      notifyListeners();
    });
  }

  void setUser(User user) {
    loggedUser = user;
    notifyListeners();
  }

  void logout() {
    loggedUser = null;
    notifyListeners();
  }

  void setIsLoggingIn(bool isLoggingIn) {
    this.isLoggingIn = isLoggingIn;
    notifyListeners();
  }

  void addUser(String username) async {
    var prefs = await SharedPreferences.getInstance();
    if (!users.contains(username)) {
      users.add(username);
      prefs.setStringList('users', users);
      notifyListeners();
    }
  }

  void removeUser(String username) async {
    var prefs = await SharedPreferences.getInstance();
    users.remove(username);
    prefs.setStringList('users', users);
    notifyListeners();
  }
}
