import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:menu_types/models/establishment_table.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';

class EstablishmentTableModel extends ChangeNotifier {
  final List<EstablishmentTable> _tables = [];

  final List<Product> products = [];
  EstablishmentTable actual = EstablishmentTable(
    id: '',
    inUse: false,
    name: 'Empty',
  );

  UnmodifiableListView<EstablishmentTable> get tables =>
      UnmodifiableListView(_tables);

  void add(EstablishmentTable table) {
    _tables.add(table);
    notifyListeners();
  }

  void remove(EstablishmentTable table) {
    _tables.remove(table);
    notifyListeners();
  }

  void updateStatus(String id, bool inUse) {
    int index = _tables.indexWhere((element) => element.id == id);
    if (index != -1) {
      EstablishmentTable table = EstablishmentTable(
        inUse: inUse,
        name: _tables[index].name,
      );
      table.id = id;
      _tables[index] = table;
      notifyListeners();
    }
  }

  void clear() {
    _tables.clear();
    notifyListeners();
  }

  set products(List<Product> products) {
    this.products = products;
    notifyListeners();
  }

  setActual(EstablishmentTable actual) {
    this.actual = actual;
    notifyListeners();
  }

  addProductInOrder(
    Purchase? purchase,
    Product product, {
    double quantity = 1,
  }) {
    if (purchase == null) return;
    var index =
        purchase.products.indexWhere((element) => element.id == product.id);
    if (index != -1) {
      product.amount -= 1;
      purchase.products[index].amount += 1;
    } else {
      product.amount -= quantity;
      var toAdd = Product.clone(product);
      toAdd.amount = quantity;
      purchase.products.add(toAdd);
    }
  }

  addProduct(Product product, {String tableId = '', double quantity = 1}) {
    if ((actual.id == tableId || tableId == '')) {
      addProductInOrder(actual.activeOrder!, product);
    }
    var index = _tables.indexWhere((element) => element.id == tableId);
    if (index != -1) {
      addProductInOrder(_tables[index].activeOrder, product);
    }
    notifyListeners();
  }

  removeProductInOrder(Purchase purchase, Product product) {
    var index =
        purchase.products.indexWhere((element) => element.id == product.id);
    if (index != -1) {
      purchase.products[index].amount += product.amount;
    }
    purchase.products.remove(product);
  }

  void removeProduct(Product product,
      {String tableId = '', double quantity = 1}) {
    if ((actual.id == tableId || tableId == '')) {
      removeProductInOrder(actual.activeOrder!, product);
    }
    var index = _tables.indexWhere((element) => element.id == tableId);
    if (index != -1) {
      removeProductInOrder(_tables[index].activeOrder!, product);
    }
    notifyListeners();
  }
}
