import 'package:intl/intl.dart';

String getCurrency(double value) {
  return NumberFormat.simpleCurrency(locale: 'pt_BR').format(value);
}

String getDateAndHour(DateTime date) {
  return DateFormat('dd/MM/yyyy HH:mm:ss').format(date);
}

String getDate(DateTime date) {
  return DateFormat('dd/MM/yyyy').format(date);
}

String getNumber(double value, {int decimal = 3}) {
  String base = '#0.';
  String mask = base.padRight(base.length + decimal, '0');

  return NumberFormat(mask, 'pt_BR').format(value);
}
