import 'dart:io';

import 'package:esc_pos_utils_plus/esc_pos_utils.dart';
import 'package:image/image.dart';
import 'package:menu_types/enums/print_request_type.dart';
import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/pos_model.dart';
import 'package:menu_types/models/print_layout/print_layout_model.dart';
import 'package:menu_types/models/printing/print_request.dart';
import 'package:menu_types/models/printing/printer.dart';

import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/configuration/site_settings_model.dart';
import 'package:menu_types/util/format_utils.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/pdf/pdf_purchase_util.dart';
import 'package:menu_types/util/screen/image_util.dart';
import 'package:menu_types/util/udp_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrintUtil {
  final POSModel posModel;
  final PrinterModel printerModel;
  final SiteSettingsModel siteSettingsModel;
  final PrintLayoutModel printLayoutModel;
  PrintUtil({
    required this.posModel,
    required this.printerModel,
    required this.siteSettingsModel,
    required this.printLayoutModel,
  });

  Future<List<int>> getBytesFromPurchase(
    Purchase purchase,
  ) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);
    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontB);

    if (printerModel.printCompanyLogo) {
      ImageUtil imageUtil = ImageUtil();
      await imageUtil.init();
      var imageFile = imageUtil.getLogoFile();
      if (imageFile != null) {
        var resizedImage = copyResize(
          decodeImage(imageFile.readAsBytesSync())!,
          width: 400,
          height: 250,
        );
        bytes += generator.image(resizedImage);
      }
    }

    bytes += generator.row([
      PosColumn(width: 1),
      PosColumn(
        text: siteSettingsModel.companyName,
        width: 10,
        styles: const PosStyles(
          bold: true,
          align: PosAlign.center,
          height: PosTextSize.size2,
        ),
      ),
      PosColumn(width: 1),
    ]);

    bytes += generator.emptyLines(1);

    bytes += generator.row([
      PosColumn(width: 1),
      PosColumn(
        text: siteSettingsModel.address,
        width: 10,
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      ),
      PosColumn(width: 1),
    ]);

    bytes += generator.row([
      PosColumn(width: 1),
      PosColumn(
        text: 'CNPJ: ${siteSettingsModel.cnpj}',
        width: 10,
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      ),
      PosColumn(width: 1),
    ]);

    bytes += generator.emptyLines(2);

    generator.spaceBetweenRows = 2;

    bytes += generator.hr(ch: '-');

    bytes += generator.row([
      PosColumn(
        text: '#',
        width: 1,
        styles: const PosStyles(align: PosAlign.left),
      ),
      PosColumn(
          text: 'Descricao',
          width: 9,
          styles: const PosStyles(align: PosAlign.center)),
      PosColumn(
        text: 'VAL',
        width: 2,
        styles: const PosStyles(align: PosAlign.right),
      ),
    ]);

    bytes += generator.hr(ch: '-');

    generator.spaceBetweenRows = 5;

    for (var i = 0; i < purchase.products.length; i++) {
      bytes += generator.row([
        PosColumn(
          text: (i + 1).toString(),
          styles: const PosStyles(align: PosAlign.left),
          width: 1,
        ),
        PosColumn(
          text: purchase.products[i].name,
          styles: const PosStyles(align: PosAlign.center),
          width: 8,
        ),
        PosColumn(
          text: getCurrency(purchase.products[i].price),
          styles: const PosStyles(align: PosAlign.right),
          width: 3,
        ),
      ]);
    }

    bytes += generator.emptyLines(1);

    bytes += generator.row([
      PosColumn(
        text: 'Total: ',
        width: 6,
        styles: const PosStyles(
          align: PosAlign.left,
          bold: true,
        ),
      ),
      PosColumn(
        text: getCurrency(purchase.totalPrice),
        width: 6,
        styles: const PosStyles(
          align: PosAlign.right,
          bold: true,
        ),
      )
    ]);

    for (var item in purchase.payments) {
      bytes += generator.row([
        PosColumn(
          text: item.type,
          width: 6,
          styles: const PosStyles(
            align: PosAlign.left,
          ),
        ),
        PosColumn(
          text: getCurrency(item.value),
          width: 6,
          styles: const PosStyles(
            align: PosAlign.right,
          ),
        )
      ]);
    }

    bytes += generator.hr(ch: '-');

    bytes += generator.emptyLines(1);

    bytes += generator.text(
      'Operador: ${purchase.operator?.name ?? 'Sem Operador'}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.text(
      'PDV: ${purchase.pos?.name ?? 'POS não registrado'}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.text(
      'Venda: ${purchase.id}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );

    bytes += generator.text(
      'Data: ${getDateAndHour(purchase.createdAt.toLocal())}',
      styles: const PosStyles(
        align: PosAlign.center,
      ),
    );
    bytes += generator.hr(ch: '-');

    if (purchase.user != null) {
      bytes += generator.emptyLines(2);
      bytes += generator.text(
        'CPF: CPF DO CLIENTE',
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      );
      bytes += generator.text(
        'Cliente: ${purchase.user?.name}',
        styles: const PosStyles(
          align: PosAlign.center,
        ),
      );
    }

    bytes += generator.feed(2);
    bytes += generator.cut();
    return bytes;
  }

  Future<void> printPurchase(Purchase purchase, {Printer? printer}) async {
    List<int> bytes = [];

    bool printToIp = [printer?.type, printerModel.printerForDelivery?.type]
        .contains(PrinterType.NETWORK);

    if (printer != null && printer.type == PrinterType.LASER ||
        printerModel.printerForPurchase != null &&
            printerModel.printerForPurchase!.type == PrinterType.LASER) {
      var data = await PdfPurchaseUtil.generateForPurchase(purchase);
      try {
        if (printerModel.usePrinterManager) {
          UdpUtil.send(
            PrintRequest(
              type: PrintRequestType.purchase,
              data: data,
              printer: printer ?? printerModel.printerForPurchase!,
              origin: posModel.getSelected(),
            ),
          );
        } else {
          File(
            printer?.path ??
                printerModel.printerForPurchase?.path ??
                printerModel.path,
          ).writeAsBytesSync(data);
        }
      } catch (e) {
        LoggerUtil.logger.e(e);
        throw Exception(e);
      }
      return;
    }

    if (printLayoutModel.printLayoutForPurchase == null) {
      bytes = await getBytesFromPurchase(purchase);
    } else {
      bytes = await printLayoutModel.printLayoutForPurchase!.getData(purchase);
    }

    try {
      if (printerModel.usePrinterManager &&
          printerModel.printerForPurchase != null) {
        UdpUtil.send(
          PrintRequest(
            type: PrintRequestType.purchase,
            data: bytes,
            printer: printer ?? printerModel.printerForPurchase!,
            origin: posModel.getSelected(),
          ),
        );
      } else if (printToIp) {
        return sendToSocket(
          printer ?? printerModel.printerForDelivery!,
          bytes,
        );
      } else {
        File(
          printer?.path ??
              printerModel.printerForPurchase?.path ??
              printerModel.path,
        ).writeAsBytesSync(bytes);
      }
    } catch (e) {
      LoggerUtil.logger.e(e);
      throw Exception(e);
    }
  }

  Future<List<int>> getBytesFromOrder(Purchase order) async {
    final profile = await CapabilityProfile.load();
    final generator = Generator(PaperSize.mm80, profile);

    List<int> bytes = [];

    bytes += generator.setGlobalFont(PosFontType.fontA);

    bytes += generator.row(
      [
        PosColumn(
          text: 'Pedido do delivery: ',
          width: 6,
          styles: const PosStyles(
              bold: true, align: PosAlign.right, height: PosTextSize.size2),
        ),
        PosColumn(
          text: order.id.toString(),
          width: 6,
          styles:
              const PosStyles(align: PosAlign.left, height: PosTextSize.size2),
        )
      ],
    );

    bytes += generator.emptyLines(1);

    bytes += generator.text(
      getDateAndHour(order.createdAt),
      styles: const PosStyles(align: PosAlign.center),
    );

    bytes += generator.setGlobalFont(PosFontType.fontB);

    bytes += generator.hr();

    bytes += generator.row([
      PosColumn(
          text: 'Produto',
          styles: const PosStyles(
            bold: true,
            align: PosAlign.left,
          ),
          width: 8),
      PosColumn(
        text: 'Quantidade',
        styles: const PosStyles(
          bold: true,
          align: PosAlign.right,
        ),
        width: 4,
      )
    ]);

    bytes += generator.hr();

    for (var product in order.products) {
      bytes += generator.row([
        PosColumn(
          text: product.name,
          styles: const PosStyles(align: PosAlign.left),
          width: 8,
        ),
        PosColumn(
          text: getNumber(product.amount),
          width: 4,
          styles: const PosStyles(
            align: PosAlign.right,
          ),
        )
      ]);
    }

    bytes += generator.feed(2);

    bytes += generator.cut();
    return bytes;
  }

  Future<void> printOrder(Purchase order, {Printer? printer}) async {
    var prefs = await SharedPreferences.getInstance();
    final path = prefs.getString('printerPath') ?? '';

    List<int> bytes = [];

    bool printToIp = [printer?.type, printerModel.printerForDelivery?.type]
        .contains(PrinterType.NETWORK);

    if (printLayoutModel.printLayoutForDelivery != null) {
      bytes = await printLayoutModel.printLayoutForDelivery!.getData(order);
    } else {
      bytes = await getBytesFromOrder(order);
    }

    try {
      if (printerModel.usePrinterManager &&
          printerModel.printerForDelivery != null) {
        UdpUtil.send(
          PrintRequest(
            type: PrintRequestType.order,
            data: bytes,
            printer: printer ?? printerModel.printerForDelivery!,
            origin: posModel.getSelected(),
          ),
        );
      } else if (printToIp) {
        return sendToSocket(
          printer ?? printerModel.printerForDelivery!,
          bytes,
        );
      } else {
        File(printer?.path ?? printerModel.printerForDelivery?.path ?? path)
            .writeAsBytes(bytes);
      }
    } catch (e) {
      LoggerUtil.logger.e(e);
    }
  }

  Future<void> internalPrinter() async {
    throw Exception('Not implemented');
  }

  Future<void> sendToSocket(Printer printer, List<int> data) async {
    var ipAndPort = printer.path.split(':');

    if (ipAndPort.length == 2) {
      String ip = ipAndPort[0];
      int port = int.parse(ipAndPort[1]);
      try {
        var socket = await Socket.connect(ip, port);
        socket.add(data);
        await socket.close();
      } catch (e) {
        LoggerUtil.logger.e(e);
        throw Exception(e);
      }
    } else {
      throw Exception('Invalid address, use the IP and Port separated by ":"');
    }
  }
}
