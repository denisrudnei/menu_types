import 'dart:io';

import 'package:menu_types/drift/drift_phone_model.dart';
import 'package:menu_types/drift/drift_pos_model.dart';
import 'package:menu_types/drift/drift_printer_model.dart';
import 'package:menu_types/drift/drift_product_model.dart';
import 'package:menu_types/drift/drift_user_model.dart';
import 'package:menu_types/drift/drift_history_product_model.dart';
import 'package:menu_types/drift/drift_payment_model.dart';
import 'package:menu_types/drift/drift_purchase_model.dart';
import 'package:path/path.dart' as p;
import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';

part 'my_database.g.dart';

@DriftDatabase(tables: [
  DriftProductModel,
  DriftUserModel,
  DriftPurchaseModel,
  DriftHistoryProductModel,
  DriftPaymentModel,
  DriftPOSModel,
  DriftPrinterModel,
  DriftPhoneModel,
])
LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [
  DriftProductModel,
  DriftUserModel,
  DriftPurchaseModel,
  DriftHistoryProductModel,
  DriftPaymentModel,
  DriftPOSModel,
  DriftPrinterModel,
  DriftPhoneModel,
])
class MyDatabase extends _$MyDatabase {
  MyDatabase() : super(_openConnection());

  MyDatabase.test(QueryExecutor e) : super(e);

  @override
  int get schemaVersion => 10;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        onCreate: (Migrator migrator) {
          return migrator.createAll();
        },
        onUpgrade: (Migrator migrator, int from, int to) async {
          if (from == 1) {
            await migrator.createTable(driftUserModel);
            await migrator.addColumn(driftUserModel, driftUserModel.active);
          }
          if (from == 2) {
            await migrator.addColumn(driftUserModel, driftUserModel.updatedAt);
            await migrator.addColumn(driftUserModel, driftUserModel.deletedAt);
          }
          if (from == 3) {
            await migrator.addColumn(
                driftProductModel, driftProductModel.barcode);
          }
          if (from == 4) {
            await migrator.createTable(driftPurchaseModel);
            await migrator.createTable(driftHistoryProductModel);
            await migrator.createTable(driftPaymentModel);
          }
          if (from == 5) {
            await migrator.createTable(driftPOSModel);
            await migrator.addColumn(
                driftPurchaseModel, driftPurchaseModel.posId);
          }
          if (from == 6) {
            await migrator.createTable(driftPrinterModel);
          }
          if (from == 7) {
            await migrator.addColumn(
                driftProductModel, driftProductModel.deletedAt);

            await migrator.addColumn(driftPOSModel, driftPOSModel.deletedAt);
          }
          if (from == 8) {
            await migrator.alterTable(
              TableMigration(driftPOSModel, columnTransformer: {
                driftPOSModel.id: driftPOSModel.id.cast(),
              }),
            );
            await migrator.alterTable(
              TableMigration(driftPrinterModel, columnTransformer: {
                driftPrinterModel.installedInId:
                    driftPrinterModel.installedInId.cast(),
              }),
            );
            await migrator.alterTable(
              TableMigration(driftPurchaseModel, columnTransformer: {
                driftPurchaseModel.posId: driftPurchaseModel.posId.cast(),
              }),
            );
          }
          if (from == 9) {
            await migrator.addColumn(driftPOSModel, driftPOSModel.hostname);
          }
        },
      );
}
