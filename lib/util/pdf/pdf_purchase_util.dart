import 'dart:typed_data';

import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/util/pdf/styles.dart';
import 'package:menu_types/util/screen/color.dart';
import 'package:menu_types/util/format_utils.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

enum SortProductPrint {
  name,
  price,
  amount,
  discount,
  total,
}

class PdfPurchaseUtil {
  static Future<Uint8List> generateForList(List<Purchase> purchases) async {
    var pdf = pw.Document();

    pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return [
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.center,
              children: [
                pw.Text(
                  'Vendas',
                  textAlign: pw.TextAlign.center,
                  style: pw.TextStyle(
                    fontSize: 18,
                    color: PdfColor.fromInt(hexColor),
                  ),
                ),
              ],
            ),
            pw.SizedBox(height: 25),
            _getPurchasesTable(purchases),
          ];
        },
        footer: (pw.Context context) {
          return pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.end,
            children: [
              pw.Text(
                getDateAndHour(DateTime.now()),
                style: pw.TextStyle(
                  fontSize: 10,
                  color: PdfColor.fromInt(hexColor),
                ),
              )
            ],
          );
        },
      ),
    );
    return pdf.save();
  }

  static String getByPurchaseIndex(Purchase purchase, int index) {
    switch (index) {
      case 0:
        return purchase.id.toString();
      case 1:
        return getCurrency(purchase.totalPrice);
      case 2:
        return getDateAndHour(purchase.createdAt);
      case 3:
        return purchase.operator?.name ?? '';
      case 4:
        return purchase.user?.name ?? '';
      default:
        return '';
    }
  }

  static pw.Table _getPurchasesTable(List<Purchase> purchases) {
    List<String> headers = [
      '#',
      'Valor',
      'Data',
      'Operador',
      'Cliente',
    ];
    return pw.Table.fromTextArray(
      border: null,
      cellAlignment: pw.Alignment.center,
      cellStyle: const pw.TextStyle(fontSize: 10),
      headerHeight: 25,
      cellHeight: 40,
      headerStyle: pw.TextStyle(
        color: PdfColor.fromInt(hexColor),
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      rowDecoration: const pw.BoxDecoration(
        border: pw.Border(
          bottom: pw.BorderSide(
            color: PdfColors.blueGrey800,
            width: .5,
          ),
        ),
      ),
      headers: headers,
      data: List<List<String>>.generate(
        purchases.length,
        (row) => List<String>.generate(
          headers.length,
          (col) => getByPurchaseIndex(purchases[row], col),
        ),
      ),
    );
  }

  static Future<Uint8List> generateForPurchase(
    Purchase purchase, {
    SortProductPrint? sortType,
  }) async {
    var pdf = pw.Document();

    var normalStyle = pw.TextStyle(
      fontSize: 10,
      color: PdfColor.fromInt(
        hexColor,
      ),
    );

    pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return [
            pw.Column(
              mainAxisAlignment: pw.MainAxisAlignment.start,
              crossAxisAlignment: pw.CrossAxisAlignment.stretch,
              children: [
                pw.Row(
                  children: [
                    pw.Text(
                      'Venda: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text('${purchase.id}', style: normalStyle),
                    pw.Spacer(),
                    pw.Text(
                      'Data: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text(
                      getDateAndHour(purchase.createdAt.toLocal()),
                      style: normalStyle,
                    ),
                  ],
                ),
                pw.Divider(),
                pw.Row(
                  children: [
                    pw.Text(
                      'Cliente: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text(
                      purchase.user?.name ?? 'Sem cliente',
                      style: normalStyle,
                    ),
                    pw.Spacer(),
                    pw.Text(
                      'Operador: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text(
                      purchase.operator?.name ?? 'Sem Operador informado',
                      style: normalStyle,
                    ),
                  ],
                ),
                pw.Divider(),
                pw.Row(
                  children: [
                    pw.Text(
                      'Itens: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text(
                      purchase.totalAmount.toString(),
                      style: normalStyle,
                    ),
                    pw.Spacer(),
                    pw.Text(
                      'Total: ',
                      textAlign: pw.TextAlign.left,
                      style: boldStyle,
                    ),
                    pw.Text(
                      getCurrency(purchase.totalPrice),
                      style: normalStyle,
                    ),
                  ],
                ),
                pw.Divider(),
                pw.SizedBox(height: 20),
                pw.Text(
                  'Produtos',
                  textAlign: pw.TextAlign.center,
                  style: pw.TextStyle(
                    fontSize: 18,
                    color: PdfColor.fromInt(hexColor),
                  ),
                ),
              ],
            ),
            pw.SizedBox(height: 15),
            _getProductsTable(purchase.products, sortType: sortType),
            pw.SizedBox(height: 20),
            pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.center,
              children: [
                pw.Text(
                  'Pagamentos',
                  textAlign: pw.TextAlign.center,
                  style: pw.TextStyle(
                    fontSize: 18,
                    color: PdfColor.fromInt(hexColor),
                  ),
                ),
              ],
            ),
            pw.SizedBox(height: 15),
            _getPaymentsTable(purchase.payments),
          ];
        },
        footer: (pw.Context context) {
          return pw.Row(
            mainAxisAlignment: pw.MainAxisAlignment.end,
            children: [
              pw.Text(
                getDateAndHour(DateTime.now()),
                style: pw.TextStyle(
                  fontSize: 10,
                  color: PdfColor.fromInt(hexColor),
                ),
              )
            ],
          );
        },
      ),
    );

    return pdf.save();
  }

  static String getByProductIndex(Product product, int index) {
    switch (index) {
      case 0:
        return product.name;
      case 1:
        return getCurrency(product.price);
      case 2:
        return getNumber(product.amount);
      case 3:
        return '${getNumber(product.discount)}%';
      case 4:
        return getCurrency(product.total);
      default:
        return '';
    }
  }

  static String getByPaymentIndex(Payment payment, int index) {
    switch (index) {
      case 0:
        return payment.type;
      case 1:
        return getCurrency(payment.value);
      default:
        return '';
    }
  }

  static pw.Table _getPaymentsTable(List<Payment> payments) {
    List<String> headers = [
      'Nome',
      'Valor',
    ];
    return pw.Table.fromTextArray(
      border: null,
      cellAlignment: pw.Alignment.center,
      cellStyle: const pw.TextStyle(
        fontSize: 10,
      ),
      headerHeight: 25,
      cellHeight: 40,
      headerStyle: pw.TextStyle(
        color: PdfColor.fromInt(hexColor),
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      rowDecoration: const pw.BoxDecoration(
        border: pw.Border(
          bottom: pw.BorderSide(
            color: PdfColors.blueGrey800,
            width: .5,
          ),
        ),
      ),
      headers: List<String>.generate(
        headers.length,
        (col) => headers[col],
      ),
      data: List<List<String>>.generate(
        payments.length,
        (row) => List<String>.generate(
          headers.length,
          (col) => getByPaymentIndex(payments[row], col),
        ),
      ),
    );
  }

  static pw.Table _getProductsTable(
    List<Product> products, {
    SortProductPrint? sortType,
  }) {
    List<String> headers = [
      'Nome',
      'Preço',
      'Quantidade',
      'Desconto',
      'Total',
    ];

    if (sortType != null) {
      switch (sortType) {
        case SortProductPrint.name:
          products.sort((a, b) => a.name.compareTo(b.name));
          break;
        case SortProductPrint.price:
          products.sort((a, b) => a.price.compareTo(b.price));
          break;
        case SortProductPrint.amount:
          products.sort((a, b) => a.amount.compareTo(b.amount));
          break;
        case SortProductPrint.discount:
          products.sort((a, b) => a.discount.compareTo(b.discount));
          break;
        case SortProductPrint.total:
          products.sort((a, b) => a.total.compareTo(b.total));
      }
    }

    return pw.Table.fromTextArray(
      border: null,
      cellAlignment: pw.Alignment.center,
      cellStyle: const pw.TextStyle(
        fontSize: 10,
      ),
      headerHeight: 25,
      cellHeight: 40,
      cellAlignments: {
        0: pw.Alignment.centerLeft,
        1: pw.Alignment.centerLeft,
        2: pw.Alignment.centerRight,
        3: pw.Alignment.center,
        4: pw.Alignment.centerRight,
      },
      columnWidths: {
        0: const pw.IntrinsicColumnWidth(flex: 3),
        1: const pw.IntrinsicColumnWidth(),
        2: const pw.IntrinsicColumnWidth(),
        3: const pw.IntrinsicColumnWidth(),
        4: const pw.IntrinsicColumnWidth(),
      },
      headerStyle: pw.TextStyle(
        color: PdfColor.fromInt(hexColor),
        fontSize: 10,
        fontWeight: pw.FontWeight.bold,
      ),
      rowDecoration: const pw.BoxDecoration(
        border: pw.Border(
          bottom: pw.BorderSide(
            color: PdfColors.blueGrey800,
            width: .5,
          ),
        ),
      ),
      headers: List<String>.generate(
        headers.length,
        (col) => headers[col],
      ),
      data: List<List<String>>.generate(
        products.length,
        (row) => List<String>.generate(
          headers.length,
          (col) => getByProductIndex(products[row], col),
        ),
      ),
    );
  }
}
