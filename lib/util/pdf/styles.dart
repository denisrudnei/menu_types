import 'package:menu_types/util/screen/color.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

var boldStyle = pw.TextStyle(
  fontWeight: pw.FontWeight.bold,
  fontSize: 10,
  color: PdfColor.fromInt(
    hexColor,
  ),
);
