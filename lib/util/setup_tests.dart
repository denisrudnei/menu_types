import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:menu_types/models/auth_model.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/auth_repository.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/pos_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

var authRepository = AuthRepository(model: AuthModel());

class SetupTests {
  static MyDatabase? _db;
  static MyDatabase get db {
    driftRuntimeOptions.dontWarnAboutMultipleDatabases = true;
    _db ??= MyDatabase.test(NativeDatabase.memory());

    return _db!;
  }

  static Future<void> savePostgresInfo() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('db-url', Platform.environment['DB_HOSTNAME'] ?? '0.0.0.0');
    prefs.setString('db-name', Platform.environment['DB_NAME'] ?? 'postgres');
    MyDatabase db = SetupTests.db;
    DbConnectionUtil dbConnectionUtil = DbConnectionUtil(
      model: DbModel(db: db),
      logModel: LogModel(),
      loadModel: LoadModel(),
    );
    await dbConnectionUtil.createTables();
  }

  static Future<POS> configurePOS() async {
    var posRepository = POSRepository(
      db: SetupTests.db,
      deviceInfo: DeviceInfo(),
    );
    var allPos = await posRepository.getPOS();
    if (allPos.isEmpty) {
      var pos = POS();
      pos.name = 'test POS';
      await posRepository.save(pos);
    }
    var configured = (await posRepository.getPOS()).first;
    (await SharedPreferences.getInstance()).setString('pos', configured.id);
    return configured;
  }

  static Future<void> saveApiInfo() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('urlBase', 'https://shop-dns.herokuapp.com/');
    prefs.setString('username', 'operador');
    prefs.setString('password', 'operador');
    prefs.setString('graphql', 'https://shop-dns.herokuapp.com/graphql');
    prefs.setString(
        'subscription', 'https://shop-dns.herokuapp.com/subscriptions');
    prefs.setString('token',
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6Im9wZXJhZG9yIiwiZW1haWwiOiJvcGVyYWRvciIsInJvbGUiOiJPUEVSQVRPUiIsImRhcmtUaGVtZSI6ZmFsc2UsImlhdCI6MTY0MTU1NTE3NiwiZXhwIjoxNjQxNTU2OTc2fQ.j_TAKxZ9gTIDoOq8H-TXhaiAd8_ev1TyEvnoVfxRmLE');
  }

  static Future<void> bootstrapTests() async {
    (await SharedPreferences.getInstance()).clear();
    await saveApiInfo();
    await savePostgresInfo();
    await configurePOS();
    DbConnectionUtil dbConnectionUtil = DbConnectionUtil(
      model: DbModel(db: SetupTests.db),
      logModel: LogModel(),
      loadModel: LoadModel(),
    );
    await dbConnectionUtil.downloadInfo();
  }

  static Future<void> login() async {
    await authRepository.login('operador', 'operador');
  }
}
