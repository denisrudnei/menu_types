import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';

abstract class LoadServiceInterface<ItemType, IdType> {
  late DbModel dbModel;
  late LoadLogRepository loadLogRepository;
  late DbConnectionUtil dbConnectionUtil;
  List<ItemType> data = [];
  List<IdType> deletedData = [];
  bool isDownloading = false;
  bool isDeleting = false;
  String text = '';
  String deletingText = '';
  late Icon icon;
  double percentage = 0;

  void populate(List<ItemType> data);
  void populateDeleted(List<IdType> deletedData);

  Future<void> saveData();
  Future<void> deleteData();

  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
