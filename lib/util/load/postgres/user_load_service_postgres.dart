import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:postgres/postgres.dart';

class UserLoadServicePostgres implements LoadServiceInterface<User, int> {
  @override
  List<User> data = [];

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<int> deletedData = [];

  @override
  String deletingText = '';

  @override
  Icon icon = const Icon(Icons.person);

  @override
  bool isDeleting = false;

  @override
  bool isDownloading = false;

  @override
  LoadLogRepository loadLogRepository;

  @override
  double percentage = 0;

  @override
  String text = '';

  UserLoadServicePostgres({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  });

  @override
  Future<void> deleteData() async {
    if (deletedData.isEmpty) return;

    String sql = '''
      UPDATE public.user
      SET "deletedAt" = @deletedAt
      WHERE id IN (${deletedData.join(',')})
    ''';

    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();
    await connection.execute(
      sql,
      substitutionValues: {
        'deletedAt': DateTime.now().toIso8601String(),
      },
    );
    await connection.close();
  }

  @override
  void populate(List<User> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<int> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String insertUsers = r'''
      INSERT INTO public.user(id, name, email, image, password, "tempPassword")
      VALUES (@id, @name, @email, @image, @password, @tempPassword) ON CONFLICT (id) DO
      UPDATE
      SET name = @name,
          email = @email,
          image = @image,
          password = @password
      ''';
    await connection.open();
    for (var i = 0; i < data.length; i++) {
      var user = data[i];
      text = 'Updating user ${user.name}';
      percentage = (i / data.length) * 100;
      dbModel.updateService(this);

      await connection.query(insertUsers, substitutionValues: {
        'id': user.id,
        'name': user.name,
        'email': user.email,
        'image': user.image,
        'password': '',
        'tempPassword': ''
      });
    }
    await connection.close();
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
