import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_log_type.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:postgres/postgres.dart';

class POSLoadServicePostgres implements LoadServiceInterface<POS, String> {
  @override
  List<POS> data = [];

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<String> deletedData = [];

  @override
  Icon icon = const Icon(Icons.point_of_sale);

  @override
  LoadLogRepository loadLogRepository;

  @override
  bool isDownloading = false;

  @override
  bool isDeleting = false;

  @override
  String text = '';

  @override
  String deletingText = '';

  @override
  double percentage = 0;

  POSLoadServicePostgres({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  });

  @override
  Future<void> deleteData() async {
    if (deletedData.isEmpty) return;

    String sql = '''
      DELETE FROM pos WHERE id IN (${deletedData.map((e) => '\'$e\'').join(',')})
    ''';

    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();
    await connection.execute(sql);
    await connection.close();
  }

  @override
  void populate(List<POS> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<String> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    String insertPOS = r'''
      INSERT INTO pos(id, name)
      VALUES (@id, @name) ON CONFLICT (id) DO
      UPDATE
      SET name = @name
    ''';

    await connection.open();

    for (var i = 0; i < data.length; i++) {
      text = 'Atualizando POS (${i + 1} de ${data.length})';

      try {
        await connection.query(insertPOS, substitutionValues: {
          'id': data[i].id,
          'name': data[i].name,
        });
      } catch (e) {
        String posFailed = 'Falha ao atualizar POS';
        text = posFailed;
        dbModel.setStatus(LoadStatus.error);
        LoggerUtil.logger.e(e);
        await loadLogRepository.saveLoadLog(
          posFailed,
          type: LoadLogType.ERROR,
        );
      }
    }
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
