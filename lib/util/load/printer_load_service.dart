import 'dart:io';

import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:postgres/postgres.dart';

class PrinterLoadService implements LoadServiceInterface<Printer, String> {
  @override
  List<Printer> data = [];

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<String> deletedData = [];

  @override
  LoadLogRepository loadLogRepository;

  @override
  bool isDownloading = false;

  @override
  bool isDeleting = false;

  @override
  String text = '';

  @override
  String deletingText = '';

  @override
  double percentage = 0;

  @override
  Icon icon = const Icon(Icons.print);

  PrinterLoadService({
    required this.dbConnectionUtil,
    required this.loadLogRepository,
    required this.dbModel,
  });

  @override
  Future<void> deleteData() async {
    isDeleting = true;
    deletingText = 'Removendo impressoras';
    dbModel.updateService(this);
    if (Platform.isAndroid || Platform.isIOS) {
      await deletePrintersSqlite(deletedData);
    } else {
      await deletePrintersPostgres(deletedData);
    }
    deletingText = 'Impressoras removidas no servidor (${deletedData.length})';
    isDeleting = false;
    dbModel.updateService(this);
  }

  @override
  void populate(List<Printer> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<String> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    isDownloading = true;
    text = 'Atualizando impressoras';
    if (Platform.isAndroid || Platform.isIOS) {
      await updatePrintersSqlite(data);
    } else {
      await updatePrintersPostgres(data);
    }
    text = 'Impressoras atualizadas';

    isDownloading = false;
    dbModel.updateService(this);
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }

  Future<void> updatePrintersSqlite(List<Printer> printers) async {
    var db = dbModel.db;
    await db.transaction(() async {
      for (var printer in printers) {
        text = 'Atualizando impressora ${printer.name}';
        dbModel.updateService(this);
        try {
          var value = DriftPrinterModelCompanion.insert(
            id: printer.id,
            name: printer.name,
            manufacturer: printer.manufacturer,
            model: printer.model,
            type: printer.type.name,
            path: printer.path,
            installedInId: printer.installedIn.id,
            createdAt: DateTime.now(),
            updatedAt: DateTime.now(),
          );
          await db.into(db.driftPrinterModel).insert(
                value,
                onConflict: DoUpdate(
                  (_) => value,
                  target: [db.driftPrinterModel.id],
                ),
              );
        } catch (e) {
          LoggerUtil.logger.e(e);
          text = 'Falha durante atualização das impressoras';

          isDownloading = false;
          dbModel.updateService(this);
        }
      }
    });
  }

  Future<void> updatePrintersPostgres(List<Printer> printers) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    String sql = '''
      INSERT INTO printer(
        id,
        name,
        path,
        manufacturer,
        model,
        type,
        "installedInId"
      )
      VALUES(
        @id,
        @name,
        @path,
        @manufacturer,
        @model,
        @type,
        @installedIn
      )
      ON CONFLICT (id) DO
      UPDATE
      SET name = @name,
      path = @path,
      manufacturer = @manufacturer,
      model = @model,
      type = @type,
      "installedInId" = @installedIn
     ''';
    try {
      await connection.transaction((tx) async {
        for (var printer in printers) {
          text = 'Atualizando impressora ${printer.name}';
          dbModel.updateService(this);
          await tx.query(sql, substitutionValues: {
            'id': printer.id,
            'name': printer.name,
            'path': printer.path,
            'manufacturer': printer.manufacturer,
            'model': printer.model,
            'type': printer.type.name,
            'installedIn': printer.installedIn.id,
          });
        }
      });
    } catch (e) {
      text = 'Falha durante atualização das impressoras';
      isDownloading = false;
      dbModel.updateService(this);
    }

    await connection.close();
  }

  Future<void> deletePrintersSqlite(List<String> deletedPrinters) async {
    final db = dbModel.db;
    await db.transaction(() async {
      for (var printer in deletedPrinters) {
        await (db.delete(db.driftPrinterModel)
              ..where((tbl) => tbl.id.equals(printer)))
            .go();
      }
    });
  }

  Future<void> deletePrintersPostgres(List<String> deletedPrinters) async {
    if (deletedPrinters.isEmpty) return;
    String sqlIds = deletedPrinters.map((id) => "'{$id}'").join(',');
    String sql = '''
      DELETE FROM printer WHERE id IN ($sqlIds)
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();
    await connection.execute(sql);
    await connection.close();
  }
}
