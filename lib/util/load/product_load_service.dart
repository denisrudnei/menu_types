import 'dart:io';

import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:postgres/postgres.dart';

class ProductLoadService implements LoadServiceInterface<Product, int> {
  @override
  DbModel dbModel;

  @override
  LoadLogRepository loadLogRepository;

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  List<Product> data = [];

  @override
  List<int> deletedData = [];

  @override
  bool isDownloading = false;

  @override
  bool isDeleting = false;

  @override
  String text = '';

  @override
  String deletingText = '';

  @override
  double percentage = 0;

  @override
  Icon icon = const Icon(Icons.shopping_cart_outlined);

  ProductLoadService({
    required this.dbModel,
    required this.loadLogRepository,
    required this.dbConnectionUtil,
  });

  @override
  void populate(List<Product> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<int> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> deleteData() async {
    isDeleting = true;
    deletingText = 'Removendo produtos';
    dbModel.updateService(this);

    if (Platform.isAndroid || Platform.isIOS) {
      await deleteProductsSqlite(deletedData);
    } else {
      await deleteProductsPostgres(deletedData);
    }

    deletingText = 'Produtos  no servidor (${deletedData.length})';
    isDeleting = false;
    dbModel.updateService(this);
  }

  @override
  Future<void> saveData() async {
    isDownloading = true;

    text = 'Inserindo produtos no banco';
    dbModel.updateService(this);

    if (Platform.isAndroid || Platform.isIOS) {
      await insertProductsSqlite(data);
    } else {
      await insertProductsPostgres(data);
    }

    String updatedProducts = 'Produtos atualizados';
    await loadLogRepository.saveLoadLog(updatedProducts);
    text = updatedProducts;
    isDownloading = false;
    dbModel.updateService(this);
  }

  Future insertProductsSqlite(List<Product> products) async {
    var db = dbModel.db;
    await db.transaction(() async {
      for (var i = 0; i < products.length; i++) {
        var product = products[i];
        text = 'Atualizando produtos (${i + 1} de ${products.length})';
        percentage = (i / products.length) * 100;
        dbModel.updateService(this);

        await db.into(db.driftProductModel).insertOnConflictUpdate(
              DriftProductModelCompanion.insert(
                id: Value(product.id),
                name: product.name,
                description: Value(product.description),
                amount: Value(product.amount),
                barcode: Value(product.barcode),
                price: Value(product.price),
              ),
            );
      }
    });
  }

  Future insertProductsPostgres(List<Product> products) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String insertProducts = r'''
      INSERT INTO product(id, name, description, price, amount, type, barcode)
      VALUES (@id, @name, @description, @price, @amount, @type, @barcode) ON CONFLICT (id) DO
      UPDATE
      SET name = @name,
          description = @description,
          price = @price,
          amount = @amount,
          type = @type,
          barcode = @barcode
      ''';

    await connection.open();
    for (var i = 0; i < products.length; i++) {
      text = 'Atualizando produtos (${i + 1} de ${products.length})';
      percentage = (i / products.length) * 100;
      dbModel.updateService(this);
      await connection.query(insertProducts, substitutionValues: {
        'id': products[i].id,
        'name': products[i].name,
        'description': products[i].description,
        'price': products[i].price,
        'amount': products[i].amount,
        'type': products[i].getTypeAsPostgresString(),
        'barcode': products[i].barcode
      });
    }
    await connection.close();
  }

  @override
  Future<void> updateStatusOnError(Error error) async {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }

  Future<void> deleteProductsSqlite(List<int> products) async {
    final db = dbModel.db;
    await db.transaction(() async {
      for (var id in products) {
        await (db.delete(db.driftProductModel)
              ..where((tbl) => tbl.id.equals(id)))
            .go();
      }
    });
  }

  Future<void> deleteProductsPostgres(List<int> products) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      DELETE FROM product WHERE id IN (@ids)
    ''';
    if (products.isEmpty) return;

    await connection.open();

    await connection.execute(
      sql,
      substitutionValues: {
        'ids': products.join(','),
      },
    );

    await connection.close();
  }
}
