import 'dart:io';

import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/load/postgres/user_load_service_postgres.dart';
import 'package:menu_types/util/load/sqlite/user_load_service_sqlite.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/service_interface.dart';

class UserLoadService
    implements
        LoadServiceInterface<User, int>,
        ServiceInterface<LoadServiceInterface<User, int>> {
  @override
  late LoadServiceInterface<User, int> implementation;

  @override
  Future<void> setImplementation() async {
    if (Platform.isAndroid || Platform.isIOS) {
      implementation = UserLoadServiceSqlite(
        dbConnectionUtil: dbConnectionUtil,
        dbModel: dbModel,
        loadLogRepository: loadLogRepository,
      );
    } else {
      implementation = UserLoadServicePostgres(
        dbConnectionUtil: dbConnectionUtil,
        dbModel: dbModel,
        loadLogRepository: loadLogRepository,
      );
    }
  }

  @override
  List<User> get data => implementation.data;

  @override
  set data(List<User> _data) {
    implementation.data = _data;
  }

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<int> get deletedData => implementation.deletedData;

  @override
  set deletedData(List<int> _deletedData) {
    implementation.deletedData = _deletedData;
  }

  @override
  LoadLogRepository loadLogRepository;

  @override
  bool get isDownloading => implementation.isDownloading;

  @override
  set isDownloading(bool _isDownloading) {
    implementation.isDownloading = _isDownloading;
  }

  @override
  bool get isDeleting => implementation.isDeleting;

  @override
  set isDeleting(bool _isDeleting) {
    implementation.isDeleting = _isDeleting;
  }

  @override
  String get text => implementation.text;

  @override
  set text(String _text) {
    implementation.text = _text;
  }

  @override
  String get deletingText => implementation.deletingText;

  @override
  set deletingText(String _deletingText) {
    implementation.deletingText = _deletingText;
  }

  @override
  double get percentage => implementation.percentage;

  @override
  set percentage(double _percentage) {
    implementation.percentage = _percentage;
  }

  @override
  Icon icon = const Icon(Icons.person);

  UserLoadService({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  }) {
    setImplementation().then((value) {
      LoggerUtil.logger
          .i('Configured ${implementation.runtimeType} for UserLoadService');
    });
  }

  @override
  Future<void> deleteData() async {
    isDeleting = true;
    deletingText = 'Removendo usuários';
    dbModel.updateService(this);
    await implementation.deleteData();
    deletingText = 'Usuários removidos no servidor (${deletedData.length})';
    isDeleting = false;
    dbModel.updateService(this);
  }

  @override
  void populate(List<User> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<int> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    isDownloading = true;
    await loadLogRepository.saveLoadLog('Iniciando carga de usuários');

    text = 'Atualizando usuários';
    dbModel.updateService(this);

    await implementation.saveData();
    String usersUpdated = 'Usuários atualizados';
    text = usersUpdated;
    isDownloading = false;
    dbModel.updateService(this);
    await loadLogRepository.saveLoadLog(usersUpdated);
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
