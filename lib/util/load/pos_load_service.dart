import 'dart:io';

import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/load/postgres/pos_load_service_postgres.dart';
import 'package:menu_types/util/load/sqlite/pos_load_service_sqlite.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/service_interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

class POSLoadService
    implements
        LoadServiceInterface<POS, String>,
        ServiceInterface<LoadServiceInterface<POS, String>> {
  @override
  late LoadServiceInterface<POS, String> implementation;

  @override
  Future<void> setImplementation() async {
    if (Platform.isAndroid || Platform.isIOS) {
      implementation = POSLoadServiceSqlite(
        dbConnectionUtil: dbConnectionUtil,
        dbModel: dbModel,
        loadLogRepository: loadLogRepository,
      );
    } else {
      implementation = POSLoadServicePostgres(
        dbConnectionUtil: dbConnectionUtil,
        dbModel: dbModel,
        loadLogRepository: loadLogRepository,
      );
    }
  }

  @override
  List<POS> get data => implementation.data;

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<String> get deletedData => implementation.deletedData;

  @override
  LoadLogRepository loadLogRepository;

  @override
  bool get isDownloading => implementation.isDownloading;

  @override
  bool get isDeleting => implementation.isDeleting;

  @override
  String get text => implementation.text;

  @override
  String get deletingText => implementation.deletingText;

  @override
  double get percentage => implementation.percentage;

  @override
  Icon icon = const Icon(Icons.point_of_sale);

  POSLoadService({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  }) {
    setImplementation();
  }

  @override
  Future<void> deleteData() async {
    isDeleting = true;
    deletingText = 'Removendo POS';
    dbModel.updateService(this);
    await implementation.deleteData();
    deletingText = 'POS removidos no servidor (${deletedData.length})';
    isDeleting = false;
    dbModel.updateService(this);
  }

  @override
  void populate(List<POS> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<String> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    await implementation.saveData();

    isDownloading = true;

    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('pos') == null && data.isNotEmpty) {
      prefs.setString('pos', data.first.id);
    }

    text = 'Inserindo POS no banco';

    String updatedPOS = 'POS atualizados';

    try {
      await loadLogRepository.saveLoadLog(updatedPOS);
    } catch (e) {
      LoggerUtil.logger.e(e);
    }
    text = updatedPOS;
    isDownloading = false;
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }

  @override
  set data(List<POS> _data) {
    implementation.data = _data;
  }

  @override
  set deletedData(List<String> _deletedData) {
    implementation.deletedData = _deletedData;
  }

  @override
  set isDeleting(bool _isDeleting) {
    implementation.isDeleting = _isDeleting;
  }

  @override
  set isDownloading(bool _isDownloading) {
    implementation.isDownloading = _isDownloading;
  }

  @override
  set percentage(double _percentage) {
    implementation.percentage = _percentage;
  }

  @override
  set deletingText(String _deletingText) {
    implementation.deletingText = _deletingText;
  }

  @override
  set text(String _text) {
    implementation.text = _text;
  }
}
