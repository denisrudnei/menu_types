import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/my_database.dart';

class POSLoadServiceSqlite implements LoadServiceInterface<POS, String> {
  @override
  List<POS> data = [];

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<String> deletedData = [];

  @override
  String deletingText = '';

  @override
  Icon icon = const Icon(Icons.point_of_sale);

  @override
  bool isDeleting = false;

  @override
  bool isDownloading = false;

  @override
  LoadLogRepository loadLogRepository;

  @override
  double percentage = 0;

  @override
  String text = '';

  POSLoadServiceSqlite({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  });

  @override
  Future<void> deleteData() async {
    final db = dbModel.db;
    await db.transaction(() async {
      for (var pos in deletedData) {
        await (db.delete(db.driftPOSModel)..where((tbl) => tbl.id.equals(pos)))
            .go();
      }
    });
  }

  @override
  void populate(List<POS> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<String> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    var db = dbModel.db;
    await db.transaction(
      () async {
        for (var item in data) {
          var value = DriftPOSModelCompanion.insert(
            id: Value(item.id),
            name: item.name,
            createdAt: item.createdAt ?? DateTime.now(),
            updatedAt: item.updatedAt ?? DateTime.now(),
          );
          await db.into(db.driftPOSModel).insert(
                value,
                onConflict: DoUpdate(
                  (_) => value,
                  target: [db.driftPOSModel.id],
                ),
              );
        }
      },
    );
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
