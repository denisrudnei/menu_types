import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/my_database.dart';

class UserLoadServiceSqlite implements LoadServiceInterface<User, int> {
  @override
  List<User> data = [];

  @override
  DbConnectionUtil dbConnectionUtil;

  @override
  DbModel dbModel;

  @override
  List<int> deletedData = [];

  @override
  String deletingText = '';

  @override
  Icon icon = const Icon(Icons.person);

  @override
  bool isDeleting = false;

  @override
  bool isDownloading = false;

  @override
  LoadLogRepository loadLogRepository;

  @override
  double percentage = 0;

  @override
  String text = '';

  UserLoadServiceSqlite({
    required this.dbConnectionUtil,
    required this.dbModel,
    required this.loadLogRepository,
  });

  @override
  Future<void> deleteData() async {
    final db = dbModel.db;

    await db.transaction(() async {
      for (var user in deletedData) {
        await (db.delete(db.driftUserModel)
              ..where((tbl) => tbl.id.equals(user)))
            .go();
      }
    });
  }

  @override
  void populate(List<User> data) {
    this.data = data;
  }

  @override
  void populateDeleted(List<int> deletedData) {
    this.deletedData = deletedData;
  }

  @override
  Future<void> saveData() async {
    var db = dbModel.db;
    await db.transaction(() async {
      for (var i = 0; i < data.length; i++) {
        var user = data[i];
        text = 'Updating user ${user.name}';
        percentage = (i / data.length) * 100;
        dbModel.updateService(this);
        await db.into(db.driftUserModel).insertOnConflictUpdate(
              DriftUserModelCompanion.insert(
                id: Value(user.id!),
                name: user.name,
                email: user.email,
                image: Value(user.image),
              ),
            );
      }
    });
  }

  @override
  Future<void> updateStatusOnError(Error error) {
    dbModel.setStatus(LoadStatus.error);
    dbModel.setText(error.toString());
    return Future.error(error);
  }
}
