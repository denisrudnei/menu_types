import 'package:flutter/material.dart';

class CutEdges extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width * 0.125, 0);
    path.lineTo(size.width * 0.875, 0);
    path.lineTo(size.width, size.height * 0.125);
    path.lineTo(size.width, size.height * 0.875);
    path.lineTo(size.width * 0.875, size.height);
    path.lineTo(size.width * 0.125, size.height);
    path.lineTo(0, size.height * 0.875);
    path.lineTo(0, size.height * 0.125);
    path.lineTo(size.width * 0.125, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
