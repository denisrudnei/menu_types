import 'dart:io';

import 'package:flutter/material.dart';
import 'package:menu_types/models/product.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:shared_preferences/shared_preferences.dart';

enum ImageType {
  product,
}

class ImageUtil {
  String basePath = '';
  late SharedPreferences prefs;

  Future<void> init() async {
    var dir = await getApplicationDocumentsDirectory();
    prefs = await SharedPreferences.getInstance();
    basePath = '${dir.path}/pdv/';
  }

  Widget getImage(Product product) {
    var productsPath = p.join(basePath, 'products/images/');
    return product.primaryImage == '/images/not-set.svg'
        ? Image.asset(
            'assets/images/not-found.jpg',
            fit: BoxFit.fitWidth,
          )
        : Image.file(
            File(
              '$productsPath${product.id}/primary',
            ),
            fit: BoxFit.fitWidth,
            errorBuilder: (_, __, ___) => Image.asset(
              'assets/images/not-found.jpg',
              fit: BoxFit.fitWidth,
            ),
          );
  }

  File? getLogoFile() {
    var cnpj = prefs.getString('cnpj') ?? 'not-found';
    var image =
        File(p.join(basePath, 'logos', cnpj.replaceAll(RegExp(r'[^0-9]'), '')));
    if (!image.existsSync()) return null;
    return image;
  }

  Widget? getLogo() {
    var file = getLogoFile();
    if (file == null) return null;
    return Image.file(file);
  }
}
