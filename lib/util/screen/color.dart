import 'package:flutter/material.dart';

Map<int, Color> color = {
  50: const Color(0xff04253e),
  100: const Color(0xff042138),
  200: const Color(0xff031a2b),
  300: const Color(0xff021625),
  400: const Color(0xff02131f),
  500: const Color(0xff020f19),
  600: const Color(0xff010b13),
  700: const Color(0xff01070c),
  800: const Color(0xff000406),
  900: const Color(0xff000000),
};

int hexColor = 0xff04253E;

MaterialColor mainColor = MaterialColor(hexColor, color);
