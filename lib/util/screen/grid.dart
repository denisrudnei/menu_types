import 'package:flutter/material.dart';

getNumberOfItems(MediaQueryData data, {double? preferredWidth}) {
  double width = data.size.width;
  if (preferredWidth != null) return (width / preferredWidth).round();
  if (width > 1024) return (width / 480).round();
  if (width > 640) return (width / 350).round();
  return (width / 200).round();
}
