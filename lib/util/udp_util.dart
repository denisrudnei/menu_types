import 'dart:convert';
import 'dart:io';

import 'package:menu_types/enums/printer_status.dart';
import 'package:menu_types/models/printing/print_request.dart';
import 'package:menu_types/models/printing/print_document.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:udp/udp.dart';

class UdpUtil {
  static Future<void> send(PrintRequest request) async {
    var sender = await UDP.bind(Endpoint.any(port: const Port(65000)));

    var bytes = await sender.send(json.encode(request.toJson()).codeUnits,
        Endpoint.broadcast(port: const Port(65001)));

    LoggerUtil.logger.i('$bytes send');
  }

  static Future<void> listen(PrinterModel model) async {
    var listener = await UDP.bind(Endpoint.any(port: const Port(65001)));
    listener.asStream().listen((datagram) {
      var text = String.fromCharCodes(datagram!.data);
      var request = PrintRequest.fromJson(jsonDecode(text));
      PrintDocument document = PrintDocument(
        origin: request.origin,
        printer: request.printer,
        type: request.type,
        status: PrinterStatus.waitingPrint,
        date: DateTime.now(),
      );

      model.addDocument(document);
      var printer = model.printers.firstWhere(
        (element) => element.id == request.printer.id,
      );

      if (PrinterStatus.waitingPrint == printer.status) {
        File(request.printer.path).writeAsBytesSync(request.data);
      }
    });
  }
}
