import 'package:graphql/client.dart';
import 'package:menu_types/models/establishment_table_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class EstablishmentTableSubscriber {
  final EstablishmentTableModel model;

  EstablishmentTableSubscriber({required this.model});

  Future<void> establishmentTableStatusChanged() async {
    GraphQLClient client = await Client.createClient();
    String subscription = r'''
      subscription {
        EstablishmentTableStatusChanged {
          id
          name
          inUse
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscription),
    );

    var result = client.subscribe(options);
    result.listen((event) {
      var table = event.data!['EstablishmentTableStatusChanged'];
      model.updateStatus(table['id'], table['inUse']);
    });
  }

  Future<void> itemAddedToTable() async {
    GraphQLClient client = await Client.createClient();
    String subscriptions = '''
      subscription {
        ItemAddedToTable {
          tableId
          product {
            productId
            data {
              id
              name
              amount
              price
              primaryImage
              description
              imageUpdatedAt
            }
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(subscriptions),
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var table = result.data!['ItemAddedToTable'];
    model.addProduct(
      Product.fromMap(table['product']['data']),
      tableId: table['tableId'],
    );
  }
}
