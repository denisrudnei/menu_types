import 'package:graphql/client.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/graphql/client.dart';

class ProductSubscriber {
  final DbModel model;
  final LogModel logModel;
  final LoadModel loadModel;

  ProductSubscriber({
    required this.model,
    required this.logModel,
    required this.loadModel,
  });

  Future<void> productUpdated() async {
    DbConnectionUtil connectionUtil = DbConnectionUtil(
        model: model, logModel: logModel, loadModel: loadModel);
    GraphQLClient client = await Client.createClient();
    String subscription = r'''
      subscription {
        ProductUpdated {
          id
          name
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscription),
    );
    var result = client.subscribe(options);
    result.listen((event) {
      connectionUtil.downloadInfo();
    });
  }
}
