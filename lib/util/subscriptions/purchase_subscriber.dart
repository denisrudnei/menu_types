import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:logger/logger.dart';
import 'package:menu_types/models/order_model.dart';
import 'package:menu_types/models/pos_model.dart';
import 'package:menu_types/models/print_layout/print_layout_model.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/configuration/site_settings_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/format_utils.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/printing/print_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseSubscriber {
  final PrintUtil printUtil;

  OrderModel orderModel;
  SiteSettingsModel siteSettingsModel;
  PrinterModel printerModel;
  PrintLayoutModel printLayoutModel;
  POSModel posModel;

  PurchaseSubscriber({
    required this.printUtil,
    required this.orderModel,
    required this.siteSettingsModel,
    required this.printerModel,
    required this.printLayoutModel,
    required this.posModel,
  });

  static Logger logger = LoggerUtil.logger;

  Future<void> subscribe() async {
    var client = await Client.createClient();

    String subscription = r'''
    subscription {
      NewPurchase {
        id
        user {
          id
          name
          image
          email
        }
        type
        products {
          id
          productId
          data {
            id
            name
            description
            price
            amount
            primaryImage
          }
        }
        totalAmount
        totalPrice
      }
    }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscription),
    );

    var result = client.subscribe(options);
    logger.i(result);
    result.listen((event) async {
      logger.i(event);
      var newPurchase = event.data!['NewPurchase'];
      if (newPurchase['type'] != 'DELIVERY') return;
      Purchase purchase = Purchase();

      purchase.id = int.parse(newPurchase['id'].toString());

      var newUser = newPurchase['user'];
      User user = User(
        name: newUser['name'],
        image: newUser['image'],
        email: newUser['email'],
      );

      user.id = int.parse(newUser['id'].toString());
      purchase.user = user;

      List<Product> products = [];

      for (var item in newPurchase['products']) {
        products.add(Product.fromMap(item['data']));
      }

      purchase.products = products;

      orderModel.addPurchase(purchase);

      var prefs = await SharedPreferences.getInstance();

      if (prefs.getBool('printWhenNewOrderArrives') ?? false) {
        printUtil.printOrder(purchase);
      }
      FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
          FlutterLocalNotificationsPlugin();
      InitializationSettings initializationSettings =
          const InitializationSettings(
        linux: LinuxInitializationSettings(defaultActionName: 'Novo pedido'),
        android: AndroidInitializationSettings('app_icon'),
      );

      flutterLocalNotificationsPlugin
          .initialize(initializationSettings)
          .then((value) {
        NotificationDetails details = const NotificationDetails(
          linux: LinuxNotificationDetails(),
          android:
              AndroidNotificationDetails('purchaseSubscriber', 'newPurchase'),
        );

        flutterLocalNotificationsPlugin.show(
          purchase.id!,
          '${user.name} realizou um pedido',
          'Pedido número ${purchase.id}, no valor de ${getCurrency(purchase.totalPrice)} recebido as ${getDateAndHour(purchase.createdAt)}',
          details,
        );
      });
    });
  }
}
