import 'package:logger/logger.dart';

class LoggerUtil {
  static final logger = Logger(
    printer: PrettyPrinter(
      printEmojis: true,
      colors: true,
      printTime: true,
    ),
  );
}
