import 'package:graphql/client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Client {
  static Future<GraphQLClient> createClient() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String token = prefs.getString('token') ?? '';

    final _httpLink = HttpLink(
      prefs.getString('graphql') ?? '',
    );
    final _authLink = AuthLink(getToken: () => 'Bearer $token');
    final _wsLink = WebSocketLink(
      prefs.getString('subscription') ?? '',
      config: SocketClientConfig(
        initialPayload: {
          'headers': {
            'Authorization': 'Bearer $token',
          }
        },
        autoReconnect: true,
        inactivityTimeout: const Duration(minutes: 10),
      ),
    );
    var link = _authLink.concat(_httpLink);

    link = Link.split((request) => request.isSubscription, _wsLink, link);

    return GraphQLClient(
      link: link,
      cache: GraphQLCache(),
    );
  }
}
