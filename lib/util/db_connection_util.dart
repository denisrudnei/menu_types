import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:graphql/client.dart';
import 'package:menu_types/enums/load_log_type.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/load/load_service_interface.dart';
import 'package:menu_types/util/load/pos_load_service.dart';
import 'package:menu_types/util/load/printer_load_service.dart';
import 'package:menu_types/util/load/product_load_service.dart';
import 'package:menu_types/util/load/user_load_service.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:postgres/postgres.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as p;

class DbConnectionUtil {
  PostgreSQLConnection? connection;
  DbModel model;
  LogModel logModel;
  LoadModel loadModel;

  List<Product> products = [];
  List<User> users = [];
  List<POS> pos = [];
  List<Printer> printers = [];

  List<int> deletedProducts = [];
  List<int> deletedUsers = [];
  List<String> deletedPOS = [];
  List<String> deletedPrinters = [];

  List<LoadServiceInterface> services = [];

  late LoadLogRepository loadLogRepository;

  DbConnectionUtil({
    required this.model,
    required this.logModel,
    required this.loadModel,
  });

  static Future<PostgreSQLConnection> getConnection() async {
    var prefs = await SharedPreferences.getInstance();
    return PostgreSQLConnection(
      prefs.getString('db-url') ?? '',
      prefs.getInt('db-port') ?? 5432,
      prefs.getString('db-name') ?? '',
      username: prefs.getString('db-username') ?? 'postgres',
      password: prefs.getString('db-password') ?? 'postgres',
    );
  }

  Future<PostgreSQLConnection> openConnection(
    String host,
    String name,
    int port, {
    String username = "postgres",
    String password = "postgres",
  }) async {
    connection ??= PostgreSQLConnection(
      host,
      port,
      name,
      username: username,
      password: password,
    );
    if (connection != null && connection!.isClosed) {
      await connection!.open();
    }
    return connection!;
  }

  Future<void> createTables() async {
    PostgreSQLConnection connection = await getConnection();
    var value = File(p.join(
      Directory.current.path,
      'assets',
      'sql',
      'tables.sql',
    )).readAsStringSync();

    await connection.open();
    try {
      await connection.transaction((ctx) async {
        ctx.execute(value);
      });
    } catch (e) {
      LoggerUtil.logger.e(e);
    }
    await connection.close();
  }

  Future<void> downloadInfo() async {
    model.setStatus(LoadStatus.loading);

    if (loadModel.firstLoad) {
      await fullLoad();
    } else {
      await partialLoad();
    }
    model.setStatus(LoadStatus.loaded);
  }

  Future<void> partialPopulate() async {
    GraphQLClient client = await Client.createClient();

    String query = r'''
     query GetLastRecentData($lastUpdate: DateTime!) {
      GetLastRecentData(lastUpdate: $lastUpdate) {
        products {
          id
          name
          barcode
          type
          description
          price
          primaryImage
          imageUpdatedAt
          amount
        }
        pos {
          id
          name
          hostname
          createdAt
          updatedAt
        }
        users {
          id
          name
          email
          image
        }
        printers {
          id
          name
          path
          type
          manufacturer
          model
          installedIn {
            id
            name
            hostname
            createdAt
            updatedAt
          }
        }
        deleted {
          products {
            id
          }
          pos {
            id
          }
          users {
            id
          }
          printers {
            id
          }
        }
      }
    }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'lastUpdate':
            loadModel.lastSuccessfulUpdate?.toUtc().toIso8601String() ??
                DateTime(2000).toUtc().toIso8601String(),
      },
    );
    model.setText('Baixando dados, carga parcial');
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      model.setStatus(LoadStatus.error);
      model.setText(result.exception?.graphqlErrors.first.message ?? '');
      throw Exception(result.exception);
    }

    products.clear();
    users.clear();
    pos.clear();
    printers.clear();

    deletedProducts.clear();
    deletedUsers.clear();
    deletedPOS.clear();
    deletedPrinters.clear();

    for (var newProduct in result.data!['GetLastRecentData']['products']) {
      products.add(Product.fromMap(newProduct));
    }

    for (var newUser in result.data!['GetLastRecentData']['users']) {
      users.add(User.fromMap(newUser));
    }

    for (var newPOS in result.data!['GetLastRecentData']['pos']) {
      pos.add(POS.fromMap(newPOS));
    }

    for (var newPrinter in result.data!['GetLastRecentData']['printers']) {
      newPrinter['installedIn']['purchases'] = [];
      newPrinter['installedIn']['loadLogs'] = [];
      printers.add(Printer.fromJson(newPrinter));
    }

    var deleted = result.data!['GetLastRecentData']['deleted'];

    for (var deletedProduct in deleted['products']) {
      deletedProducts.add(int.parse(deletedProduct['id']));
    }

    for (var deletedUser in deleted['users']) {
      deletedUsers.add(int.parse(deletedUser['id']));
    }

    for (var deletedPos in deleted['pos']) {
      deletedPOS.add(deletedPos['id']);
    }

    for (var deletedPrinter in deleted['printers']) {
      deletedPrinters.add(deletedPrinter['id']);
    }
  }

  @visibleForTesting
  Future<void> partialLoad() async {
    await partialPopulate();
    var dateOfUpdate = DateTime.now();

    await populateServices();

    await savePOS();
    try {
      await Future.wait([
        ...services.map((service) => service.saveData()),
        _downloadProductsImages(products.where((element) {
          if (element.imageUpdatedAt == null) return false;
          return element.imageUpdatedAt!
              .isAfter(loadModel.lastSuccessfulUpdate!);
        }).toList()),
        _downloadUserImages(users),
        deleteData(),
      ]);
    } catch (e) {
      model.setText(e.toString());
      model.setStatus(LoadStatus.error);
      return;
    }

    model.setText('Todos os dados foram atualizados');
    loadModel.setLastSuccessfulUpdate(dateOfUpdate);
    model.setText('');
  }

  Future<void> fullPopulate() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
     query {
      GetProducts {
        id
        barcode
        type
        name
        description
        price
        primaryImage
        amount
      }
      GetPOS {
        id
        name
        hostname
        createdAt
        updatedAt
      }
      GetUsers {
        id
        name
        email
        image
      }
      GetAllPrinters {
        id
        name
        path
        type
        manufacturer
        model
        installedIn {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
      GetDeletedData {
        products {
          id
        }
        users {
          id
        }
        pos {
          id
        }
        printers {
          id
        }
      }
    }
    ''';
    QueryOptions options = QueryOptions(document: gql(query));
    model.setText('Baixando dados, carga completa');
    var result = await client.query(options);

    for (var list in [
      products,
      users,
      pos,
      printers,
      deletedProducts,
      deletedUsers,
      deletedPOS,
      deletedPrinters
    ]) {
      list.clear();
    }

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    for (var newProduct in result.data!['GetProducts']) {
      products.add(Product.fromMap(newProduct));
    }

    for (var newUser in result.data!['GetUsers']) {
      users.add(User.fromMap(newUser));
    }

    for (var newPOS in result.data!['GetPOS']) {
      pos.add(POS.fromMap(newPOS));
    }

    for (var newPrinter in result.data!['GetAllPrinters']) {
      newPrinter['installedIn']['purchases'] = [];
      newPrinter['installedIn']['loadLogs'] = [];
      printers.add(Printer.fromJson(newPrinter));
    }

    var deleted = result.data!['GetDeletedData'];

    for (var deletedProduct in deleted['products']) {
      deletedProducts.add(int.parse(deletedProduct['id']));
    }

    for (var deletedUser in deleted['users']) {
      deletedUsers.add(int.parse(deletedUser['id']));
    }

    for (var deletedPos in deleted['pos']) {
      deletedPOS.add(deletedPos['id']);
    }

    for (var deletedPrinter in deleted['printers']) {
      deletedPrinters.add(deletedPrinter['id']);
    }
  }

  Future<void> populateServices() async {
    loadLogRepository = LoadLogRepository(
      model: model,
      loadModel: loadModel,
      logModel: logModel,
    );

    services.clear();

    var productService = ProductLoadService(
      dbModel: model,
      loadLogRepository: loadLogRepository,
      dbConnectionUtil: this,
    );

    productService.populate(products);
    productService.populateDeleted(deletedProducts);

    UserLoadService userLoadService = UserLoadService(
      dbConnectionUtil: this,
      dbModel: model,
      loadLogRepository: loadLogRepository,
    );

    userLoadService.populate(users);
    userLoadService.populateDeleted(deletedUsers);

    PrinterLoadService printerLoadService = PrinterLoadService(
      dbConnectionUtil: this,
      loadLogRepository: loadLogRepository,
      dbModel: model,
    );

    printerLoadService.populate(printers);
    printerLoadService.populateDeleted(deletedPrinters);

    services.add(productService);
    services.add(userLoadService);
    services.add(printerLoadService);
    model.setServices(services);
  }

  Future<POSLoadService> populatePOS() async {
    POSLoadService posLoadService = POSLoadService(
      dbConnectionUtil: this,
      dbModel: model,
      loadLogRepository: loadLogRepository,
    );

    posLoadService.populate(pos);
    posLoadService.populateDeleted(deletedPOS);
    model.addService(posLoadService);
    return posLoadService;
  }

  Future<void> savePOS() async {
    POSLoadService posLoadService = await populatePOS();

    await posLoadService.saveData();
  }

  @visibleForTesting
  Future<void> fullLoad() async {
    await fullPopulate();
    var dateOfUpdate = DateTime.now();

    await populateServices();

    await savePOS();

    try {
      await Future.wait([
        ...services.map((service) => service.saveData()),
        _downloadProductsImages(products),
        _downloadUserImages(users),
        deleteData(),
      ]);
    } catch (e) {
      model.setText(e.toString());
      model.setStatus(LoadStatus.error);
      return;
    }

    model.setText('Todos os dados foram atualizados');
    loadModel.setLastSuccessfulUpdate(dateOfUpdate);
  }

  Future<List<String>> _downloadProductsImages(List<Product> products) async {
    model.setIsDownloadingFiles(true);
    HttpClient client = HttpClient();
    List<Product> withImages = products
        .where((element) => element.primaryImage != '/images/not-set.svg')
        .toList();

    model.setFileText('Total de imagens para baixar: ${withImages.length}');
    var dir = await getApplicationDocumentsDirectory();

    for (var product in withImages) {
      try {
        var request = await client.getUrl(Uri.parse(product.primaryImage));
        var response = await request.close();
        if (response.statusCode == 200) {
          var path = dir.path + '/pdv/products/images/${product.id}/';
          File(path + 'primary').create(recursive: true);
          var bytes = await consolidateHttpClientResponseBytes(response);

          var file = File(path + '/primary');

          model.setFileText('Baixando imagem para o produto: ${product.name}');

          await file.writeAsBytes(bytes);
        } else {
          model.setFileText(
              'Falha ao realizar download da imagem ${product.primaryImage} [${product.id}]');
        }
      } catch (e) {
        String imageFailed = 'Falha ao baixar imagem (Produto: ${product.id})';
        model.setFileText(imageFailed);
        model.setStatus(LoadStatus.error);
        await loadLogRepository.saveLoadLog(
          imageFailed,
          type: LoadLogType.ERROR,
        );
      }
    }
    String imageDownloadFinished = 'Download de imagens concluído';
    model.setFileText(imageDownloadFinished);
    model.setIsDownloadingFiles(false);
    await loadLogRepository.saveLoadLog(imageDownloadFinished);
    return products.map((e) => e.primaryImage).toList();
  }

  Future<List<String>> _downloadUserImages(List<User> users) async {
    model.setIsDownloadingFiles(true);
    HttpClient client = HttpClient();
    List<User> withImages = users
        .where((element) => element.image != '/images/profile.jpg')
        .toList();

    model.setFileText('Total de imagens para baixar: ${withImages.length}');

    var dir = await getApplicationDocumentsDirectory();

    for (var user in withImages) {
      try {
        var request = await client.getUrl(Uri.parse(user.image));
        var response = await request.close();
        if (response.statusCode == 200) {
          var path = dir.path + '/pdv/users/images/${user.email}';
          File(path).create(recursive: true);
          var bytes = await consolidateHttpClientResponseBytes(response);

          var file = File(path);

          model.setFileText('Baixando imagem para o usário: ${user.name}');

          await file.writeAsBytes(bytes);
        } else {
          model.setFileText(
              'Falha ao realizar download da imagem ${user.image} [${user.name}]');
        }
      } catch (e) {
        String imageFailed = 'Falha ao baixar imagem (Usuário: ${user.name})';
        model.setFileText(imageFailed);
        model.setStatus(LoadStatus.error);
        await loadLogRepository.saveLoadLog(
          imageFailed,
          type: LoadLogType.ERROR,
        );
      }
    }

    String imageDownloadFinished = 'Download de imagens concluído';

    model.setFileText(imageDownloadFinished);
    model.setIsDownloadingFiles(false);

    await loadLogRepository.saveLoadLog(imageDownloadFinished);

    return users.map((e) => e.image).toList();
  }

  Future<void> deleteData() async {
    await Future.wait(services.map((service) => service.deleteData()));
  }
}
