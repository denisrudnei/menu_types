abstract class ServiceInterface<T> {
  late T implementation;
  Future<void> setImplementation();
}
