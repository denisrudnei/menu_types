import 'package:menu_types/repository/order_repository.dart';
import 'package:menu_types/util/subscriptions/purchase_subscriber.dart';

List<Future> getFutures(
  OrderRepository orderRepository,
  PurchaseSubscriber purchaseSubscriber,
) {
  return [
    purchaseSubscriber.subscribe(),
    orderRepository.orderStatusChanged(),
    orderRepository.bulkPurchasesStatusUpdate(),
    orderRepository.getDelivery(),
  ];
}
