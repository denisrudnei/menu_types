// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: type=lint
class product_model extends DataClass implements Insertable<product_model> {
  final int id;
  final String name;
  final String description;
  final String primaryImage;
  final double price;
  final double amount;
  final String barcode;
  final DateTime? deletedAt;
  product_model(
      {required this.id,
      required this.name,
      required this.description,
      required this.primaryImage,
      required this.price,
      required this.amount,
      required this.barcode,
      this.deletedAt});
  factory product_model.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return product_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      primaryImage: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}primary_image'])!,
      price: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}price'])!,
      amount: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}amount'])!,
      barcode: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}barcode'])!,
      deletedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}deleted_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['description'] = Variable<String>(description);
    map['primary_image'] = Variable<String>(primaryImage);
    map['price'] = Variable<double>(price);
    map['amount'] = Variable<double>(amount);
    map['barcode'] = Variable<String>(barcode);
    if (!nullToAbsent || deletedAt != null) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt);
    }
    return map;
  }

  DriftProductModelCompanion toCompanion(bool nullToAbsent) {
    return DriftProductModelCompanion(
      id: Value(id),
      name: Value(name),
      description: Value(description),
      primaryImage: Value(primaryImage),
      price: Value(price),
      amount: Value(amount),
      barcode: Value(barcode),
      deletedAt: deletedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedAt),
    );
  }

  factory product_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return product_model(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      description: serializer.fromJson<String>(json['description']),
      primaryImage: serializer.fromJson<String>(json['primaryImage']),
      price: serializer.fromJson<double>(json['price']),
      amount: serializer.fromJson<double>(json['amount']),
      barcode: serializer.fromJson<String>(json['barcode']),
      deletedAt: serializer.fromJson<DateTime?>(json['deletedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'description': serializer.toJson<String>(description),
      'primaryImage': serializer.toJson<String>(primaryImage),
      'price': serializer.toJson<double>(price),
      'amount': serializer.toJson<double>(amount),
      'barcode': serializer.toJson<String>(barcode),
      'deletedAt': serializer.toJson<DateTime?>(deletedAt),
    };
  }

  product_model copyWith(
          {int? id,
          String? name,
          String? description,
          String? primaryImage,
          double? price,
          double? amount,
          String? barcode,
          DateTime? deletedAt}) =>
      product_model(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        primaryImage: primaryImage ?? this.primaryImage,
        price: price ?? this.price,
        amount: amount ?? this.amount,
        barcode: barcode ?? this.barcode,
        deletedAt: deletedAt ?? this.deletedAt,
      );
  @override
  String toString() {
    return (StringBuffer('product_model(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description, ')
          ..write('primaryImage: $primaryImage, ')
          ..write('price: $price, ')
          ..write('amount: $amount, ')
          ..write('barcode: $barcode, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, name, description, primaryImage, price, amount, barcode, deletedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is product_model &&
          other.id == this.id &&
          other.name == this.name &&
          other.description == this.description &&
          other.primaryImage == this.primaryImage &&
          other.price == this.price &&
          other.amount == this.amount &&
          other.barcode == this.barcode &&
          other.deletedAt == this.deletedAt);
}

class DriftProductModelCompanion extends UpdateCompanion<product_model> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> description;
  final Value<String> primaryImage;
  final Value<double> price;
  final Value<double> amount;
  final Value<String> barcode;
  final Value<DateTime?> deletedAt;
  const DriftProductModelCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.description = const Value.absent(),
    this.primaryImage = const Value.absent(),
    this.price = const Value.absent(),
    this.amount = const Value.absent(),
    this.barcode = const Value.absent(),
    this.deletedAt = const Value.absent(),
  });
  DriftProductModelCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    this.description = const Value.absent(),
    this.primaryImage = const Value.absent(),
    this.price = const Value.absent(),
    this.amount = const Value.absent(),
    this.barcode = const Value.absent(),
    this.deletedAt = const Value.absent(),
  }) : name = Value(name);
  static Insertable<product_model> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? description,
    Expression<String>? primaryImage,
    Expression<double>? price,
    Expression<double>? amount,
    Expression<String>? barcode,
    Expression<DateTime?>? deletedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (description != null) 'description': description,
      if (primaryImage != null) 'primary_image': primaryImage,
      if (price != null) 'price': price,
      if (amount != null) 'amount': amount,
      if (barcode != null) 'barcode': barcode,
      if (deletedAt != null) 'deleted_at': deletedAt,
    });
  }

  DriftProductModelCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<String>? description,
      Value<String>? primaryImage,
      Value<double>? price,
      Value<double>? amount,
      Value<String>? barcode,
      Value<DateTime?>? deletedAt}) {
    return DriftProductModelCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      primaryImage: primaryImage ?? this.primaryImage,
      price: price ?? this.price,
      amount: amount ?? this.amount,
      barcode: barcode ?? this.barcode,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (primaryImage.present) {
      map['primary_image'] = Variable<String>(primaryImage.value);
    }
    if (price.present) {
      map['price'] = Variable<double>(price.value);
    }
    if (amount.present) {
      map['amount'] = Variable<double>(amount.value);
    }
    if (barcode.present) {
      map['barcode'] = Variable<String>(barcode.value);
    }
    if (deletedAt.present) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftProductModelCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description, ')
          ..write('primaryImage: $primaryImage, ')
          ..write('price: $price, ')
          ..write('amount: $amount, ')
          ..write('barcode: $barcode, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }
}

class $DriftProductModelTable extends DriftProductModel
    with TableInfo<$DriftProductModelTable, product_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftProductModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _primaryImageMeta =
      const VerificationMeta('primaryImage');
  @override
  late final GeneratedColumn<String?> primaryImage = GeneratedColumn<String?>(
      'primary_image', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _priceMeta = const VerificationMeta('price');
  @override
  late final GeneratedColumn<double?> price = GeneratedColumn<double?>(
      'price', aliasedName, false,
      type: const RealType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _amountMeta = const VerificationMeta('amount');
  @override
  late final GeneratedColumn<double?> amount = GeneratedColumn<double?>(
      'amount', aliasedName, false,
      type: const RealType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _barcodeMeta = const VerificationMeta('barcode');
  @override
  late final GeneratedColumn<String?> barcode = GeneratedColumn<String?>(
      'barcode', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _deletedAtMeta = const VerificationMeta('deletedAt');
  @override
  late final GeneratedColumn<DateTime?> deletedAt = GeneratedColumn<DateTime?>(
      'deleted_at', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, name, description, primaryImage, price, amount, barcode, deletedAt];
  @override
  String get aliasedName => _alias ?? 'drift_product_model';
  @override
  String get actualTableName => 'drift_product_model';
  @override
  VerificationContext validateIntegrity(Insertable<product_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('primary_image')) {
      context.handle(
          _primaryImageMeta,
          primaryImage.isAcceptableOrUnknown(
              data['primary_image']!, _primaryImageMeta));
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price']!, _priceMeta));
    }
    if (data.containsKey('amount')) {
      context.handle(_amountMeta,
          amount.isAcceptableOrUnknown(data['amount']!, _amountMeta));
    }
    if (data.containsKey('barcode')) {
      context.handle(_barcodeMeta,
          barcode.isAcceptableOrUnknown(data['barcode']!, _barcodeMeta));
    }
    if (data.containsKey('deleted_at')) {
      context.handle(_deletedAtMeta,
          deletedAt.isAcceptableOrUnknown(data['deleted_at']!, _deletedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  product_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return product_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftProductModelTable createAlias(String alias) {
    return $DriftProductModelTable(attachedDatabase, alias);
  }
}

class user_model extends DataClass implements Insertable<user_model> {
  final int id;
  final String name;
  final String email;
  final String image;
  final bool active;
  final DateTime updatedAt;
  final DateTime? deletedAt;
  user_model(
      {required this.id,
      required this.name,
      required this.email,
      required this.image,
      required this.active,
      required this.updatedAt,
      this.deletedAt});
  factory user_model.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return user_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      email: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}email'])!,
      image: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image'])!,
      active: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}active'])!,
      updatedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
      deletedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}deleted_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['email'] = Variable<String>(email);
    map['image'] = Variable<String>(image);
    map['active'] = Variable<bool>(active);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    if (!nullToAbsent || deletedAt != null) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt);
    }
    return map;
  }

  DriftUserModelCompanion toCompanion(bool nullToAbsent) {
    return DriftUserModelCompanion(
      id: Value(id),
      name: Value(name),
      email: Value(email),
      image: Value(image),
      active: Value(active),
      updatedAt: Value(updatedAt),
      deletedAt: deletedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedAt),
    );
  }

  factory user_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return user_model(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      email: serializer.fromJson<String>(json['email']),
      image: serializer.fromJson<String>(json['image']),
      active: serializer.fromJson<bool>(json['active']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
      deletedAt: serializer.fromJson<DateTime?>(json['deletedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'email': serializer.toJson<String>(email),
      'image': serializer.toJson<String>(image),
      'active': serializer.toJson<bool>(active),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
      'deletedAt': serializer.toJson<DateTime?>(deletedAt),
    };
  }

  user_model copyWith(
          {int? id,
          String? name,
          String? email,
          String? image,
          bool? active,
          DateTime? updatedAt,
          DateTime? deletedAt}) =>
      user_model(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        image: image ?? this.image,
        active: active ?? this.active,
        updatedAt: updatedAt ?? this.updatedAt,
        deletedAt: deletedAt ?? this.deletedAt,
      );
  @override
  String toString() {
    return (StringBuffer('user_model(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('email: $email, ')
          ..write('image: $image, ')
          ..write('active: $active, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, name, email, image, active, updatedAt, deletedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is user_model &&
          other.id == this.id &&
          other.name == this.name &&
          other.email == this.email &&
          other.image == this.image &&
          other.active == this.active &&
          other.updatedAt == this.updatedAt &&
          other.deletedAt == this.deletedAt);
}

class DriftUserModelCompanion extends UpdateCompanion<user_model> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> email;
  final Value<String> image;
  final Value<bool> active;
  final Value<DateTime> updatedAt;
  final Value<DateTime?> deletedAt;
  const DriftUserModelCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.email = const Value.absent(),
    this.image = const Value.absent(),
    this.active = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.deletedAt = const Value.absent(),
  });
  DriftUserModelCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    required String email,
    this.image = const Value.absent(),
    this.active = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.deletedAt = const Value.absent(),
  })  : name = Value(name),
        email = Value(email);
  static Insertable<user_model> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? email,
    Expression<String>? image,
    Expression<bool>? active,
    Expression<DateTime>? updatedAt,
    Expression<DateTime?>? deletedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (email != null) 'email': email,
      if (image != null) 'image': image,
      if (active != null) 'active': active,
      if (updatedAt != null) 'updated_at': updatedAt,
      if (deletedAt != null) 'deleted_at': deletedAt,
    });
  }

  DriftUserModelCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<String>? email,
      Value<String>? image,
      Value<bool>? active,
      Value<DateTime>? updatedAt,
      Value<DateTime?>? deletedAt}) {
    return DriftUserModelCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      image: image ?? this.image,
      active: active ?? this.active,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (email.present) {
      map['email'] = Variable<String>(email.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (active.present) {
      map['active'] = Variable<bool>(active.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    if (deletedAt.present) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftUserModelCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('email: $email, ')
          ..write('image: $image, ')
          ..write('active: $active, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }
}

class $DriftUserModelTable extends DriftUserModel
    with TableInfo<$DriftUserModelTable, user_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftUserModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _emailMeta = const VerificationMeta('email');
  @override
  late final GeneratedColumn<String?> email = GeneratedColumn<String?>(
      'email', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String?> image = GeneratedColumn<String?>(
      'image', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _activeMeta = const VerificationMeta('active');
  @override
  late final GeneratedColumn<bool?> active = GeneratedColumn<bool?>(
      'active', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (active IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime?> updatedAt = GeneratedColumn<DateTime?>(
      'updated_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      clientDefault: DateTime.now);
  final VerificationMeta _deletedAtMeta = const VerificationMeta('deletedAt');
  @override
  late final GeneratedColumn<DateTime?> deletedAt = GeneratedColumn<DateTime?>(
      'deleted_at', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, name, email, image, active, updatedAt, deletedAt];
  @override
  String get aliasedName => _alias ?? 'drift_user_model';
  @override
  String get actualTableName => 'drift_user_model';
  @override
  VerificationContext validateIntegrity(Insertable<user_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('email')) {
      context.handle(
          _emailMeta, email.isAcceptableOrUnknown(data['email']!, _emailMeta));
    } else if (isInserting) {
      context.missing(_emailMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    if (data.containsKey('active')) {
      context.handle(_activeMeta,
          active.isAcceptableOrUnknown(data['active']!, _activeMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    }
    if (data.containsKey('deleted_at')) {
      context.handle(_deletedAtMeta,
          deletedAt.isAcceptableOrUnknown(data['deleted_at']!, _deletedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  user_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return user_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftUserModelTable createAlias(String alias) {
    return $DriftUserModelTable(attachedDatabase, alias);
  }
}

class purchase_model extends DataClass implements Insertable<purchase_model> {
  final int id;
  final DateTime createdAt;
  final double totalAmount;
  final double totalPrice;
  final String type;
  final int? userId;
  final int operatorId;
  final String status;
  final String origin;
  final String posId;
  purchase_model(
      {required this.id,
      required this.createdAt,
      required this.totalAmount,
      required this.totalPrice,
      required this.type,
      this.userId,
      required this.operatorId,
      required this.status,
      required this.origin,
      required this.posId});
  factory purchase_model.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return purchase_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      createdAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      totalAmount: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}total_amount'])!,
      totalPrice: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}total_price'])!,
      type: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}type'])!,
      userId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}user_id']),
      operatorId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}operator_id'])!,
      status: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}status'])!,
      origin: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}origin'])!,
      posId: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}pos_id'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['created_at'] = Variable<DateTime>(createdAt);
    map['total_amount'] = Variable<double>(totalAmount);
    map['total_price'] = Variable<double>(totalPrice);
    map['type'] = Variable<String>(type);
    if (!nullToAbsent || userId != null) {
      map['user_id'] = Variable<int?>(userId);
    }
    map['operator_id'] = Variable<int>(operatorId);
    map['status'] = Variable<String>(status);
    map['origin'] = Variable<String>(origin);
    map['pos_id'] = Variable<String>(posId);
    return map;
  }

  DriftPurchaseModelCompanion toCompanion(bool nullToAbsent) {
    return DriftPurchaseModelCompanion(
      id: Value(id),
      createdAt: Value(createdAt),
      totalAmount: Value(totalAmount),
      totalPrice: Value(totalPrice),
      type: Value(type),
      userId:
          userId == null && nullToAbsent ? const Value.absent() : Value(userId),
      operatorId: Value(operatorId),
      status: Value(status),
      origin: Value(origin),
      posId: Value(posId),
    );
  }

  factory purchase_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return purchase_model(
      id: serializer.fromJson<int>(json['id']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      totalAmount: serializer.fromJson<double>(json['totalAmount']),
      totalPrice: serializer.fromJson<double>(json['totalPrice']),
      type: serializer.fromJson<String>(json['type']),
      userId: serializer.fromJson<int?>(json['userId']),
      operatorId: serializer.fromJson<int>(json['operatorId']),
      status: serializer.fromJson<String>(json['status']),
      origin: serializer.fromJson<String>(json['origin']),
      posId: serializer.fromJson<String>(json['posId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'totalAmount': serializer.toJson<double>(totalAmount),
      'totalPrice': serializer.toJson<double>(totalPrice),
      'type': serializer.toJson<String>(type),
      'userId': serializer.toJson<int?>(userId),
      'operatorId': serializer.toJson<int>(operatorId),
      'status': serializer.toJson<String>(status),
      'origin': serializer.toJson<String>(origin),
      'posId': serializer.toJson<String>(posId),
    };
  }

  purchase_model copyWith(
          {int? id,
          DateTime? createdAt,
          double? totalAmount,
          double? totalPrice,
          String? type,
          int? userId,
          int? operatorId,
          String? status,
          String? origin,
          String? posId}) =>
      purchase_model(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        totalAmount: totalAmount ?? this.totalAmount,
        totalPrice: totalPrice ?? this.totalPrice,
        type: type ?? this.type,
        userId: userId ?? this.userId,
        operatorId: operatorId ?? this.operatorId,
        status: status ?? this.status,
        origin: origin ?? this.origin,
        posId: posId ?? this.posId,
      );
  @override
  String toString() {
    return (StringBuffer('purchase_model(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('totalAmount: $totalAmount, ')
          ..write('totalPrice: $totalPrice, ')
          ..write('type: $type, ')
          ..write('userId: $userId, ')
          ..write('operatorId: $operatorId, ')
          ..write('status: $status, ')
          ..write('origin: $origin, ')
          ..write('posId: $posId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, createdAt, totalAmount, totalPrice, type,
      userId, operatorId, status, origin, posId);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is purchase_model &&
          other.id == this.id &&
          other.createdAt == this.createdAt &&
          other.totalAmount == this.totalAmount &&
          other.totalPrice == this.totalPrice &&
          other.type == this.type &&
          other.userId == this.userId &&
          other.operatorId == this.operatorId &&
          other.status == this.status &&
          other.origin == this.origin &&
          other.posId == this.posId);
}

class DriftPurchaseModelCompanion extends UpdateCompanion<purchase_model> {
  final Value<int> id;
  final Value<DateTime> createdAt;
  final Value<double> totalAmount;
  final Value<double> totalPrice;
  final Value<String> type;
  final Value<int?> userId;
  final Value<int> operatorId;
  final Value<String> status;
  final Value<String> origin;
  final Value<String> posId;
  const DriftPurchaseModelCompanion({
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.totalAmount = const Value.absent(),
    this.totalPrice = const Value.absent(),
    this.type = const Value.absent(),
    this.userId = const Value.absent(),
    this.operatorId = const Value.absent(),
    this.status = const Value.absent(),
    this.origin = const Value.absent(),
    this.posId = const Value.absent(),
  });
  DriftPurchaseModelCompanion.insert({
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.totalAmount = const Value.absent(),
    this.totalPrice = const Value.absent(),
    this.type = const Value.absent(),
    this.userId = const Value.absent(),
    required int operatorId,
    this.status = const Value.absent(),
    this.origin = const Value.absent(),
    required String posId,
  })  : operatorId = Value(operatorId),
        posId = Value(posId);
  static Insertable<purchase_model> custom({
    Expression<int>? id,
    Expression<DateTime>? createdAt,
    Expression<double>? totalAmount,
    Expression<double>? totalPrice,
    Expression<String>? type,
    Expression<int?>? userId,
    Expression<int>? operatorId,
    Expression<String>? status,
    Expression<String>? origin,
    Expression<String>? posId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (createdAt != null) 'created_at': createdAt,
      if (totalAmount != null) 'total_amount': totalAmount,
      if (totalPrice != null) 'total_price': totalPrice,
      if (type != null) 'type': type,
      if (userId != null) 'user_id': userId,
      if (operatorId != null) 'operator_id': operatorId,
      if (status != null) 'status': status,
      if (origin != null) 'origin': origin,
      if (posId != null) 'pos_id': posId,
    });
  }

  DriftPurchaseModelCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? createdAt,
      Value<double>? totalAmount,
      Value<double>? totalPrice,
      Value<String>? type,
      Value<int?>? userId,
      Value<int>? operatorId,
      Value<String>? status,
      Value<String>? origin,
      Value<String>? posId}) {
    return DriftPurchaseModelCompanion(
      id: id ?? this.id,
      createdAt: createdAt ?? this.createdAt,
      totalAmount: totalAmount ?? this.totalAmount,
      totalPrice: totalPrice ?? this.totalPrice,
      type: type ?? this.type,
      userId: userId ?? this.userId,
      operatorId: operatorId ?? this.operatorId,
      status: status ?? this.status,
      origin: origin ?? this.origin,
      posId: posId ?? this.posId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (totalAmount.present) {
      map['total_amount'] = Variable<double>(totalAmount.value);
    }
    if (totalPrice.present) {
      map['total_price'] = Variable<double>(totalPrice.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<int?>(userId.value);
    }
    if (operatorId.present) {
      map['operator_id'] = Variable<int>(operatorId.value);
    }
    if (status.present) {
      map['status'] = Variable<String>(status.value);
    }
    if (origin.present) {
      map['origin'] = Variable<String>(origin.value);
    }
    if (posId.present) {
      map['pos_id'] = Variable<String>(posId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftPurchaseModelCompanion(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('totalAmount: $totalAmount, ')
          ..write('totalPrice: $totalPrice, ')
          ..write('type: $type, ')
          ..write('userId: $userId, ')
          ..write('operatorId: $operatorId, ')
          ..write('status: $status, ')
          ..write('origin: $origin, ')
          ..write('posId: $posId')
          ..write(')'))
        .toString();
  }
}

class $DriftPurchaseModelTable extends DriftPurchaseModel
    with TableInfo<$DriftPurchaseModelTable, purchase_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftPurchaseModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime?> createdAt = GeneratedColumn<DateTime?>(
      'created_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      clientDefault: () => DateTime.now().toLocal());
  final VerificationMeta _totalAmountMeta =
      const VerificationMeta('totalAmount');
  @override
  late final GeneratedColumn<double?> totalAmount = GeneratedColumn<double?>(
      'total_amount', aliasedName, false,
      type: const RealType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _totalPriceMeta = const VerificationMeta('totalPrice');
  @override
  late final GeneratedColumn<double?> totalPrice = GeneratedColumn<double?>(
      'total_price', aliasedName, false,
      type: const RealType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String?> type = GeneratedColumn<String?>(
      'type', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant('NORMAL'));
  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  @override
  late final GeneratedColumn<int?> userId = GeneratedColumn<int?>(
      'user_id', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  final VerificationMeta _operatorIdMeta = const VerificationMeta('operatorId');
  @override
  late final GeneratedColumn<int?> operatorId = GeneratedColumn<int?>(
      'operator_id', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _statusMeta = const VerificationMeta('status');
  @override
  late final GeneratedColumn<String?> status = GeneratedColumn<String?>(
      'status', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant('REQUIRED'));
  final VerificationMeta _originMeta = const VerificationMeta('origin');
  @override
  late final GeneratedColumn<String?> origin = GeneratedColumn<String?>(
      'origin', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant('POS'));
  final VerificationMeta _posIdMeta = const VerificationMeta('posId');
  @override
  late final GeneratedColumn<String?> posId = GeneratedColumn<String?>(
      'pos_id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        createdAt,
        totalAmount,
        totalPrice,
        type,
        userId,
        operatorId,
        status,
        origin,
        posId
      ];
  @override
  String get aliasedName => _alias ?? 'drift_purchase_model';
  @override
  String get actualTableName => 'drift_purchase_model';
  @override
  VerificationContext validateIntegrity(Insertable<purchase_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    }
    if (data.containsKey('total_amount')) {
      context.handle(
          _totalAmountMeta,
          totalAmount.isAcceptableOrUnknown(
              data['total_amount']!, _totalAmountMeta));
    }
    if (data.containsKey('total_price')) {
      context.handle(
          _totalPriceMeta,
          totalPrice.isAcceptableOrUnknown(
              data['total_price']!, _totalPriceMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id']!, _userIdMeta));
    }
    if (data.containsKey('operator_id')) {
      context.handle(
          _operatorIdMeta,
          operatorId.isAcceptableOrUnknown(
              data['operator_id']!, _operatorIdMeta));
    } else if (isInserting) {
      context.missing(_operatorIdMeta);
    }
    if (data.containsKey('status')) {
      context.handle(_statusMeta,
          status.isAcceptableOrUnknown(data['status']!, _statusMeta));
    }
    if (data.containsKey('origin')) {
      context.handle(_originMeta,
          origin.isAcceptableOrUnknown(data['origin']!, _originMeta));
    }
    if (data.containsKey('pos_id')) {
      context.handle(
          _posIdMeta, posId.isAcceptableOrUnknown(data['pos_id']!, _posIdMeta));
    } else if (isInserting) {
      context.missing(_posIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  purchase_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return purchase_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftPurchaseModelTable createAlias(String alias) {
    return $DriftPurchaseModelTable(attachedDatabase, alias);
  }
}

class history_product_model extends DataClass
    implements Insertable<history_product_model> {
  final int id;
  final int productId;
  final int purchaseId;
  final String dataName;
  final String dataBarcode;
  final String dataDescription;
  final double dataAmount;
  final double dataPrice;
  history_product_model(
      {required this.id,
      required this.productId,
      required this.purchaseId,
      required this.dataName,
      required this.dataBarcode,
      required this.dataDescription,
      required this.dataAmount,
      required this.dataPrice});
  factory history_product_model.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return history_product_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      productId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}product_id'])!,
      purchaseId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}purchase_id'])!,
      dataName: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}data_name'])!,
      dataBarcode: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}data_barcode'])!,
      dataDescription: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}data_description'])!,
      dataAmount: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}data_amount'])!,
      dataPrice: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}data_price'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['product_id'] = Variable<int>(productId);
    map['purchase_id'] = Variable<int>(purchaseId);
    map['data_name'] = Variable<String>(dataName);
    map['data_barcode'] = Variable<String>(dataBarcode);
    map['data_description'] = Variable<String>(dataDescription);
    map['data_amount'] = Variable<double>(dataAmount);
    map['data_price'] = Variable<double>(dataPrice);
    return map;
  }

  DriftHistoryProductModelCompanion toCompanion(bool nullToAbsent) {
    return DriftHistoryProductModelCompanion(
      id: Value(id),
      productId: Value(productId),
      purchaseId: Value(purchaseId),
      dataName: Value(dataName),
      dataBarcode: Value(dataBarcode),
      dataDescription: Value(dataDescription),
      dataAmount: Value(dataAmount),
      dataPrice: Value(dataPrice),
    );
  }

  factory history_product_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return history_product_model(
      id: serializer.fromJson<int>(json['id']),
      productId: serializer.fromJson<int>(json['productId']),
      purchaseId: serializer.fromJson<int>(json['purchaseId']),
      dataName: serializer.fromJson<String>(json['dataName']),
      dataBarcode: serializer.fromJson<String>(json['dataBarcode']),
      dataDescription: serializer.fromJson<String>(json['dataDescription']),
      dataAmount: serializer.fromJson<double>(json['dataAmount']),
      dataPrice: serializer.fromJson<double>(json['dataPrice']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'productId': serializer.toJson<int>(productId),
      'purchaseId': serializer.toJson<int>(purchaseId),
      'dataName': serializer.toJson<String>(dataName),
      'dataBarcode': serializer.toJson<String>(dataBarcode),
      'dataDescription': serializer.toJson<String>(dataDescription),
      'dataAmount': serializer.toJson<double>(dataAmount),
      'dataPrice': serializer.toJson<double>(dataPrice),
    };
  }

  history_product_model copyWith(
          {int? id,
          int? productId,
          int? purchaseId,
          String? dataName,
          String? dataBarcode,
          String? dataDescription,
          double? dataAmount,
          double? dataPrice}) =>
      history_product_model(
        id: id ?? this.id,
        productId: productId ?? this.productId,
        purchaseId: purchaseId ?? this.purchaseId,
        dataName: dataName ?? this.dataName,
        dataBarcode: dataBarcode ?? this.dataBarcode,
        dataDescription: dataDescription ?? this.dataDescription,
        dataAmount: dataAmount ?? this.dataAmount,
        dataPrice: dataPrice ?? this.dataPrice,
      );
  @override
  String toString() {
    return (StringBuffer('history_product_model(')
          ..write('id: $id, ')
          ..write('productId: $productId, ')
          ..write('purchaseId: $purchaseId, ')
          ..write('dataName: $dataName, ')
          ..write('dataBarcode: $dataBarcode, ')
          ..write('dataDescription: $dataDescription, ')
          ..write('dataAmount: $dataAmount, ')
          ..write('dataPrice: $dataPrice')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, productId, purchaseId, dataName,
      dataBarcode, dataDescription, dataAmount, dataPrice);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is history_product_model &&
          other.id == this.id &&
          other.productId == this.productId &&
          other.purchaseId == this.purchaseId &&
          other.dataName == this.dataName &&
          other.dataBarcode == this.dataBarcode &&
          other.dataDescription == this.dataDescription &&
          other.dataAmount == this.dataAmount &&
          other.dataPrice == this.dataPrice);
}

class DriftHistoryProductModelCompanion
    extends UpdateCompanion<history_product_model> {
  final Value<int> id;
  final Value<int> productId;
  final Value<int> purchaseId;
  final Value<String> dataName;
  final Value<String> dataBarcode;
  final Value<String> dataDescription;
  final Value<double> dataAmount;
  final Value<double> dataPrice;
  const DriftHistoryProductModelCompanion({
    this.id = const Value.absent(),
    this.productId = const Value.absent(),
    this.purchaseId = const Value.absent(),
    this.dataName = const Value.absent(),
    this.dataBarcode = const Value.absent(),
    this.dataDescription = const Value.absent(),
    this.dataAmount = const Value.absent(),
    this.dataPrice = const Value.absent(),
  });
  DriftHistoryProductModelCompanion.insert({
    this.id = const Value.absent(),
    required int productId,
    required int purchaseId,
    required String dataName,
    required String dataBarcode,
    required String dataDescription,
    required double dataAmount,
    required double dataPrice,
  })  : productId = Value(productId),
        purchaseId = Value(purchaseId),
        dataName = Value(dataName),
        dataBarcode = Value(dataBarcode),
        dataDescription = Value(dataDescription),
        dataAmount = Value(dataAmount),
        dataPrice = Value(dataPrice);
  static Insertable<history_product_model> custom({
    Expression<int>? id,
    Expression<int>? productId,
    Expression<int>? purchaseId,
    Expression<String>? dataName,
    Expression<String>? dataBarcode,
    Expression<String>? dataDescription,
    Expression<double>? dataAmount,
    Expression<double>? dataPrice,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (productId != null) 'product_id': productId,
      if (purchaseId != null) 'purchase_id': purchaseId,
      if (dataName != null) 'data_name': dataName,
      if (dataBarcode != null) 'data_barcode': dataBarcode,
      if (dataDescription != null) 'data_description': dataDescription,
      if (dataAmount != null) 'data_amount': dataAmount,
      if (dataPrice != null) 'data_price': dataPrice,
    });
  }

  DriftHistoryProductModelCompanion copyWith(
      {Value<int>? id,
      Value<int>? productId,
      Value<int>? purchaseId,
      Value<String>? dataName,
      Value<String>? dataBarcode,
      Value<String>? dataDescription,
      Value<double>? dataAmount,
      Value<double>? dataPrice}) {
    return DriftHistoryProductModelCompanion(
      id: id ?? this.id,
      productId: productId ?? this.productId,
      purchaseId: purchaseId ?? this.purchaseId,
      dataName: dataName ?? this.dataName,
      dataBarcode: dataBarcode ?? this.dataBarcode,
      dataDescription: dataDescription ?? this.dataDescription,
      dataAmount: dataAmount ?? this.dataAmount,
      dataPrice: dataPrice ?? this.dataPrice,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (productId.present) {
      map['product_id'] = Variable<int>(productId.value);
    }
    if (purchaseId.present) {
      map['purchase_id'] = Variable<int>(purchaseId.value);
    }
    if (dataName.present) {
      map['data_name'] = Variable<String>(dataName.value);
    }
    if (dataBarcode.present) {
      map['data_barcode'] = Variable<String>(dataBarcode.value);
    }
    if (dataDescription.present) {
      map['data_description'] = Variable<String>(dataDescription.value);
    }
    if (dataAmount.present) {
      map['data_amount'] = Variable<double>(dataAmount.value);
    }
    if (dataPrice.present) {
      map['data_price'] = Variable<double>(dataPrice.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftHistoryProductModelCompanion(')
          ..write('id: $id, ')
          ..write('productId: $productId, ')
          ..write('purchaseId: $purchaseId, ')
          ..write('dataName: $dataName, ')
          ..write('dataBarcode: $dataBarcode, ')
          ..write('dataDescription: $dataDescription, ')
          ..write('dataAmount: $dataAmount, ')
          ..write('dataPrice: $dataPrice')
          ..write(')'))
        .toString();
  }
}

class $DriftHistoryProductModelTable extends DriftHistoryProductModel
    with TableInfo<$DriftHistoryProductModelTable, history_product_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftHistoryProductModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _productIdMeta = const VerificationMeta('productId');
  @override
  late final GeneratedColumn<int?> productId = GeneratedColumn<int?>(
      'product_id', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _purchaseIdMeta = const VerificationMeta('purchaseId');
  @override
  late final GeneratedColumn<int?> purchaseId = GeneratedColumn<int?>(
      'purchase_id', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _dataNameMeta = const VerificationMeta('dataName');
  @override
  late final GeneratedColumn<String?> dataName = GeneratedColumn<String?>(
      'data_name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _dataBarcodeMeta =
      const VerificationMeta('dataBarcode');
  @override
  late final GeneratedColumn<String?> dataBarcode = GeneratedColumn<String?>(
      'data_barcode', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _dataDescriptionMeta =
      const VerificationMeta('dataDescription');
  @override
  late final GeneratedColumn<String?> dataDescription =
      GeneratedColumn<String?>('data_description', aliasedName, false,
          type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _dataAmountMeta = const VerificationMeta('dataAmount');
  @override
  late final GeneratedColumn<double?> dataAmount = GeneratedColumn<double?>(
      'data_amount', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  final VerificationMeta _dataPriceMeta = const VerificationMeta('dataPrice');
  @override
  late final GeneratedColumn<double?> dataPrice = GeneratedColumn<double?>(
      'data_price', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        productId,
        purchaseId,
        dataName,
        dataBarcode,
        dataDescription,
        dataAmount,
        dataPrice
      ];
  @override
  String get aliasedName => _alias ?? 'drift_history_product_model';
  @override
  String get actualTableName => 'drift_history_product_model';
  @override
  VerificationContext validateIntegrity(
      Insertable<history_product_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('product_id')) {
      context.handle(_productIdMeta,
          productId.isAcceptableOrUnknown(data['product_id']!, _productIdMeta));
    } else if (isInserting) {
      context.missing(_productIdMeta);
    }
    if (data.containsKey('purchase_id')) {
      context.handle(
          _purchaseIdMeta,
          purchaseId.isAcceptableOrUnknown(
              data['purchase_id']!, _purchaseIdMeta));
    } else if (isInserting) {
      context.missing(_purchaseIdMeta);
    }
    if (data.containsKey('data_name')) {
      context.handle(_dataNameMeta,
          dataName.isAcceptableOrUnknown(data['data_name']!, _dataNameMeta));
    } else if (isInserting) {
      context.missing(_dataNameMeta);
    }
    if (data.containsKey('data_barcode')) {
      context.handle(
          _dataBarcodeMeta,
          dataBarcode.isAcceptableOrUnknown(
              data['data_barcode']!, _dataBarcodeMeta));
    } else if (isInserting) {
      context.missing(_dataBarcodeMeta);
    }
    if (data.containsKey('data_description')) {
      context.handle(
          _dataDescriptionMeta,
          dataDescription.isAcceptableOrUnknown(
              data['data_description']!, _dataDescriptionMeta));
    } else if (isInserting) {
      context.missing(_dataDescriptionMeta);
    }
    if (data.containsKey('data_amount')) {
      context.handle(
          _dataAmountMeta,
          dataAmount.isAcceptableOrUnknown(
              data['data_amount']!, _dataAmountMeta));
    } else if (isInserting) {
      context.missing(_dataAmountMeta);
    }
    if (data.containsKey('data_price')) {
      context.handle(_dataPriceMeta,
          dataPrice.isAcceptableOrUnknown(data['data_price']!, _dataPriceMeta));
    } else if (isInserting) {
      context.missing(_dataPriceMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  history_product_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return history_product_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftHistoryProductModelTable createAlias(String alias) {
    return $DriftHistoryProductModelTable(attachedDatabase, alias);
  }
}

class payment_model extends DataClass implements Insertable<payment_model> {
  final int id;
  final String type;
  final double value;
  final double paid;
  final double change;
  final int purchaseId;
  payment_model(
      {required this.id,
      required this.type,
      required this.value,
      required this.paid,
      required this.change,
      required this.purchaseId});
  factory payment_model.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return payment_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      type: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}type'])!,
      value: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}value'])!,
      paid: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}paid'])!,
      change: const RealType()
          .mapFromDatabaseResponse(data['${effectivePrefix}change'])!,
      purchaseId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}purchase_id'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['type'] = Variable<String>(type);
    map['value'] = Variable<double>(value);
    map['paid'] = Variable<double>(paid);
    map['change'] = Variable<double>(change);
    map['purchase_id'] = Variable<int>(purchaseId);
    return map;
  }

  DriftPaymentModelCompanion toCompanion(bool nullToAbsent) {
    return DriftPaymentModelCompanion(
      id: Value(id),
      type: Value(type),
      value: Value(value),
      paid: Value(paid),
      change: Value(change),
      purchaseId: Value(purchaseId),
    );
  }

  factory payment_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return payment_model(
      id: serializer.fromJson<int>(json['id']),
      type: serializer.fromJson<String>(json['type']),
      value: serializer.fromJson<double>(json['value']),
      paid: serializer.fromJson<double>(json['paid']),
      change: serializer.fromJson<double>(json['change']),
      purchaseId: serializer.fromJson<int>(json['purchaseId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'type': serializer.toJson<String>(type),
      'value': serializer.toJson<double>(value),
      'paid': serializer.toJson<double>(paid),
      'change': serializer.toJson<double>(change),
      'purchaseId': serializer.toJson<int>(purchaseId),
    };
  }

  payment_model copyWith(
          {int? id,
          String? type,
          double? value,
          double? paid,
          double? change,
          int? purchaseId}) =>
      payment_model(
        id: id ?? this.id,
        type: type ?? this.type,
        value: value ?? this.value,
        paid: paid ?? this.paid,
        change: change ?? this.change,
        purchaseId: purchaseId ?? this.purchaseId,
      );
  @override
  String toString() {
    return (StringBuffer('payment_model(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('paid: $paid, ')
          ..write('change: $change, ')
          ..write('purchaseId: $purchaseId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, type, value, paid, change, purchaseId);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is payment_model &&
          other.id == this.id &&
          other.type == this.type &&
          other.value == this.value &&
          other.paid == this.paid &&
          other.change == this.change &&
          other.purchaseId == this.purchaseId);
}

class DriftPaymentModelCompanion extends UpdateCompanion<payment_model> {
  final Value<int> id;
  final Value<String> type;
  final Value<double> value;
  final Value<double> paid;
  final Value<double> change;
  final Value<int> purchaseId;
  const DriftPaymentModelCompanion({
    this.id = const Value.absent(),
    this.type = const Value.absent(),
    this.value = const Value.absent(),
    this.paid = const Value.absent(),
    this.change = const Value.absent(),
    this.purchaseId = const Value.absent(),
  });
  DriftPaymentModelCompanion.insert({
    this.id = const Value.absent(),
    this.type = const Value.absent(),
    required double value,
    required double paid,
    required double change,
    required int purchaseId,
  })  : value = Value(value),
        paid = Value(paid),
        change = Value(change),
        purchaseId = Value(purchaseId);
  static Insertable<payment_model> custom({
    Expression<int>? id,
    Expression<String>? type,
    Expression<double>? value,
    Expression<double>? paid,
    Expression<double>? change,
    Expression<int>? purchaseId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (type != null) 'type': type,
      if (value != null) 'value': value,
      if (paid != null) 'paid': paid,
      if (change != null) 'change': change,
      if (purchaseId != null) 'purchase_id': purchaseId,
    });
  }

  DriftPaymentModelCompanion copyWith(
      {Value<int>? id,
      Value<String>? type,
      Value<double>? value,
      Value<double>? paid,
      Value<double>? change,
      Value<int>? purchaseId}) {
    return DriftPaymentModelCompanion(
      id: id ?? this.id,
      type: type ?? this.type,
      value: value ?? this.value,
      paid: paid ?? this.paid,
      change: change ?? this.change,
      purchaseId: purchaseId ?? this.purchaseId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (value.present) {
      map['value'] = Variable<double>(value.value);
    }
    if (paid.present) {
      map['paid'] = Variable<double>(paid.value);
    }
    if (change.present) {
      map['change'] = Variable<double>(change.value);
    }
    if (purchaseId.present) {
      map['purchase_id'] = Variable<int>(purchaseId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftPaymentModelCompanion(')
          ..write('id: $id, ')
          ..write('type: $type, ')
          ..write('value: $value, ')
          ..write('paid: $paid, ')
          ..write('change: $change, ')
          ..write('purchaseId: $purchaseId')
          ..write(')'))
        .toString();
  }
}

class $DriftPaymentModelTable extends DriftPaymentModel
    with TableInfo<$DriftPaymentModelTable, payment_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftPaymentModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String?> type = GeneratedColumn<String?>(
      'type', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant('MONEY'));
  final VerificationMeta _valueMeta = const VerificationMeta('value');
  @override
  late final GeneratedColumn<double?> value = GeneratedColumn<double?>(
      'value', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  final VerificationMeta _paidMeta = const VerificationMeta('paid');
  @override
  late final GeneratedColumn<double?> paid = GeneratedColumn<double?>(
      'paid', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  final VerificationMeta _changeMeta = const VerificationMeta('change');
  @override
  late final GeneratedColumn<double?> change = GeneratedColumn<double?>(
      'change', aliasedName, false,
      type: const RealType(), requiredDuringInsert: true);
  final VerificationMeta _purchaseIdMeta = const VerificationMeta('purchaseId');
  @override
  late final GeneratedColumn<int?> purchaseId = GeneratedColumn<int?>(
      'purchase_id', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, type, value, paid, change, purchaseId];
  @override
  String get aliasedName => _alias ?? 'drift_payment_model';
  @override
  String get actualTableName => 'drift_payment_model';
  @override
  VerificationContext validateIntegrity(Insertable<payment_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    }
    if (data.containsKey('value')) {
      context.handle(
          _valueMeta, value.isAcceptableOrUnknown(data['value']!, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    if (data.containsKey('paid')) {
      context.handle(
          _paidMeta, paid.isAcceptableOrUnknown(data['paid']!, _paidMeta));
    } else if (isInserting) {
      context.missing(_paidMeta);
    }
    if (data.containsKey('change')) {
      context.handle(_changeMeta,
          change.isAcceptableOrUnknown(data['change']!, _changeMeta));
    } else if (isInserting) {
      context.missing(_changeMeta);
    }
    if (data.containsKey('purchase_id')) {
      context.handle(
          _purchaseIdMeta,
          purchaseId.isAcceptableOrUnknown(
              data['purchase_id']!, _purchaseIdMeta));
    } else if (isInserting) {
      context.missing(_purchaseIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  payment_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return payment_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftPaymentModelTable createAlias(String alias) {
    return $DriftPaymentModelTable(attachedDatabase, alias);
  }
}

class DriftPOSModelData extends DataClass
    implements Insertable<DriftPOSModelData> {
  final String id;
  final String name;
  final String? hostname;
  final DateTime createdAt;
  final DateTime updatedAt;
  final DateTime? deletedAt;
  DriftPOSModelData(
      {required this.id,
      required this.name,
      this.hostname,
      required this.createdAt,
      required this.updatedAt,
      this.deletedAt});
  factory DriftPOSModelData.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DriftPOSModelData(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      hostname: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}hostname']),
      createdAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      updatedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
      deletedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}deleted_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || hostname != null) {
      map['hostname'] = Variable<String?>(hostname);
    }
    map['created_at'] = Variable<DateTime>(createdAt);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    if (!nullToAbsent || deletedAt != null) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt);
    }
    return map;
  }

  DriftPOSModelCompanion toCompanion(bool nullToAbsent) {
    return DriftPOSModelCompanion(
      id: Value(id),
      name: Value(name),
      hostname: hostname == null && nullToAbsent
          ? const Value.absent()
          : Value(hostname),
      createdAt: Value(createdAt),
      updatedAt: Value(updatedAt),
      deletedAt: deletedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedAt),
    );
  }

  factory DriftPOSModelData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DriftPOSModelData(
      id: serializer.fromJson<String>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      hostname: serializer.fromJson<String?>(json['hostname']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
      deletedAt: serializer.fromJson<DateTime?>(json['deletedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'name': serializer.toJson<String>(name),
      'hostname': serializer.toJson<String?>(hostname),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
      'deletedAt': serializer.toJson<DateTime?>(deletedAt),
    };
  }

  DriftPOSModelData copyWith(
          {String? id,
          String? name,
          String? hostname,
          DateTime? createdAt,
          DateTime? updatedAt,
          DateTime? deletedAt}) =>
      DriftPOSModelData(
        id: id ?? this.id,
        name: name ?? this.name,
        hostname: hostname ?? this.hostname,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        deletedAt: deletedAt ?? this.deletedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DriftPOSModelData(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('hostname: $hostname, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, name, hostname, createdAt, updatedAt, deletedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DriftPOSModelData &&
          other.id == this.id &&
          other.name == this.name &&
          other.hostname == this.hostname &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt &&
          other.deletedAt == this.deletedAt);
}

class DriftPOSModelCompanion extends UpdateCompanion<DriftPOSModelData> {
  final Value<String> id;
  final Value<String> name;
  final Value<String?> hostname;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  final Value<DateTime?> deletedAt;
  const DriftPOSModelCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.hostname = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.deletedAt = const Value.absent(),
  });
  DriftPOSModelCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    this.hostname = const Value.absent(),
    required DateTime createdAt,
    required DateTime updatedAt,
    this.deletedAt = const Value.absent(),
  })  : name = Value(name),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<DriftPOSModelData> custom({
    Expression<String>? id,
    Expression<String>? name,
    Expression<String?>? hostname,
    Expression<DateTime>? createdAt,
    Expression<DateTime>? updatedAt,
    Expression<DateTime?>? deletedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (hostname != null) 'hostname': hostname,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
      if (deletedAt != null) 'deleted_at': deletedAt,
    });
  }

  DriftPOSModelCompanion copyWith(
      {Value<String>? id,
      Value<String>? name,
      Value<String?>? hostname,
      Value<DateTime>? createdAt,
      Value<DateTime>? updatedAt,
      Value<DateTime?>? deletedAt}) {
    return DriftPOSModelCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      hostname: hostname ?? this.hostname,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (hostname.present) {
      map['hostname'] = Variable<String?>(hostname.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    if (deletedAt.present) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftPOSModelCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('hostname: $hostname, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }
}

class $DriftPOSModelTable extends DriftPOSModel
    with TableInfo<$DriftPOSModelTable, DriftPOSModelData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftPOSModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      $customConstraints: 'UNIQUE',
      clientDefault: () => uuid.v4());
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _hostnameMeta = const VerificationMeta('hostname');
  @override
  late final GeneratedColumn<String?> hostname = GeneratedColumn<String?>(
      'hostname', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime?> createdAt = GeneratedColumn<DateTime?>(
      'created_at', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime?> updatedAt = GeneratedColumn<DateTime?>(
      'updated_at', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _deletedAtMeta = const VerificationMeta('deletedAt');
  @override
  late final GeneratedColumn<DateTime?> deletedAt = GeneratedColumn<DateTime?>(
      'deleted_at', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [id, name, hostname, createdAt, updatedAt, deletedAt];
  @override
  String get aliasedName => _alias ?? 'drift_p_o_s_model';
  @override
  String get actualTableName => 'drift_p_o_s_model';
  @override
  VerificationContext validateIntegrity(Insertable<DriftPOSModelData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('hostname')) {
      context.handle(_hostnameMeta,
          hostname.isAcceptableOrUnknown(data['hostname']!, _hostnameMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    if (data.containsKey('deleted_at')) {
      context.handle(_deletedAtMeta,
          deletedAt.isAcceptableOrUnknown(data['deleted_at']!, _deletedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  DriftPOSModelData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DriftPOSModelData.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftPOSModelTable createAlias(String alias) {
    return $DriftPOSModelTable(attachedDatabase, alias);
  }
}

class DriftPrinterModelData extends DataClass
    implements Insertable<DriftPrinterModelData> {
  final String id;
  final String name;
  final String manufacturer;
  final String model;
  final String type;
  final String path;
  final String installedInId;
  final DateTime createdAt;
  final DateTime updatedAt;
  final DateTime? deletedAt;
  DriftPrinterModelData(
      {required this.id,
      required this.name,
      required this.manufacturer,
      required this.model,
      required this.type,
      required this.path,
      required this.installedInId,
      required this.createdAt,
      required this.updatedAt,
      this.deletedAt});
  factory DriftPrinterModelData.fromData(Map<String, dynamic> data,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return DriftPrinterModelData(
      id: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      manufacturer: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}manufacturer'])!,
      model: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}model'])!,
      type: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}type'])!,
      path: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}path'])!,
      installedInId: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}installed_in_id'])!,
      createdAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      updatedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
      deletedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}deleted_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<String>(id);
    map['name'] = Variable<String>(name);
    map['manufacturer'] = Variable<String>(manufacturer);
    map['model'] = Variable<String>(model);
    map['type'] = Variable<String>(type);
    map['path'] = Variable<String>(path);
    map['installed_in_id'] = Variable<String>(installedInId);
    map['created_at'] = Variable<DateTime>(createdAt);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    if (!nullToAbsent || deletedAt != null) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt);
    }
    return map;
  }

  DriftPrinterModelCompanion toCompanion(bool nullToAbsent) {
    return DriftPrinterModelCompanion(
      id: Value(id),
      name: Value(name),
      manufacturer: Value(manufacturer),
      model: Value(model),
      type: Value(type),
      path: Value(path),
      installedInId: Value(installedInId),
      createdAt: Value(createdAt),
      updatedAt: Value(updatedAt),
      deletedAt: deletedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(deletedAt),
    );
  }

  factory DriftPrinterModelData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DriftPrinterModelData(
      id: serializer.fromJson<String>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      manufacturer: serializer.fromJson<String>(json['manufacturer']),
      model: serializer.fromJson<String>(json['model']),
      type: serializer.fromJson<String>(json['type']),
      path: serializer.fromJson<String>(json['path']),
      installedInId: serializer.fromJson<String>(json['installedInId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
      deletedAt: serializer.fromJson<DateTime?>(json['deletedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'name': serializer.toJson<String>(name),
      'manufacturer': serializer.toJson<String>(manufacturer),
      'model': serializer.toJson<String>(model),
      'type': serializer.toJson<String>(type),
      'path': serializer.toJson<String>(path),
      'installedInId': serializer.toJson<String>(installedInId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
      'deletedAt': serializer.toJson<DateTime?>(deletedAt),
    };
  }

  DriftPrinterModelData copyWith(
          {String? id,
          String? name,
          String? manufacturer,
          String? model,
          String? type,
          String? path,
          String? installedInId,
          DateTime? createdAt,
          DateTime? updatedAt,
          DateTime? deletedAt}) =>
      DriftPrinterModelData(
        id: id ?? this.id,
        name: name ?? this.name,
        manufacturer: manufacturer ?? this.manufacturer,
        model: model ?? this.model,
        type: type ?? this.type,
        path: path ?? this.path,
        installedInId: installedInId ?? this.installedInId,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        deletedAt: deletedAt ?? this.deletedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DriftPrinterModelData(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('manufacturer: $manufacturer, ')
          ..write('model: $model, ')
          ..write('type: $type, ')
          ..write('path: $path, ')
          ..write('installedInId: $installedInId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, manufacturer, model, type, path,
      installedInId, createdAt, updatedAt, deletedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DriftPrinterModelData &&
          other.id == this.id &&
          other.name == this.name &&
          other.manufacturer == this.manufacturer &&
          other.model == this.model &&
          other.type == this.type &&
          other.path == this.path &&
          other.installedInId == this.installedInId &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt &&
          other.deletedAt == this.deletedAt);
}

class DriftPrinterModelCompanion
    extends UpdateCompanion<DriftPrinterModelData> {
  final Value<String> id;
  final Value<String> name;
  final Value<String> manufacturer;
  final Value<String> model;
  final Value<String> type;
  final Value<String> path;
  final Value<String> installedInId;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  final Value<DateTime?> deletedAt;
  const DriftPrinterModelCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.manufacturer = const Value.absent(),
    this.model = const Value.absent(),
    this.type = const Value.absent(),
    this.path = const Value.absent(),
    this.installedInId = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.deletedAt = const Value.absent(),
  });
  DriftPrinterModelCompanion.insert({
    required String id,
    required String name,
    required String manufacturer,
    required String model,
    required String type,
    required String path,
    required String installedInId,
    required DateTime createdAt,
    required DateTime updatedAt,
    this.deletedAt = const Value.absent(),
  })  : id = Value(id),
        name = Value(name),
        manufacturer = Value(manufacturer),
        model = Value(model),
        type = Value(type),
        path = Value(path),
        installedInId = Value(installedInId),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<DriftPrinterModelData> custom({
    Expression<String>? id,
    Expression<String>? name,
    Expression<String>? manufacturer,
    Expression<String>? model,
    Expression<String>? type,
    Expression<String>? path,
    Expression<String>? installedInId,
    Expression<DateTime>? createdAt,
    Expression<DateTime>? updatedAt,
    Expression<DateTime?>? deletedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (manufacturer != null) 'manufacturer': manufacturer,
      if (model != null) 'model': model,
      if (type != null) 'type': type,
      if (path != null) 'path': path,
      if (installedInId != null) 'installed_in_id': installedInId,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
      if (deletedAt != null) 'deleted_at': deletedAt,
    });
  }

  DriftPrinterModelCompanion copyWith(
      {Value<String>? id,
      Value<String>? name,
      Value<String>? manufacturer,
      Value<String>? model,
      Value<String>? type,
      Value<String>? path,
      Value<String>? installedInId,
      Value<DateTime>? createdAt,
      Value<DateTime>? updatedAt,
      Value<DateTime?>? deletedAt}) {
    return DriftPrinterModelCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      manufacturer: manufacturer ?? this.manufacturer,
      model: model ?? this.model,
      type: type ?? this.type,
      path: path ?? this.path,
      installedInId: installedInId ?? this.installedInId,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (manufacturer.present) {
      map['manufacturer'] = Variable<String>(manufacturer.value);
    }
    if (model.present) {
      map['model'] = Variable<String>(model.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (path.present) {
      map['path'] = Variable<String>(path.value);
    }
    if (installedInId.present) {
      map['installed_in_id'] = Variable<String>(installedInId.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    if (deletedAt.present) {
      map['deleted_at'] = Variable<DateTime?>(deletedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftPrinterModelCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('manufacturer: $manufacturer, ')
          ..write('model: $model, ')
          ..write('type: $type, ')
          ..write('path: $path, ')
          ..write('installedInId: $installedInId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('deletedAt: $deletedAt')
          ..write(')'))
        .toString();
  }
}

class $DriftPrinterModelTable extends DriftPrinterModel
    with TableInfo<$DriftPrinterModelTable, DriftPrinterModelData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftPrinterModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<String?> id = GeneratedColumn<String?>(
      'id', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: true,
      $customConstraints: 'UNIQUE');
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _manufacturerMeta =
      const VerificationMeta('manufacturer');
  @override
  late final GeneratedColumn<String?> manufacturer = GeneratedColumn<String?>(
      'manufacturer', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _modelMeta = const VerificationMeta('model');
  @override
  late final GeneratedColumn<String?> model = GeneratedColumn<String?>(
      'model', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String?> type = GeneratedColumn<String?>(
      'type', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _pathMeta = const VerificationMeta('path');
  @override
  late final GeneratedColumn<String?> path = GeneratedColumn<String?>(
      'path', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _installedInIdMeta =
      const VerificationMeta('installedInId');
  @override
  late final GeneratedColumn<String?> installedInId = GeneratedColumn<String?>(
      'installed_in_id', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime?> createdAt = GeneratedColumn<DateTime?>(
      'created_at', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime?> updatedAt = GeneratedColumn<DateTime?>(
      'updated_at', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  final VerificationMeta _deletedAtMeta = const VerificationMeta('deletedAt');
  @override
  late final GeneratedColumn<DateTime?> deletedAt = GeneratedColumn<DateTime?>(
      'deleted_at', aliasedName, true,
      type: const IntType(), requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        name,
        manufacturer,
        model,
        type,
        path,
        installedInId,
        createdAt,
        updatedAt,
        deletedAt
      ];
  @override
  String get aliasedName => _alias ?? 'drift_printer_model';
  @override
  String get actualTableName => 'drift_printer_model';
  @override
  VerificationContext validateIntegrity(
      Insertable<DriftPrinterModelData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('manufacturer')) {
      context.handle(
          _manufacturerMeta,
          manufacturer.isAcceptableOrUnknown(
              data['manufacturer']!, _manufacturerMeta));
    } else if (isInserting) {
      context.missing(_manufacturerMeta);
    }
    if (data.containsKey('model')) {
      context.handle(
          _modelMeta, model.isAcceptableOrUnknown(data['model']!, _modelMeta));
    } else if (isInserting) {
      context.missing(_modelMeta);
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (data.containsKey('path')) {
      context.handle(
          _pathMeta, path.isAcceptableOrUnknown(data['path']!, _pathMeta));
    } else if (isInserting) {
      context.missing(_pathMeta);
    }
    if (data.containsKey('installed_in_id')) {
      context.handle(
          _installedInIdMeta,
          installedInId.isAcceptableOrUnknown(
              data['installed_in_id']!, _installedInIdMeta));
    } else if (isInserting) {
      context.missing(_installedInIdMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    if (data.containsKey('deleted_at')) {
      context.handle(_deletedAtMeta,
          deletedAt.isAcceptableOrUnknown(data['deleted_at']!, _deletedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  DriftPrinterModelData map(Map<String, dynamic> data, {String? tablePrefix}) {
    return DriftPrinterModelData.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftPrinterModelTable createAlias(String alias) {
    return $DriftPrinterModelTable(attachedDatabase, alias);
  }
}

class phone_model extends DataClass implements Insertable<phone_model> {
  final int id;
  final String number;
  final String description;
  final int userId;
  phone_model(
      {required this.id,
      required this.number,
      required this.description,
      required this.userId});
  factory phone_model.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return phone_model(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      number: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}number'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description'])!,
      userId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}user_id'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['number'] = Variable<String>(number);
    map['description'] = Variable<String>(description);
    map['user_id'] = Variable<int>(userId);
    return map;
  }

  DriftPhoneModelCompanion toCompanion(bool nullToAbsent) {
    return DriftPhoneModelCompanion(
      id: Value(id),
      number: Value(number),
      description: Value(description),
      userId: Value(userId),
    );
  }

  factory phone_model.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return phone_model(
      id: serializer.fromJson<int>(json['id']),
      number: serializer.fromJson<String>(json['number']),
      description: serializer.fromJson<String>(json['description']),
      userId: serializer.fromJson<int>(json['userId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'number': serializer.toJson<String>(number),
      'description': serializer.toJson<String>(description),
      'userId': serializer.toJson<int>(userId),
    };
  }

  phone_model copyWith(
          {int? id, String? number, String? description, int? userId}) =>
      phone_model(
        id: id ?? this.id,
        number: number ?? this.number,
        description: description ?? this.description,
        userId: userId ?? this.userId,
      );
  @override
  String toString() {
    return (StringBuffer('phone_model(')
          ..write('id: $id, ')
          ..write('number: $number, ')
          ..write('description: $description, ')
          ..write('userId: $userId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, number, description, userId);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is phone_model &&
          other.id == this.id &&
          other.number == this.number &&
          other.description == this.description &&
          other.userId == this.userId);
}

class DriftPhoneModelCompanion extends UpdateCompanion<phone_model> {
  final Value<int> id;
  final Value<String> number;
  final Value<String> description;
  final Value<int> userId;
  const DriftPhoneModelCompanion({
    this.id = const Value.absent(),
    this.number = const Value.absent(),
    this.description = const Value.absent(),
    this.userId = const Value.absent(),
  });
  DriftPhoneModelCompanion.insert({
    this.id = const Value.absent(),
    required String number,
    required String description,
    required int userId,
  })  : number = Value(number),
        description = Value(description),
        userId = Value(userId);
  static Insertable<phone_model> custom({
    Expression<int>? id,
    Expression<String>? number,
    Expression<String>? description,
    Expression<int>? userId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (number != null) 'number': number,
      if (description != null) 'description': description,
      if (userId != null) 'user_id': userId,
    });
  }

  DriftPhoneModelCompanion copyWith(
      {Value<int>? id,
      Value<String>? number,
      Value<String>? description,
      Value<int>? userId}) {
    return DriftPhoneModelCompanion(
      id: id ?? this.id,
      number: number ?? this.number,
      description: description ?? this.description,
      userId: userId ?? this.userId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (number.present) {
      map['number'] = Variable<String>(number.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (userId.present) {
      map['user_id'] = Variable<int>(userId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DriftPhoneModelCompanion(')
          ..write('id: $id, ')
          ..write('number: $number, ')
          ..write('description: $description, ')
          ..write('userId: $userId')
          ..write(')'))
        .toString();
  }
}

class $DriftPhoneModelTable extends DriftPhoneModel
    with TableInfo<$DriftPhoneModelTable, phone_model> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $DriftPhoneModelTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _numberMeta = const VerificationMeta('number');
  @override
  late final GeneratedColumn<String?> number = GeneratedColumn<String?>(
      'number', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _userIdMeta = const VerificationMeta('userId');
  @override
  late final GeneratedColumn<int?> userId = GeneratedColumn<int?>(
      'user_id', aliasedName, false,
      type: const IntType(), requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, number, description, userId];
  @override
  String get aliasedName => _alias ?? 'drift_phone_model';
  @override
  String get actualTableName => 'drift_phone_model';
  @override
  VerificationContext validateIntegrity(Insertable<phone_model> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('number')) {
      context.handle(_numberMeta,
          number.isAcceptableOrUnknown(data['number']!, _numberMeta));
    } else if (isInserting) {
      context.missing(_numberMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('user_id')) {
      context.handle(_userIdMeta,
          userId.isAcceptableOrUnknown(data['user_id']!, _userIdMeta));
    } else if (isInserting) {
      context.missing(_userIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  phone_model map(Map<String, dynamic> data, {String? tablePrefix}) {
    return phone_model.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $DriftPhoneModelTable createAlias(String alias) {
    return $DriftPhoneModelTable(attachedDatabase, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $DriftProductModelTable driftProductModel =
      $DriftProductModelTable(this);
  late final $DriftUserModelTable driftUserModel = $DriftUserModelTable(this);
  late final $DriftPurchaseModelTable driftPurchaseModel =
      $DriftPurchaseModelTable(this);
  late final $DriftHistoryProductModelTable driftHistoryProductModel =
      $DriftHistoryProductModelTable(this);
  late final $DriftPaymentModelTable driftPaymentModel =
      $DriftPaymentModelTable(this);
  late final $DriftPOSModelTable driftPOSModel = $DriftPOSModelTable(this);
  late final $DriftPrinterModelTable driftPrinterModel =
      $DriftPrinterModelTable(this);
  late final $DriftPhoneModelTable driftPhoneModel =
      $DriftPhoneModelTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        driftProductModel,
        driftUserModel,
        driftPurchaseModel,
        driftHistoryProductModel,
        driftPaymentModel,
        driftPOSModel,
        driftPrinterModel,
        driftPhoneModel
      ];
}
