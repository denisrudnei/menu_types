import 'dart:convert';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/pos_repository.dart';
import 'package:menu_types/repository/printer_repository.dart';
import 'package:menu_types/repository/product_repository.dart';
import 'package:menu_types/repository/user_repository.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

class BackupController {
  final PrinterRepository printerRepository;
  final POSRepository posRepository;
  final UserRepository userRepository;
  final ProductRepository productRepository;

  BackupController({
    required this.printerRepository,
    required this.posRepository,
    required this.userRepository,
    required this.productRepository,
  });

  Future<void> backup() async {
    List<Printer> printers = await printerRepository.getAllPrinters();
    List<POS> pos = await posRepository.getPOS();
    List<User> users = await userRepository.getUsers();
    List<Product> products =
        await productRepository.searchProductsByName('', limit: 1000000);

    var directory = await getApplicationDocumentsDirectory();
    var backup = p.join(directory.path, 'pdv');

    var printersJson = jsonEncode(printers.map((e) => e.toJson()).toList());
    var printersFile = File(p.join(backup, 'printers.json'));
    printersFile.writeAsStringSync(printersJson);

    var posJson = jsonEncode(pos.map((e) => e.toJson()).toList());
    var posFile = File(p.join(backup, 'pos.json'));
    posFile.writeAsStringSync(posJson);

    var usersJson = jsonEncode(users.map((e) => e.toJson()).toList());
    var usersFile = File(p.join(backup, 'users.json'));
    usersFile.writeAsStringSync(usersJson);

    var productsJson = jsonEncode(products.map((e) => e.toJson()).toList());
    var productsFile = File(p.join(backup, 'products.json'));
    productsFile.writeAsStringSync(productsJson);

    var encoder = ZipFileEncoder();
    encoder.create(p.join(backup, 'backup.zip'));

    encoder.addFile(printersFile);
    encoder.addFile(posFile);
    encoder.addFile(usersFile);
    encoder.addFile(productsFile);

    encoder.close();
    printersFile.deleteSync();
    posFile.deleteSync();
    usersFile.deleteSync();
    productsFile.deleteSync();
  }
}
