import 'dart:io';

import 'package:menu_types/models/screen/background_model.dart';

class BackgroundController {
  final BackgroundModel model;

  BackgroundController({
    required this.model,
  });

  Future<void> setBackgroundFolder(Directory directory) async {
    if (!(await directory.exists())) {
      throw Exception('Path ${directory.path} not found');
    }
    await model.setBackgroundFolder(directory.path);
    await for (var file in directory.list()) {
      var parts = file.path.split('.');
      if (parts.length < 2) return;
      if (!['jpg', 'png', 'gif', 'webp'].contains(parts[parts.length - 1])) {
        return;
      }
      model.addImagePath(file.path);
    }
  }

  Future<void> setDefaultImage(String image) async {
    await model.setDefaultImage(image);
  }

  void removeImage(String image) {
    model.removeImage(image);
  }
}
