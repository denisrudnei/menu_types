import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/purchase_repository.dart';

class PurchaseController {
  PurchaseRepository repository;
  ProductsModel productsModel;
  PurchasesModel model;

  PurchaseController({
    required this.repository,
    required this.model,
    required this.productsModel,
  });

  Future<Purchase> start({
    required POS pos,
    required User operator,
    required PurchaseType type,
  }) async {
    var result = await repository.start(
      pos: pos,
      operator: operator,
      type: type,
    );
    model.beginPurchase(purchase: result);
    return result;
  }

  Future<List<Payment>> addPayment(Payment payment) async {
    List<Payment> result = await repository.addPayment(payment);
    model.addPayment(payment);
    return result;
  }

  Future<void> removePayment(Payment payment) async {
    await repository.removePayment(payment);
    model.removePayment(payment);
  }

  Future<HistoryProduct> addProduct(
    Product product, {
    double quantity = 1,
  }) async {
    var productToCopy = Product.clone(product);
    productToCopy.amount = quantity;
    HistoryProduct result = await repository.addProduct(productToCopy);
    model.add(product, quantity: quantity);
    productsModel.removeQuantity(productToCopy, quantity);
    return result;
  }

  Future<void> subtractProduct(
    Product product, {
    double quantity = 1,
  }) async {
    var productToCopy = Product.clone(product);
    productToCopy.amount = quantity;
    await repository.subtractProduct(productToCopy);
    model.subtract(product, quantity: quantity);
    productsModel.addQuantity(product, quantity);
  }

  Future<void> cancel() {
    return repository.cancel();
  }

  Future<Purchase> finish() async {
    Purchase result = await repository.finish();
    model.endPurchase();
    return result;
  }

  Future<Purchase?> getCurrentPurchase() {
    return repository.getCurrentPurchase();
  }

  Future<List<DateTime>> getDaysWithPurchases() {
    return repository.getDaysWithPurchases();
  }

  Future<List<Payment>> getPaymentsForPurchase(int purchaseId) {
    return repository.getPaymentsForPurchase(purchaseId);
  }

  Future<List<Product>> getProductsForPurchase(int purchaseId) {
    return repository.getProductsForPurchase(purchaseId);
  }

  Future<Purchase?> getPurchase(String id) {
    return repository.getPurchase(id);
  }

  Future<List<Purchase>> getPurchasesInDate(DateTime date) {
    return getPurchasesInDate(date);
  }

  Future<Purchase> removeClient() async {
    Purchase result = await repository.removeClient();
    model.removeUser();
    return result;
  }

  Future<Purchase> setClient(User client) async {
    Purchase result = await repository.setClient(client);
    model.setUser(client);
    return result;
  }

  Future<Purchase> setOperator(User operator) async {
    Purchase result = await repository.setOperator(operator);
    model.setOperator(operator);
    return result;
  }

  Future<List<Payment>> setPayment(List<Payment> payment) async {
    List<Payment> result = await repository.setPayment(payment);
    model.setPayment(result);
    return result;
  }

  Future<List<Product>> setProducts(List<Product> products) async {
    List<Product> result = await repository.setProducts(products);
    for (var product in result) {
      model.add(product);
    }
    return result;
  }

  Future<void> removeProduct(Product product) async {
    await repository.removeProduct(product);
    model.removeProduct(product);
    productsModel.remove(product);
  }

  void addQuantity(Product product, double quantity) {
    productsModel.addQuantity(product, quantity);
  }

  void removeQuantity(Product product, double quantity) {
    productsModel.removeQuantity(product, quantity);
  }

  void update(Product product, double quantity) {
    model.update(product, quantity);
    productsModel.update(product, quantity);
  }

  Future<DeliveryStatus> updateStatus(DeliveryStatus status) async {
    DeliveryStatus result = await repository.updateStatus(status);
    model.updateStatus(status);
    return result;
  }
}
