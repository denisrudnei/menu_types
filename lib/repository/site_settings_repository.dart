import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:menu_types/models/configuration/site_settings_model.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class SiteSettingsRepository {
  final SiteSettingsModel model;

  SiteSettingsRepository({required this.model});

  Future setInfo() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query {
        GetSiteSettings {
          name
          cnpj
          address
          logo
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      fetchPolicy: FetchPolicy.networkOnly,
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var siteSettings = result.data!['GetSiteSettings'];
    model.setCompanyName(siteSettings['name']);
    model.setCnpj(siteSettings['cnpj']);
    model.setAddress(siteSettings['address']);
    String url = result.data!['GetSiteSettings']['logo'];
    if (url == '/images/not-set.svg') {
      model.setLogo(
          Image.asset('assets/images/not-found.jpg', fit: BoxFit.cover));
    } else {
      model.setLogo(
          Image.network(url + '?${DateTime.now()}', fit: BoxFit.cover));
    }
  }
}
