import 'package:graphql/client.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/models/order_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class OrderRepository {
  final OrderModel model;

  OrderRepository({required this.model});

  Future<void> getDelivery() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query {
        GetDelivery {
          id
          status
          user {
            id
            name
            image
          }
          products {
            id
            productId
            data {
              id
              name
              description
              price
              amount
              primaryImage
            }
          }
          totalAmount
          totalPrice
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      fetchPolicy: FetchPolicy.networkOnly,
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.clear();
    var newOrders = result.data!['GetDelivery'];
    for (var order in newOrders) {
      var orderToAdd = Purchase();

      var newUser = order['user'];
      var user = User(
        name: newUser['name'] ?? '',
        email: newUser['email'] ?? '',
        image: newUser['image'] ?? '',
      );
      orderToAdd.id = int.parse(order['id']);
      orderToAdd.user = user;
      orderToAdd.status = DeliveryStatus.values
          .where((element) => element.toShortString() == order['status'])
          .first;

      List<Product> products = [];

      for (var item in order['products']) {
        products.add(Product.fromMap(item['data']));
      }

      orderToAdd.products = products;
      model.addPurchase(orderToAdd);
    }
  }

  void changeOrderStatus(int id, DeliveryStatus status) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation ChangePurchaseStatus($id: ID!, $status: DeliveryStatus!) {
        ChangePurchaseStatus(id: $id, status: $status) 
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'status': status.toShortString(),
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
  }

  Future<void> changeOrdersStatus(List<int> ids, DeliveryStatus status) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation ChangePurchasesStatus($ids: [ID!]!, $status: DeliveryStatus!) {
        ChangePurchasesStatus(ids: $ids, status: $status)
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'ids': ids,
        'status': status.toShortString(),
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
  }

  Future<void> orderStatusChanged() async {
    GraphQLClient client = await Client.createClient();

    String subscriptionQuery = r'''
      subscription {
        PurchaseStatusUpdated {
          id
          status
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscriptionQuery),
    );

    var subscription = client.subscribe(options);

    subscription.listen((event) {
      var purchase = event.data!['PurchaseStatusUpdated'];

      model.updateStatus(
        int.parse(purchase['id'].toString()),
        DeliveryStatus.values
            .where((element) => element.toShortString() == purchase!['status'])
            .first,
      );
    });
  }

  Future<void> bulkPurchasesStatusUpdate() async {
    GraphQLClient client = await Client.createClient();

    String subscription = r'''
      subscription {
        BulkPurchasesStatusUpdate {
          id
          status
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscription),
    );

    var result = client.subscribe(options);

    result.listen((event) {
      var result = event.data!['BulkPurchasesStatusUpdate'] as List<dynamic>;

      List<int> ids = [];
      late DeliveryStatus status;

      for (var item in result) {
        ids.add(int.parse(item['id'].toString()));
      }

      status = DeliveryStatus.values
          .where((element) => element.toShortString() == result.first['status'])
          .first;

      model.updateStatusForOrders(ids, status);
    });
  }
}
