import 'dart:io';

import 'package:menu_types/models/user.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/repository/db/postgres/user_repository_postgres.dart';
import 'package:menu_types/repository/db/sqlite/user_repository_sqlite.dart';
import 'package:menu_types/repository/graphql/user_repository.dart';
import 'package:menu_types/repository/interfaces/user_repository_interface.dart';
import 'package:menu_types/repository/repository_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepository
    implements
        UserRepositoryInterface,
        RepositoryInterface<UserRepositoryInterface> {
  late MyDatabase db;
  @override
  late UserRepositoryInterface implementation;

  UserRepository({required this.db}) {
    implementation = UserRepositoryGraphql();
    setImplementation().then((value) {
      LoggerUtil.logger
          .i('Configured ${implementation.runtimeType} for UserRepository');
    });
  }

  @override
  Future<void> setImplementation() async {
    var prefs = await SharedPreferences.getInstance();
    var getInServer = prefs.getBool('searchInServerFirst') ?? false;
    if (getInServer) {
      implementation = UserRepositoryGraphql();
      return;
    }

    if (Platform.isAndroid || Platform.isIOS) {
      implementation = UserRepositorySqlite(db: db);
      return;
    }
    implementation = UserRepositoryPostgres();
  }

  @override
  Future<Phone> addPhone(Phone phone, int id) async {
    return implementation.addPhone(phone, id);
  }

  @override
  Future<List<User>> findUsers(String name) async {
    return implementation.findUsers(name);
  }

  @override
  Future<List<User>> getUsers() async {
    return implementation.getUsers();
  }

  @override
  Future<void> removePhone(String phoneId, int id) async {
    return implementation.removePhone(phoneId, id);
  }

  @override
  Future<void> remove(int id) async {
    return implementation.remove(id);
  }

  @override
  Future<User> save(User user) async {
    return implementation.save(user);
  }

  @override
  Future<User?> getOne(int id) async {
    return implementation.getOne(id);
  }

  @override
  Future<User?> create(String email, String name) async {
    return implementation.create(email, name);
  }
}
