import 'package:graphql/client.dart';
import 'package:menu_types/models/printing/print_layout.dart';
import 'package:menu_types/models/print_layout/cut_item.dart';
import 'package:menu_types/models/print_layout/empty_line_item.dart';
import 'package:menu_types/models/print_layout/header_item.dart';
import 'package:menu_types/models/print_layout/line_item.dart';
import 'package:menu_types/models/print_layout/payment_info_item.dart';
import 'package:menu_types/models/print_layout/product_table_item.dart';
import 'package:menu_types/models/print_layout/purchase_information.dart';
import 'package:menu_types/models/print_layout/print_layout_model.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class PrintLayoutRepository {
  // TODO create a database version of this class
  final PrintLayoutModel model;

  PrintLayoutRepository({required this.model});

  Future<List<PrintLayout>> getPrintLayouts() async {
    var client = await Client.createClient();
    String query = '''
      query {
        GetPrintLayouts {
          name
          id
          items {
            id
            type
            position
            ... on HeaderItem {
              address
              cnpj
              storeName
            }
            ... on EmptyLineItem {
              numberOfLines
            }
            ... on ProductTable {
              printType
            }
            ... on LineItem {
              character
            }
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    model.clear();

    for (var layout in result.data!['GetPrintLayouts']) {
      model.add(PrintLayout.fromMap(layout));
    }
    return model.layouts.toList();
  }

  Future<PrintLayout> getPrintLayout(String id) async {
    var client = await Client.createClient();
    String query = r'''
      query GetPrintLayout ($id: ID!){
        GetPrintLayout(id: $id) {
          name
          id
          items {
            id
            position
            type
            ... on HeaderItem {
              address
              cnpj
              storeName
            }
            ... on EmptyLineItem {
              numberOfLines
            }
            ... on LineItem {
              character
            }
            ... on ProductTable {
              printType
            }
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'id': id,
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    return PrintLayout.fromMap(result.data!['GetPrintLayout']);
  }

  Future<bool> removePrintLayout(String id) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation RemovePrintLayout($id: ID!) {
        RemovePrintLayout(id: $id)
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {'id': id},
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.removeById(id);
    return true;
  }

  Future<PrintLayout> create(String name) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation CreatePrintLayout($layout: CreatePrintLayoutInput!) {
        CreatePrintLayout(layout: $layout) {
          id
          name
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'layout': {
          'name': name,
        }
      },
    );

    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    PrintLayout layout = PrintLayout.fromMap(result.data!['CreatePrintLayout']);
    model.add(layout);

    return layout;
  }

  Future<void> addHeaderInPrintLayout(
    String id, {
    bool storeName = false,
    bool address = false,
    bool cnpj = false,
  }) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddHeaderInPrintLayout($id: ID!, $header: AddHeaderInput!) {
        AddHeaderInPrintLayout(id: $id, header: $header) {
          id
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'header': {
          'storeName': storeName,
          'address': address,
          'cnpj': cnpj,
        },
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var data = result.data!['AddHeaderInPrintLayout'];
    model.addItem(
      id,
      HeaderItem(
        id: data['id'],
        position: data['position'],
        type: 'HeaderItem',
        cnpj: cnpj,
        address: address,
        storeName: storeName,
      ),
    );
  }

  Future<void> addEmptyLineItem(String id, int lines) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddEmptyLineInPrintLayout($id: ID!, $lines: Int!) {
        AddEmptyLineInPrintLayout(id: $id, lines: $lines) {
          id
          type
          numberOfLines
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'lines': lines,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.addItem(
      id,
      EmptyLineItem.fromMap(result.data!['AddEmptyLineInPrintLayout']),
    );
  }

  Future<void> addProductTable(String id, String type) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddProductTable($id: ID!, $type: PurchaseType!) {
        AddProductTable(id: $id, type: $type) {
          id
          type
          printType
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'type': type,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    model.addItem(
      id,
      ProductTableItem.fromMap(result.data!['AddProductTable']),
    );
  }

  Future<void> addCutItem(String id) async {
    var client = await Client.createClient();

    String mutation = r'''
      mutation AddCutItem($id: ID!) {
        AddCutItem (id: $id) {
          id
          type
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.addItem(id, CutItem.fromMap(result.data!['AddCutItem']));
  }

  Future<void> addLineItem(String id, String character) async {
    var client = await Client.createClient();

    String mutation = r'''
      mutation AddLineItem($id: ID!, $character: String!) {
        AddLineItem (id: $id, character: $character) {
          id
          type
          position
          character
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'character': character,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.addItem(id, LineItem.fromMap(result.data!['AddLineItem']));
  }

  Future<void> addPurchaseInformation(String id) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddPurchaseInformation($id: ID!) {
        AddPurchaseInformation(id: $id) {
          id
          type
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.addItem(
      id,
      PurchaseInformation.fromMap(result.data!['AddPurchaseInformation']),
    );
  }

  Future<void> addPayment(String id) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddPayment($id: ID!) {
        AddPayment(id: $id) {
          id
          type
          position
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
      },
    );
    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);

      throw Exception(result.exception);
    }

    model.addItem(
      id,
      PaymentInfoItem.fromMap(result.data!['AddPayment']),
    );
  }

  Future<void> moveItemPosition(
      String id, int oldPosition, int newPosition) async {
    model.setPosition(id, oldPosition, newPosition);
    var client = await Client.createClient();
    String mutation = r'''
      mutation MoveItemPosition($id: ID!, $oldPosition: Int!, $newPosition: Int!) {
        MoveItemPosition(id: $id, oldPosition: $oldPosition, newPosition: $newPosition)
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'oldPosition': oldPosition,
        'newPosition': newPosition,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
  }

  Future<void> removePrintLayoutItem(String id, String item) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation RemovePrintLayoutItem($id: ID!, $item: ID!) {
        RemovePrintLayoutItem(id: $id item: $item)
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'item': item,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.removeItem(id, item);
  }
}
