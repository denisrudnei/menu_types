import 'package:graphql/client.dart';
import 'package:menu_types/models/user_notification.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class NotificationRepository {
  Future<List<UserNotification>> getNotifications() async {
    GraphQLClient client = await Client.createClient();
    String query = '''
      query {
        GetAllNotifications {
          id
          content
          date
          read
          origin
          user {
            id
            name
            email
            image
          }
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    List<UserNotification> notifications = [];

    for (var notification in result.data!['GetAllNotifications']) {
      notifications.add(UserNotification.fromMap(notification));
    }

    return notifications;
  }

  Future<void> read(String id) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation ReadNotification($id: ID!) {
        ReadNotification(id: $id)
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
      },
    );

    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
  }
}
