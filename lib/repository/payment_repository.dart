import 'package:graphql/client.dart';
import 'package:menu_types/models/payment/payment_type.dart';
import 'package:menu_types/models/payment/payment_types_model.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class PaymentRepository {
  final PaymentTypesModel model;

  PaymentRepository({required this.model});

  Future<List<PaymentType>> getPaymentTypes() async {
    GraphQLClient client = await Client.createClient();
    List<PaymentType> paymentTypes = [];
    String query = r'''
      query GetPaymentTypes($locale: Locale) {
        GetPaymentTypes (locale: $locale) {
          text
          value
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: const {'locale': 'PT_BR'},
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      return paymentTypes;
    }

    for (var paymentType in result.data!['GetPaymentTypes']) {
      paymentTypes.add(
        PaymentType(
          value: paymentType['value'],
          text: paymentType['text'],
        ),
      );
    }
    model.setAll(paymentTypes);
    return paymentTypes;
  }
}
