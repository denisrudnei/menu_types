import 'package:graphql/client.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class SummaryRepository {
  Future<List<SummaryItem>> getPurchaseSummaryInMonth(
    int year,
    int month,
  ) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetPurchaseSummaryInMonth($year: Int!, $month: Int!) {
        GetPurchaseSummaryInMonth(year: $year, month: $month) {
          day
          total
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'year': year,
        'month': month,
      },
    );

    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    List<SummaryItem> summary = [];
    for (var item in result.data!['GetPurchaseSummaryInMonth']) {
      var summaryItem = SummaryItem(
        day: item['day'],
        total: item['total'],
      );
      summary.add(summaryItem);
    }
    return summary;
  }
}

class SummaryItem {
  SummaryItem({required this.day, required this.total}) : super();
  final String day;
  final int total;
}
