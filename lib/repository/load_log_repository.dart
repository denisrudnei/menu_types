import 'dart:io';

import 'package:menu_types/enums/load_log_type.dart';
import 'package:menu_types/enums/load_status.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_log.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:postgres/postgres.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoadLogRepository {
  DbModel model;
  LogModel logModel;
  LoadModel loadModel;

  LoadLogRepository({
    required this.model,
    required this.loadModel,
    required this.logModel,
  });

  Future<List<LoadLog>> getLoadLogs({
    List<LoadLogType> types = const [],
  }) async {
    List<LoadLog> loadLogs = [];
    logModel.clear();

    PostgreSQLConnection connection =
        await DbConnectionUtil.getConnection().catchError((e) {
      model.setStatus(LoadStatus.error);
      model.setText('Falha ao conectar ao banco');
    });
    String query = r'''
      SELECT id,
            message,
            "posId",
            type,
            "createdAt"
      FROM load_log
      ORDER BY "createdAt" DESC
      ''';
    await connection.open();
    var result = await connection.query(query);
    await connection.close();

    for (var row in result) {
      var pos = POS();
      pos.id = row[2];
      var loadLog = LoadLog(
        id: row[0].toString(),
        message: row[1],
        pos: pos,
        type: LoadLogType.values
            .firstWhere((element) => element.toShortString() == row[3]),
      );
      loadLog.createdAt = DateTime.parse(row[4].toString());
      if (types.contains(loadLog.type)) {
        loadLogs.add(loadLog);
        logModel.addLoadLog(loadLog);
      }
    }
    return loadLogs;
  }

  Future<void> saveLoadLog(
    String message, {
    LoadLogType type = LoadLogType.INFO,
  }) async {
    if (Platform.isAndroid || Platform.isIOS) return;

    var prefs = await SharedPreferences.getInstance();
    String posId = prefs.getString('pos') ?? '';
    // FIXME

    try {
      PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

      POS pos = POS();
      pos.id = posId;

      LoadLog loadLog = LoadLog(
        id: '',
        message: message,
        pos: pos,
        type: type,
      );

      String insert =
          'INSERT INTO load_log(message, "posId", type) values(@message, @pos, @type)';
      await connection.open();
      await connection.query(insert, substitutionValues: {
        'message': loadLog.message,
        'pos': loadLog.pos.id,
        'type': type.toShortString(),
      });
      await connection.close();
      logModel.addLoadLog(loadLog);
    } catch (e) {
      LoggerUtil.logger.e(e);
      model.setStatus(LoadStatus.error);
      model.setText('Falha ao conectar ao banco');
    }
  }
}
