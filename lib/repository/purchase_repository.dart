import 'dart:io';

import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/postgres/purchase_repository_postgres.dart';
import 'package:menu_types/repository/db/sqlite/purchase_repository_sqlite.dart';
import 'package:menu_types/repository/graphql/purchase_repository_graphql.dart';
import 'package:menu_types/repository/interfaces/purchase_repository_interface.dart';
import 'package:menu_types/repository/repository_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseRepository
    implements
        PurchaseRepositoryInterface,
        RepositoryInterface<PurchaseRepositoryInterface> {
  @override
  MyDatabase db;

  @override
  late PurchaseRepositoryInterface implementation;

  PurchaseRepository(this.db) {
    implementation = PurchaseRepositoryGraphql();
    setImplementation().then((_) {
      LoggerUtil.logger.i(
          'Configured ${implementation.runtimeType} as implementation of PurchaseRepository');
    });
  }

  @override
  Future<void> setImplementation() async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool('searchInServerFirst') ?? false;
    if (searchInServerFirst) {
      implementation = PurchaseRepositoryGraphql();
      return;
    }

    try {
      if (Platform.isAndroid || Platform.isIOS) {
        implementation = PurchaseRepositorySqlite(db);
        return;
      } else {
        implementation = PurchaseRepositoryPostgres(db);
        return;
      }
    } catch (e) {
      LoggerUtil.logger.e(e);
      implementation = PurchaseRepositoryGraphql();
    }
  }

  @override
  Future<Purchase> finish() async {
    return implementation.finish();
  }

  @override
  Future<List<DateTime>> getDaysWithPurchases() async {
    return implementation.getDaysWithPurchases();
  }

  @override
  Future<Purchase?> getPurchase(String id) async {
    return implementation.getPurchase(id);
  }

  @override
  Future<List<Purchase>> getPurchasesInDate(DateTime date) async {
    return implementation.getPurchasesInDate(date);
  }

  @override
  Future<List<Payment>> addPayment(Payment payment) async {
    return implementation.addPayment(payment);
  }

  @override
  Future<void> removePayment(Payment payment) async {
    return implementation.removePayment(payment);
  }

  @override
  Future<List<Product>> getProductsForPurchase(int purchaseId) async {
    return implementation.getProductsForPurchase(purchaseId);
  }

  @override
  Future<List<Product>> setProducts(List<Product> products) async {
    return implementation.setProducts(products);
  }

  @override
  Future<List<Payment>> getPaymentsForPurchase(int purchaseId) async {
    return implementation.getPaymentsForPurchase(purchaseId);
  }

  @override
  Future<HistoryProduct> addProduct(Product product) {
    return implementation.addProduct(product);
  }

  @override
  Future<void> removeProduct(Product product) {
    return implementation.removeProduct(product);
  }

  @override
  Future<Purchase?> getCurrentPurchase() {
    return implementation.getCurrentPurchase();
  }

  @override
  Future<Purchase> setClient(User client) {
    return implementation.setClient(client);
  }

  @override
  Future<Purchase> setOperator(User operator) {
    return implementation.setOperator(operator);
  }

  @override
  Future<List<Payment>> setPayment(List<Payment> payment) {
    return implementation.setPayment(payment);
  }

  @override
  Future<Purchase> start({
    required POS pos,
    required User operator,
    required PurchaseType type,
  }) {
    return implementation.start(pos: pos, operator: operator, type: type);
  }

  @override
  Future<DeliveryStatus> updateStatus(DeliveryStatus status) {
    return implementation.updateStatus(status);
  }

  @override
  Future<Purchase> removeClient() {
    return implementation.removeClient();
  }

  @override
  Future<void> cancel() {
    return implementation.cancel();
  }

  @override
  Future<HistoryProduct> subtractProduct(Product product) {
    return implementation.subtractProduct(product);
  }
}
