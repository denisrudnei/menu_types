import 'dart:convert';
import 'dart:io';

import 'package:dbcrypt/dbcrypt.dart';
import 'package:flutter/foundation.dart';
import 'package:graphql/client.dart';
import 'package:menu_types/models/auth_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository {
  final AuthModel model;

  AuthRepository({required this.model});
  static const String _savedUsersString = 'savedUsers';
  Future<User> login(String username, String password) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation Login($username: String!, $password: String!) {
        Login(username: $username, password: $password) {
          id
          name
          email
          image
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'username': username,
        'password': password,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var loggedUser = result.data!['Login'];
    var user = User(
      name: loggedUser['name'],
      email: loggedUser['email'],
      image: loggedUser['image'],
    );
    user.id = int.parse(loggedUser['id'].toString());
    saveLogin(username, password);
    downloadUserImage(user);
    model.setUser(user);
    return user;
  }

  /// Check if the login and password entered are correct
  Future<bool> offlineLogin(String username, String password) async {
    return SharedPreferences.getInstance().then((prefs) {
      var savedUsers = prefs.getString(_savedUsersString);

      if (savedUsers == null) return false;

      var users = Map<String, String>.from(jsonDecode(savedUsers));

      if (!users.containsKey(username)) {
        return false;
      }

      if (!DBCrypt().checkpw(password, users[username]!)) {
        return false;
      }

      return true;
    });
  }

  /// Saves username and password in order to login offline,
  /// password is encrypted before being saved
  Future<String> saveLogin(String username, String password) async {
    return SharedPreferences.getInstance().then((prefs) {
      var savedUsers = prefs.getString(_savedUsersString);

      var hashedPassword = DBCrypt().hashpw(
        password,
        DBCrypt().gensalt(),
      );

      Map<String, String> loggedUsers = {};

      if (savedUsers != null) {
        loggedUsers.addAll(Map<String, String>.from(jsonDecode(savedUsers)));
      }

      loggedUsers.addAll({username: hashedPassword});

      prefs.setString(_savedUsersString, jsonEncode(loggedUsers));

      return username;
    });
  }

  Future<bool> hasImageDownloaded(String username) async {
    var dir = await getApplicationDocumentsDirectory();
    var path = '${dir.path}/pdv/users/images/$username';
    return File(path).exists();
  }

  Future<String> downloadUserImage(User user) async {
    if (user.image == '/images/profile.jpg') return '';
    HttpClient client = HttpClient();

    var path = '';

    var dir = await getApplicationDocumentsDirectory();
    try {
      var request = await client.getUrl(Uri.parse(user.image));
      var response = await request.close();
      if (response.statusCode == 200) {
        path = '${dir.path}/pdv/users/images/${user.email}';
        File(path).createSync(recursive: true);
        var bytes = await consolidateHttpClientResponseBytes(response);

        var file = File(path);

        await file.writeAsBytes(bytes);
      }
    } catch (e) {
      LoggerUtil.logger.e(e);
    }
    return path;
  }
}
