import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

class DeviceInfo {
  DeviceInfoPlugin plugin = DeviceInfoPlugin();
  String _hostname = '';

  DeviceInfo() {
    if (Platform.isAndroid) {
      plugin.androidInfo.then((value) {
        _hostname = value.host ?? '';
      });
    }

    if (Platform.isIOS) {
      plugin.iosInfo.then((value) {
        _hostname = value.name ?? '';
      });
    }

    if (Platform.isLinux) {
      plugin.linuxInfo.then((value) {
        _hostname = value.prettyName;
      });
    }

    if (Platform.isWindows) {
      plugin.windowsInfo.then((value) {
        _hostname = value.computerName;
      });
    }
  }

  String get hostname {
    return _hostname;
  }
}
