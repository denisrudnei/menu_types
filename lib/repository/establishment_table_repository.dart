import 'package:graphql/client.dart';
import 'package:menu_types/models/establishment_table.dart';
import 'package:menu_types/models/establishment_table_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class EstablishmentTableRepository {
  final EstablishmentTableModel model;

  EstablishmentTableRepository({required this.model});

  Future newEstablishmentTable() async {
    GraphQLClient client = await Client.createClient();
    String subscriptionQuery = r'''
      subscription {
        EstablishmentTableCreated {
          id
          name
          inUse
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscriptionQuery),
    );

    var subscription = client.subscribe(options);
    subscription.listen((event) {
      var created = event.data?['EstablishmentTableCreated'];

      model.add(
        EstablishmentTable(
          id: created['id'],
          name: created['name'],
          inUse: created['inUse'],
        ),
      );
    });
  }

  Future establishmentTableStatusChanged() async {
    GraphQLClient client = await Client.createClient();
    String subscriptionQuery = r'''
      subscription {
        EstablishmentTableStatusChanged {
          id
          name
          inUse
        }
      }
    ''';
    SubscriptionOptions options = SubscriptionOptions(
      document: gql(subscriptionQuery),
    );

    var subscription = client.subscribe(options);

    subscription.listen((event) {
      var updated = event.data?['EstablishmentTableStatusChanged'];

      for (var element in model.tables) {
        if (element.id == updated['id']) {
          model.updateStatus(element.id, updated['inUse']);
        }
      }
    });
  }

  Future createEstablishmentTable(EstablishmentTable table) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation CreateEstablishmentTable($table: CreateEstablishmentTableInput!) {
        CreateEstablishmentTable(table: $table) {
          id
          name
          inUse
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'table': {
          'name': table.name,
          'inUse': table.inUse,
        },
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
    } else {
      var data = result.data!['CreateEstablishmentTable'];
      var newEstablishmentTable = EstablishmentTable(
        id: data['id'],
        name: data['name'],
        inUse: data['inUse'],
      );
      model.add(newEstablishmentTable);
    }
  }

  Future establishmentTableRemoved() async {
    GraphQLClient client = await Client.createClient();
    String subscriptionQuery = r'''
      subscription {
        EstablishmentTableRemoved {
          id
        }
      }
    ''';

    var options = SubscriptionOptions(
      document: gql(subscriptionQuery),
    );

    var result = client.subscribe(options);

    result.listen((event) {
      model.remove(
        EstablishmentTable.fromMap(event.data!['EstablishmentTableRemoved']),
      );
    });
  }

  Future removeEstablishmentTable(EstablishmentTable table) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation RemoveEstablishmentTable($id: ID!) {
        RemoveEstablishmentTable(id: $id)
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': table.id,
      },
    );
    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
    }
    model.remove(table);
  }

  Future setEstablishmentTableStatus(String id, bool inUse) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation UpdateEstablishmentTableStatus($id: ID!, $inUse: Boolean!) {
        UpdateEstablishmentTableStatus(id: $id, inUse: $inUse) {
          id
          name
          inUse
        }
      }
    ''';

    MutationOptions mutationOptions =
        MutationOptions(document: gql(mutation), variables: {
      'id': id,
      'inUse': inUse,
    });

    var result = await client.mutate(mutationOptions);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var data = result.data!['UpdateEstablishmentTableStatus'];
    return EstablishmentTable(
      id: data['id'],
      name: data['name'],
      inUse: data['inUse'],
    );
  }

  Future<EstablishmentTable> getTable(String id) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetTable($id: ID!) {
        GetTable(id: $id) {
          id
          name
          inUse
          activeOrder {
            totalPrice
            totalAmount
            createdAt
            id
            user {
              id
              name
              email
              image
            }
            operator {
              id
              name
              email
              image
            }
            pos {
              id
              name
            }
            products {
              id
              productId
              data {
                id
                name
                amount
                price
                primaryImage
              }
            }
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'id': id,
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var table = EstablishmentTable.fromMap(result.data!['GetTable']);
    model.setActual(table);
    return table;
  }

  Future getTables() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query {
        GetTables {
          id
          name
          inUse
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      fetchPolicy: FetchPolicy.networkOnly,
    );
    var result = await client.query(options);

    model.clear();

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    for (var table in result.data!['GetTables']) {
      model.add(EstablishmentTable(
        id: table['id'],
        inUse: table['inUse'],
        name: table['name'],
      ));
    }
  }

  Future<void> updateStatus(String id, bool inUse) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation UpdateEstablishmentTableStatus($id: ID!, $inUse: Boolean!) {
        UpdateEstablishmentTableStatus(id: $id, inUse: $inUse) {
          id
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'inUse': inUse,
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    model.updateStatus(id, inUse);
  }

  Future<void> addItemToTable(String id, Product product) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation AddItemToTable($id: ID!, $product: ProductForPurchaseInput!) {
        AddItemToTable(id: $id, product: $product) {
          tableId
          product {
            id
            productId
            data {
              id
              name
              primaryImage
              amount
              price
              category {
                id
                name
                image
              }
            }
          }
        }
      }
    ''';
    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'product': {
          'id': product.id,
          'amount': product.amount,
        },
      },
    );

    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    model.addProduct(product, tableId: id, quantity: product.amount);
  }
}
