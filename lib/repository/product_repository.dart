import 'dart:io';

import 'package:graphql/client.dart';
import 'package:menu_types/models/page_info.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/search_input.dart';
import 'package:menu_types/repository/db/postgres/products_repository_postgres.dart';
import 'package:menu_types/repository/db/sqlite/products_repository_sqlite.dart';
import 'package:menu_types/repository/graphql/products_graphql.dart';
import 'package:menu_types/repository/interfaces/product_repository_interface.dart';
import 'package:menu_types/repository/repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductRepository
    implements
        ProductRepositoryInterface,
        RepositoryInterface<ProductRepositoryInterface> {
  @override
  late MyDatabase db;

  @override
  final PurchasesModel purchasesModel;

  @override
  final ProductsModel model;

  @override
  late ProductRepositoryInterface implementation;

  @override
  Future<void> setImplementation() async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool('searchInServerFirst') ?? false;
    if (searchInServerFirst) {
      implementation = ProductGraphql(
        purchasesModel: purchasesModel,
        model: model,
      );
      return;
    }

    try {
      if (Platform.isAndroid || Platform.isIOS) {
        implementation = ProductsRepositorySqlite(
          db: db,
          purchasesModel: purchasesModel,
          model: model,
        );
        return;
      } else {
        implementation = ProductsRepositoryPostgres(
          purchasesModel: purchasesModel,
          model: model,
        );
        return;
      }
    } catch (e) {
      LoggerUtil.logger.e(e);

      implementation = ProductGraphql(
        purchasesModel: purchasesModel,
        model: model,
      );
    }
  }

  ProductRepository({
    required this.db,
    required this.purchasesModel,
    required this.model,
  }) {
    implementation = ProductGraphql(
      purchasesModel: purchasesModel,
      model: model,
    );
    setImplementation().then((_) {
      LoggerUtil.logger
          .i('Configured ${implementation.runtimeType} for ProductRepository');
    });
  }

  @override
  Future<List<Product>> searchProductsByName(
    String text, {
    int page = 1,
    int limit = 10,
  }) async {
    return implementation.searchProductsByName(text, page: page, limit: limit);
  }

  @override
  Future<List<Product>> getProductsByBarcode(
    String barcode, {
    int limit = 10,
    int page = 1,
  }) async {
    return implementation.getProductsByBarcode(
      barcode,
      limit: limit,
      page: page,
    );
  }

  @override
  Future<List<String>> getProductTypes() async {
    GraphQLClient client = await Client.createClient();
    String query = '''
      query {
        GetProductTypes
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
    );

    var result = await client.query(options);

    if (result.hasException) {
      throw Exception(result.exception);
    }
    List<String> types = [];
    for (var type in result.data!['GetProductTypes']) {
      types.add(type);
    }
    model.setTypes(types);
    return types;
  }

  @override
  Future<List<Product>> searchProducts({int? page, SearchInput? search}) async {
    // TODO: implement searchProducts

    GraphQLClient client = await Client.createClient();
    String query = r'''
      query SearchProducts($page: Int, $search: SearchProductInput!) {
        SearchProducts(page: $page, search: $search, limit: 25) {
          total
          pageInfo {
            page
            pages
          }
          edges {
            node {
              id
              primaryImage
              name
              barcode
              type
              price
              amount
              category {
                id
                name
              }
            }
          }
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'page': page ?? model.pageInfo.page,
        'search': search == null
            ? SearchInput.empty().copyWith(type: model.selectedTypes).toJson()
            : {
                'name': search.name,
                'minPrice': search.minPrice,
                'maxPrice': search.maxPrice,
                'type': model.selectedTypes,
              },
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      throw Exception(result.exception);
    }
    List<Product> products = [];
    for (var product in result.data!['SearchProducts']['edges']) {
      products.add(Product.fromMap(product['node']));
    }
    model.setProducts(products);

    model.setPageInfo(
      PageInfo.fromMap(result.data!['SearchProducts']['pageInfo']),
    );
    return products;
  }

  @override
  Future<Product> save(Product product) async {
    return implementation.save(product);
  }
}
