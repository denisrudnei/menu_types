import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/interfaces/pos_repository_interface.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:postgres/postgres.dart';

class POSRepositoryPostgres implements POSRepositoryInterface {
  @override
  MyDatabase? db;
  @override
  DeviceInfo? deviceInfo;

  POSRepositoryPostgres();

  @override
  Future<POS> save(POS pos) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    String query = '''
      INSERT INTO POS(
        name,
        "createdAt",
        "updatedAt"
      )
      VALUES(
        @name,
        @created,
        @updated
      )
      RETURNING id
    ''';
    var result = await connection.query(query, substitutionValues: {
      'name': pos.name,
      'hostname': pos.hostname,
      'created': pos.createdAt,
      'updated': pos.updatedAt,
    });
    await connection.close();
    return await getPOSById(result[0][0]) as POS;
  }

  @override
  Future<List<POS>> getPOS() async {
    List<POS> pos = [];
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    String query = '''
      select id, name, hostname, "createdAt", "updatedAt" from pos;
    ''';

    var result = await connection.mappedResultsQuery(query);

    for (var item in result) {
      var newPOS = POS();
      newPOS.id = item['pos']!['id'];
      newPOS.name = item['pos']!['name'];
      newPOS.createdAt = item['pos']!['createdAt'];
      newPOS.updatedAt = item['pos']!['updatedAt'];
      newPOS.hostname = item['pos']!['hostname'];
      pos.add(newPOS);
    }

    await connection.close();
    return pos;
  }

  @override
  Future<POS?> getPOSById(String id) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    String sql = '''
      select id, name, "createdAt", "updatedAt" from pos where id = @id
    ''';

    await connection.open();

    var result = await connection.mappedResultsQuery(
      sql,
      substitutionValues: {
        'id': id,
      },
    );

    await connection.close();

    if (result.isEmpty) return null;

    var pos = POS();

    pos.id = result.first['pos']!['id'];
    pos.name = result.first['pos']!['name'];
    pos.createdAt = result.first['pos']!['createdAt'];
    pos.updatedAt = result.first['pos']!['updatedAt'];

    return pos;
  }

  @override
  Future<POS?> configure(String id, String hostname) async {
    String sql = '''
      UPDATE pos SET hostname = @hostname WHERE id = @id
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': id,
      'hostname': hostname,
    });
    await connection.close();
    return getPOSById(id);
  }

  @override
  Future<List<POS>> getAvailablePOS() async {
    String sql = '''
      SELECT * FROM pos WHERE hostname = ''
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    var result = await connection.mappedResultsQuery(sql);
    await connection.close();

    return result.map((e) => POS.fromPostgres(e)).toList();
  }

  @override
  Future<POS?> getPOSByHostname(String hostname) async {
    String sql = '''
      SELECT
        id,
        name,
        hostname,
        "createdAt",
        "updatedAt"
      FROM pos
      WHERE hostname = @hostname
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    var result = await connection.mappedResultsQuery(sql, substitutionValues: {
      'hostname': hostname,
    });
    await connection.close();
    return POS()
      ..id = result.first['pos']!['id']
      ..name = result.first['pos']!['name']
      ..hostname = result.first['pos']!['hostname']
      ..createdAt = result.first['pos']!['createdAt']
      ..updatedAt = result.first['pos']!['updatedAt'];
  }
}
