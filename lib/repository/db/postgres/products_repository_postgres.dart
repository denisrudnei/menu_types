import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/search_input.dart';
import 'package:menu_types/repository/interfaces/product_repository_interface.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:path_provider/path_provider.dart';
import 'package:postgres/postgres.dart';

class ProductsRepositoryPostgres implements ProductRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  final PurchasesModel purchasesModel;

  @override
  ProductsModel model;

  ProductsRepositoryPostgres({
    required this.purchasesModel,
    required this.model,
  });

  @override
  Future<List<Product>> searchProductsByName(
    String text, {
    int page = 1,
    int limit = 10,
  }) async {
    List<Product> products = [];
    var dir = await getApplicationDocumentsDirectory();
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();

    var values = {
      'name': '%$text%',
      'barcode': text,
      'id': int.tryParse(text),
      'offset': (page - 1) * limit,
      'limit': limit,
    };
    var totalQuery = '''
      SELECT count(*)
        FROM product
        WHERE name ilike @name
          OR barcode = @barcode
          OR id = @id
    ''';

    var total = (await connection.query(
      totalQuery,
      substitutionValues: values,
    ))[0][0];

    String select = '''
      SELECT id, name, description, price, amount, type
      FROM product
      WHERE name ilike @name
        OR barcode = @barcode
        OR id = @id
      OFFSET @offset
      LIMIT @limit
    ''';

    try {
      var result = await connection.query(select, substitutionValues: values);

      for (var item in result) {
        var image = dir.path + '/pdv/products/images/${item[0]}/primary';

        products.add(
          Product(
            id: item[0],
            name: item[1],
            description: item[2],
            price: double.parse(item[3].toString()),
            amount: double.parse(item[4]),
            primaryImage: image,
          ),
        );
      }
      model.setSearched(products);
      model.setPage(page);
      model.setPages((total / limit).ceil());
      model.setTotalSearched(total);
    } catch (e) {
      LoggerUtil.logger.e(e);
    } finally {
      await connection.close();
    }
    return products;
  }

  @override
  Future<List<Product>> getProductsByBarcode(
    String barcode, {
    int limit = 10,
    int page = 1,
  }) async {
    List<Product> products = [];
    var dir = await getApplicationDocumentsDirectory();
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();

    var values = {
      'barcode': barcode,
      'offset': (page - 1) * limit,
      'limit': limit,
      'type': '{ProductType.POS}'
    };
    var totalQuery = '''
      SELECT count(*)
        FROM product
        WHERE type && @type
          AND barcode = @barcode
    ''';

    var total = (await connection.query(
      totalQuery,
      substitutionValues: values,
    ))[0][0];

    String select = '''
      SELECT id, name, description, price, amount, type, barcode
      FROM product
      WHERE type && @type
        AND barcode = @barcode
      OFFSET @offset
      LIMIT @limit
    ''';
    try {
      var result = await connection.query(select, substitutionValues: values);

      for (var item in result) {
        var image = dir.path + '/pdv/products/images/${item[0]}/primary';

        products.add(
          Product(
            id: item[0],
            name: item[1],
            description: item[2],
            price: double.parse(item[3].toString()),
            amount: double.parse(item[4]),
            primaryImage: image,
            barcode: item[6],
          ),
        );
      }
      model.setSearched(products);
      model.setPage(page);
      model.setPages((total / limit).ceil());
      model.setTotalSearched(total);
    } catch (e) {
      LoggerUtil.logger.e(e);
    } finally {
      await connection.close();
    }

    return products;
  }

  @override
  Future<List<String>> getProductTypes() {
    // TODO: implement getProductTypes
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> searchProducts({int? page, SearchInput? search}) {
    // TODO: implement searchProducts
    throw UnimplementedError();
  }

  @override
  Future<Product> save(Product product) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      INSERT INTO product(
        name,
        description,
        amount,
        price,
        type,
        barcode,
        synchronized
      )
      VALUES(
        @name,
        @description,
        @amount,
        @price,
        @type,
        @barcode,
        @synchronized
      )
      ON CONFLICT (id) DO
      UPDATE
      SET name = @name,
      description = @description,
      amount = @amount,
      price = @price,
      type = @type,
      barcode = @barcode,
      synchronized = @synchronized
      RETURNING id
    ''';
    await connection.open();

    await connection.mappedResultsQuery(sql, substitutionValues: {
      'name': product.name,
      'description': product.description,
      'amount': product.amount,
      'price': product.price,
      'type': product.getTypeAsPostgresString(),
      'barcode': product.barcode,
      'synchronized': false,
    });

    await connection.close();

    return product;
  }
}
