import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/repository/interfaces/printer_repository_interface.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:postgres/postgres.dart';

class PrinterRepositoryPostgres implements PrinterRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  PrinterModel model;

  PrinterRepositoryPostgres({this.db, required this.model});

  @override
  Future<List<Printer>> getAllPrinters() async {
    List<Printer> printers = [];
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    String sql = '''
    SELECT id,
          name,
          path,
          manufacturer,
          model,
          type,
          "installedInId"
    FROM  printer 
    ''';
    var result = await connection.query(sql);
    for (var row in result) {
      printers.add(
        Printer(
          id: row[0],
          name: row[1],
          path: row[2],
          manufacturer: row[3],
          model: row[4],
          type: PrinterType.values
              .firstWhere((element) => element.name == row[5]),
          installedIn: await POSRepositoryPostgres().getPOSById(row[6]) as POS,
        ),
      );
    }
    await connection.close();
    model.setPrinters(printers);
    return printers;
  }

  @override
  Future<Printer> create(Printer printer) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    String sql = '''
      INSERT INTO printer(
        id, name, path, manufacturer, model, 
        type, "installedInId"
      ) 
      VALUES 
        (
          @id, @name, @path, @manufacturer, @model, 
          @type, @installedIn
        )
    ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': printer.id,
      'name': printer.name,
      'path': printer.path,
      'manufacturer': printer.manufacturer,
      'model': printer.model,
      'type': printer.type.name,
      'installedIn': printer.installedIn.id,
    });
    await connection.close();
    return printer;
  }
}
