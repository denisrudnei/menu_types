import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_origin.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/repository/db/postgres/user_repository_postgres.dart';
import 'package:menu_types/repository/interfaces/purchase_repository_interface.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:postgres/postgres.dart';

class PurchaseRepositoryPostgres implements PurchaseRepositoryInterface {
  @override
  late MyDatabase db;

  PurchaseRepositoryPostgres(this.db);

  @override
  Future<Purchase> start({
    required POS pos,
    required User operator,
    required PurchaseType type,
  }) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    String sql = '''
      INSERT INTO purchase(
        type,
        status,
        origin,
        "operatorId",
        "userId",
        "createdAt",
        "posId"
      )
      VALUES(
        @type,
        @status,
        @origin,
        @operator,
        @user,
        @createdAt,
        @posId
      )
      RETURNING id
    ''';

    await connection.open();

    var result = await connection.query(sql, substitutionValues: {
      'type': 'NORMAL',
      'status': DeliveryStatus.REQUIRED.toShortString(),
      'origin': PurchaseOrigin.POS.toShortString(),
      'operator': operator.id,
      'user': (await getCurrentPurchase())?.user,
      'createdAt': DateTime.now().toLocal().toIso8601String(),
      'posId': pos.id,
    });

    await connection.close();

    Purchase purchase = (await getPurchase(result[0][0].toString()))!;

    return purchase;
  }

  @override
  Future<Purchase> finish() async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No purchase to finish');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;
    int id = currentPurchase.id!;

    String sql = '''
      UPDATE purchase SET status = @status WHERE id = @id
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    await connection.query(
      sql,
      substitutionValues: {
        'id': currentPurchase.id,
        'status': DeliveryStatus.FINISHED.toShortString(),
      },
    );
    await connection.close();

    return (await getPurchase(id.toString()))!;
  }

  @override
  Future<List<DateTime>> getDaysWithPurchases() async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    List<DateTime> daysWithPurchases = [];
    String sql = '''
      SELECT EXTRACT(YEAR FROM "createdAt") as year, EXTRACT(MONTH FROM "createdAt") as month, EXTRACT(DAY FROM "createdAt") as day from purchase GROUP BY year, month, day;
    ''';
    await connection.open();
    var result = await connection.query(sql);

    for (var row in result) {
      double year = double.parse(row[0]);
      double month = double.parse(row[1]);
      double day = double.parse(row[2]);
      daysWithPurchases.add(
        DateTime(
          year.toInt(),
          month.toInt(),
          day.toInt(),
        ),
      );
    }

    return daysWithPurchases;
  }

  @override
  Future<Purchase?> getPurchase(String id) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      SELECT 
        id,
        "createdAt",
        "userId",
        type,
        status,
        origin,
        "operatorId",
        "posId"
      FROM purchase
      WHERE id = @id;
    ''';
    await connection.open();
    var result = await connection.mappedResultsQuery(
      sql,
      substitutionValues: {
        'id': int.parse(id),
      },
    );

    await connection.close();

    if (result.isEmpty) return null;
    var purchase = Purchase();
    purchase.id = int.parse(id);

    purchase.createdAt = DateTime.parse(
        result.first['purchase']!['createdAt'].toString().substring(0, 19));

    if (result.first['purchase']!['userId'] != null) {
      purchase.user = await UserRepositoryPostgres()
          .getOne(int.parse(result.first['purchase']!['userId'].toString()));
    }

    if (result.first['purchase']!['operatorId'] != null) {
      purchase.operator = await UserRepositoryPostgres().getOne(
          int.parse(result.first['purchase']!['operatorId'].toString()));
    }

    purchase.type = PurchaseType.values.firstWhere(
        (element) => element.name == result.first['purchase']!['type']);

    purchase.status = DeliveryStatus.values.firstWhere(
        (element) => element.name == result.first['purchase']!['status']);

    purchase.origin = PurchaseOrigin.values.firstWhere(
        (element) => element.name == result.first['purchase']!['origin']);

    if (result.first['purchase']!['posId'] != null) {
      purchase.pos = await POSRepositoryPostgres()
          .getPOSById(result.first['purchase']!['posId'].toString());
    }

    purchase.products = await getProductsForPurchase(int.parse(id));

    purchase.payments = await getPaymentsForPurchase(int.parse(id));

    purchase.payment = Payment.grouped(purchase.payments);

    return purchase;
  }

  @override
  Future<List<Purchase>> getPurchasesInDate(DateTime date) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    List<Purchase> purchases = [];
    String sql = '''
      SELECT id FROM purchase WHERE "createdAt" BETWEEN @start and @end;
    ''';
    await connection.open();
    var result = await connection.mappedResultsQuery(
      sql,
      substitutionValues: {
        'start': DateTime(date.year, date.month, date.day, 0, 0, 0)
            .toLocal()
            .toIso8601String(),
        'end': DateTime(date.year, date.month, date.day, 23, 59, 59)
            .toLocal()
            .toIso8601String(),
      },
    );

    await connection.close();

    for (var row in result) {
      var purchase =
          await getPurchase(row['purchase']!['id'].toString()) as Purchase;
      purchases.add(purchase);
    }

    return purchases;
  }

  @override
  Future<HistoryProduct> addProduct(Product product) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();

    String sql = '''
        INSERT INTO history_product(
          "productId", "purchaseId", "dataName", 
          "dataDescription", "dataAmount", 
          "dataPrice", "dataType", "dataBarcode"
        ) 
        VALUES 
          (
            @productId, @purchaseId, @name, @description, 
            @amount, @price, @type, @barcode
          ) RETURNING id
    ''';

    var result = await connection.query(sql, substitutionValues: {
      'productId': product.id,
      'purchaseId': (await getCurrentPurchase())!.id,
      'name': product.name,
      'description': product.description,
      'amount': product.amount,
      'price': product.price,
      'type': product.getTypeAsPostgresString(),
      'barcode': product.barcode,
    });

    await connection.close();

    return HistoryProduct(
      id: result[0][0].toString(),
      productId: product.id,
      data: product,
      purchase: Purchase(),
    );
  }

  @override
  Future<HistoryProduct> subtractProduct(Product product) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
        UPDATE history_product SET "dataAmount" = ("dataAmount" - @amount) WHERE "productId" = @id AND "purchaseId" = @purchaseId
      ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': product.id,
      'purchaseId': (await getCurrentPurchase())!.id,
      'amount': product.amount,
    });

    await connection.close();
    String sqlHistoryProduct = '''
      SELECT * FROM history_product WHERE "productId" = @id AND "purchaseId" = @purchaseId 
    ''';
    connection = await DbConnectionUtil.getConnection();
    await connection.open();
    var results = await connection
        .mappedResultsQuery(sqlHistoryProduct, substitutionValues: {
      'id': product.id,
      'purchaseId': (await getCurrentPurchase())!.id,
    });
    await connection.close();
    if (results.isEmpty) {
      throw Exception('Product not found');
    }
    var result = results.first['history_product']!;
    return HistoryProduct(
      id: result['id'].toString(),
      productId: result['productId'],
      data: Product(
        id: result['productId'],
        amount: double.parse(result['dataAmount']),
        name: result['dataName'],
        price: double.parse(result['dataPrice']),
        description: result['dataDescription'],
        primaryImage: '',
      ),
      purchase: (await getCurrentPurchase())!,
    );
  }

  @override
  Future<void> removeProduct(Product product) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
        DELETE FROM history_product WHERE "productId" = @id AND "purchaseId" = @purchaseId
      ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': product.id,
      'purchaseId': (await getCurrentPurchase())!.id,
    });

    await connection.close();
  }

  @override
  Future<List<Product>> getProductsForPurchase(int purchaseId) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    List<Product> products = [];
    String sql = '''
      SELECT "productId", "dataName", "dataDescription", "dataAmount", "dataPrice", "dataBarcode" from history_product where "purchaseId" = @id;
    ''';
    await connection.open();
    var result = await connection.mappedResultsQuery(sql, substitutionValues: {
      'id': purchaseId,
    });
    await connection.close();
    for (var row in result) {
      products.add(Product(
        id: row['history_product']!['productId'],
        name: row['history_product']!['dataName'],
        description: row['history_product']!['dataDescription'],
        amount: double.parse(row['history_product']!['dataAmount']),
        price: double.parse(row['history_product']!['dataPrice']),
        barcode: row['history_product']!['dataBarcode'],
        primaryImage: '',
      ));
    }
    return products;
  }

  @override
  Future<List<Payment>> addPayment(Payment payment) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      INSERT INTO payment(type, value, paid, change, "purchaseId")
      VALUES(@type, @value, @paid, @change, @purchaseId)
    ''';

    await connection.open();
    await connection.query(
      sql,
      substitutionValues: {
        'type': payment.type,
        'value': payment.value,
        'paid': payment.value,
        'change': 0,
        'purchaseId': currentPurchase.id,
      },
    );

    await connection.close();
    return getPaymentsForPurchase(currentPurchase.id!);
  }

  @override
  Future<List<Payment>> getPaymentsForPurchase(int purchaseId) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    List<Payment> payments = [];
    String sql = '''
      select id, type, value, paid, change from payment where "purchaseId" = @id
    ''';
    await connection.open();

    var result = await connection.query(
      sql,
      substitutionValues: {
        'id': purchaseId,
      },
    );

    await connection.close();

    for (var row in result) {
      var payment = Payment(
        id: row[0].toString(),
        type: row[1],
        value: double.parse(row[2]),
        paid: double.parse(row[3]),
        change: double.parse(row[4]),
      );
      payments.add(payment);
    }

    await connection.close();

    return payments;
  }

  @override
  Future<Purchase?> getCurrentPurchase() async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      SELECT
        id,
        "createdAt",
        "userId",
        type,
        status,
        "establishmentTableId",
        "operatorId",
        origin,
        "posId"
      FROM purchase
      WHERE status NOT IN('${DeliveryStatus.FINISHED.toShortString()}')
      ORDER BY id DESC
    ''';

    await connection.open();

    var result = await connection.mappedResultsQuery(sql);

    await connection.close();
    if (result.isEmpty) return null;
    return Purchase.fromPostgres(
      result.first,
      UserRepositoryPostgres(),
      POSRepositoryPostgres(),
    );
  }

  @override
  Future<Purchase> setClient(User client) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      UPDATE purchase set "userId" = @userId WHERE id = @id
    ''';
    await connection.open();

    await connection.query(
      sql,
      substitutionValues: {
        'id': (await getCurrentPurchase())!.id,
        'userId': client.id,
      },
    );

    await connection.close();

    return (await getCurrentPurchase())!;
  }

  @override
  Future<Purchase> setOperator(User operator) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      UPDATE purchase set "operatorId" = @userId WHERE id = @id
    ''';
    await connection.open();

    await connection.query(
      sql,
      substitutionValues: {
        'id': (await getCurrentPurchase())!.id,
        'operatorId': operator.id,
      },
    );

    await connection.close();
    return (await getCurrentPurchase())!;
  }

  @override
  Future<List<Payment>> setPayment(List<Payment> payment) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    for (var item in payment) {
      await addPayment(item);
    }
    return getPaymentsForPurchase((await getCurrentPurchase())!.id!);
  }

  @override
  Future<List<Product>> setProducts(List<Product> products) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    List<Product> result = [];
    for (var product in products) {
      result.add((await addProduct(product)).data);
    }
    return result;
  }

  @override
  Future<DeliveryStatus> updateStatus(DeliveryStatus status) async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      UPDATE purchase SET status = @status WHERE id = @id
    ''';

    await connection.open();

    await connection.query(sql, substitutionValues: {
      'id': (await getCurrentPurchase())!.id!,
      'status': status.toShortString(),
    });

    await connection.close();

    return status;
  }

  @override
  Future<Purchase> removeClient() async {
    if (await getCurrentPurchase() == null) {
      throw Exception('No current purchase');
    }

    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      UPDATE purchase SET "userId" = @userId WHERE id = @id
    ''';

    await connection.open();

    await connection.query(sql, substitutionValues: {
      'id': (await getCurrentPurchase())!.id!,
      'userId': 'NULL',
    });

    await connection.close();

    return (await getPurchase((await getCurrentPurchase())!.id.toString()))!;
  }

  @override
  Future<void> cancel() async {
    String sql = '''
      DELETE FROM purchase WHERE id = @id
    ''';
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await _deletePayments((await getCurrentPurchase())!.id!);

    await _deleteHistoryProducts((await getCurrentPurchase())!.id!);

    await connection.open();

    await connection.query(sql, substitutionValues: {
      'id': (await getCurrentPurchase())!.id!,
    });

    await connection.close();
  }

  Future<void> _deletePayments(int purchaseId) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      DELETE FROM payment WHERE "purchaseId" = @id
    ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': purchaseId,
    });
    await connection.close();
  }

  Future<void> _deleteHistoryProducts(int purchaseId) async {
    String sql = '''
      DELETE FROM history_product WHERE "purchaseId" = @id
    ''';

    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': purchaseId,
    });
    await connection.close();
  }

  @override
  Future<void> removePayment(Payment payment) async {
    if (payment.id == '') return;
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      DELETE FROM payment WHERE id = @id
    ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'id': int.parse(payment.id),
    });
    await connection.close();
  }
}
