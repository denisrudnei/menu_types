import 'package:menu_types/models/user.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/repository/interfaces/user_repository_interface.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:postgres/postgres.dart';

class UserRepositoryPostgres implements UserRepositoryInterface {
  @override
  Future<Phone> addPhone(Phone phone, int id) {
    // TODO: implement addPhone
    throw UnimplementedError();
  }

  @override
  Future<List<User>> findUsers(String name) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    List<User> users = [];
    String sql = '''
      SELECT id,
             NAME,
             email,
             image,
             active
      FROM   PUBLIC.USER
      WHERE  NAME ilike @name
    ''';

    await connection.open();
    var result = await connection.query(
      sql,
      substitutionValues: {'name': '%$name%'},
    );
    await connection.close();

    for (var item in result) {
      var user = User(name: item[1], email: item[2], image: item[3]);
      user.id = item[0];
      users.add(user);
    }
    return users;
  }

  @override
  Future<List<User>> getUsers() async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    List<User> users = [];

    String sql = '''
      SELECT id,
            NAME,
            email,
            image,
            active
      FROM PUBLIC.USER
      WHERE "deletedAt" IS NULL;
    ''';

    await connection.open();
    var result = await connection.query(sql);
    await connection.close();

    for (var item in result) {
      var user = User(name: item[1], email: item[2], image: item[3]);
      user.id = item[0];
      users.add(user);
    }
    return users;
  }

  @override
  Future<void> removePhone(String phoneId, int id) {
    // TODO: implement removePhone
    throw UnimplementedError();
  }

  @override
  Future<void> remove(int id) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      DELETE FROM PUBLIC.USER
      WHERE  id = @id 
    ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {'id': id});
    await connection.close();
  }

  @override
  Future<User> save(User user) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String sql = '''
      INSERT INTO public.user (
        name,
        email,
        image,
        active,
        password,
        "tempPassword"
      )
      VALUES (
        @name,
        @email,
        @image,
        @active,
        @password,
        ''
      )
      ON CONFLICT (email) DO
      UPDATE
      SET 
      id = PUBLIC.user.id,
      name = @name,
      email = @email,
      image = @image,
      password = @password,
      "tempPassword" = ''
    ''';
    await connection.open();
    await connection.query(sql, substitutionValues: {
      'name': user.name,
      'email': user.email,
      'image': user.image,
      'active': user.active,
      'password': user.password,
    });
    await connection.close();
    return user;
  }

  @override
  Future<User?> getOne(int id) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();

    String sql = '''
      SELECT 
        id, 
        NAME, 
        email, 
        image 
      FROM 
        PUBLIC.user 
      WHERE 
        id = @id
    ''';
    await connection.open();
    var result = await connection.query(sql, substitutionValues: {
      'id': id,
    });

    await connection.close();

    if (result.isEmpty) return null;

    return User(
      name: result.first[1],
      email: result.first[2],
      image: result.first[3],
    );
  }

  @override
  Future<User?> create(String email, String name) async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    String getByEmail = '''
      SELECT id FROM PUBLIC.user WHERE email = @email
    ''';
    await connection.open();
    var users = await connection.query(getByEmail, substitutionValues: {
      'email': email,
    });

    if (users.isNotEmpty) {
      await connection.close();
      return getOne(users[0][0]);
    }

    String insert = '''
      INSERT INTO PUBLIC.user(
        name,
        email,
        password,
        "tempPassword"
      )
      VALUES(
        @name,
        @email,
        '',
        ''
      )
      ON CONFLICT (email) DO UPDATE
      SET id = PUBLIC.user.id,
      name = @name,
      email = @email
      RETURNING *
    ''';

    var result = await connection.mappedResultsQuery(
      insert,
      substitutionValues: {
        'name': name,
        'email': email,
      },
    );

    await connection.close();

    return User.fromPostgres(result.first);
  }
}
