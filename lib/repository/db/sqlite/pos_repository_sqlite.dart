import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/interfaces/pos_repository_interface.dart';
import 'package:menu_types/util/my_database.dart';

class POSRepositorySqlite implements POSRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  late DeviceInfo? deviceInfo;

  POSRepositorySqlite({required this.db});

  @override
  Future<POS> save(POS pos) async {
    var result = await db!
        .into(db!.driftPOSModel)
        .insertReturning(DriftPOSModelCompanion.insert(
          name: pos.name,
          createdAt: pos.createdAt ?? DateTime.now(),
          updatedAt: pos.updatedAt ?? DateTime.now(),
        ));
    return POS.fromDrift(result);
  }

  @override
  Future<List<POS>> getPOS() async {
    List<POS> pos = [];
    var result = await db!.select(db!.driftPOSModel).get();

    for (var item in result) {
      var newPOS = POS();
      newPOS.id = item.id;
      newPOS.name = item.name;
      newPOS.createdAt = item.createdAt;
      newPOS.updatedAt = item.updatedAt;
      pos.add(newPOS);
    }

    return pos;
  }

  @override
  Future<POS?> getPOSById(String id) async {
    var result = await (db!.select(db!.driftPOSModel)
          ..where((tbl) => tbl.id.equals(id)))
        .getSingleOrNull();

    if (result == null) return null;

    var pos = POS();

    pos.id = result.id;
    pos.name = result.name;
    pos.createdAt = result.createdAt;
    pos.updatedAt = result.updatedAt;

    return pos;
  }

  @override
  Future<POS?> configure(String id, String hostname) {
    throw UnimplementedError();
  }

  @override
  Future<List<POS>> getAvailablePOS() async {
    return [];
  }

  @override
  Future<POS?> getPOSByHostname(String hostname) async {
    var result = await (db!.select(db!.driftPOSModel)
          ..where((tbl) => tbl.hostname.equals(hostname)))
        .get();
    if (result.isEmpty) return null;
    return POS.fromDrift(result.first);
  }
}
