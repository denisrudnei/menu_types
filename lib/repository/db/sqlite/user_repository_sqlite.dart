import 'package:drift/drift.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/repository/interfaces/user_repository_interface.dart';
import 'package:menu_types/util/my_database.dart';

class UserRepositorySqlite implements UserRepositoryInterface {
  late MyDatabase db;
  UserRepositorySqlite({required this.db});

  @override
  Future<Phone> addPhone(Phone phone, int id) async {
    var result = await db.into(db.driftPhoneModel).insertReturning(
          DriftPhoneModelCompanion.insert(
            number: phone.number,
            description: phone.description,
            userId: id,
          ),
        );
    return Phone.fromDrift(result);
  }

  @override
  Future<List<User>> findUsers(String name) async {
    var result = await db.customSelect(
        'select id, name, email, image, active from drift_user_model where name like ?',
        variables: [
          Variable('%$name%'),
        ]).get();

    List<User> users = [];
    for (var item in result) {
      var user = item.data;
      users.add(User(
        name: user['name'],
        email: user['email'],
        image: user['image'],
      ));
    }
    return users;
  }

  @override
  Future<List<User>> getUsers() async {
    var result = await (db.select(db.driftUserModel)
          ..where((tbl) => tbl.deletedAt.isNull()))
        .get();
    return result.map((user) => User.fromMap(user.toJson())).toList();
  }

  @override
  Future<void> removePhone(String phoneId, int id) async {
    await (db.delete(db.driftPhoneModel)
          ..where(
            (tbl) => tbl.id.equals(int.parse(phoneId)) & tbl.userId.equals(id),
          ))
        .go();
  }

  @override
  Future<void> remove(int id) async {
    await (db.update(db.driftUserModel)..where((tbl) => tbl.id.equals(id)))
        .write(DriftUserModelCompanion(
      deletedAt: Value(DateTime.now()),
    ));
  }

  @override
  Future<User> save(User user) async {
    var result = await db.into(db.driftUserModel).insertReturning(
          DriftUserModelCompanion.insert(name: user.name, email: user.email),
        );

    return (await getOne(result.id))!;
  }

  @override
  Future<User?> getOne(int id) async {
    var result = await (db.select(db.driftUserModel)
          ..where((tbl) => tbl.id.equals(id)))
        .getSingleOrNull();

    if (result == null) return null;
    User user = User.fromDrift(result);
    var phones = await (db.select(db.driftPhoneModel)
          ..where((tbl) => tbl.userId.equals(user.id)))
        .get();
    for (var phone in phones) {
      user.phones.add(Phone.fromDrift(phone));
    }
    return user;
  }

  @override
  Future<User?> create(String email, String name) async {
    var inDb = await (db.select(db.driftUserModel)
          ..where((tbl) => tbl.email.equals(email)))
        .getSingleOrNull();

    if (inDb != null) return User.fromDrift(inDb);

    var newUser = await db
        .into(db.driftUserModel)
        .insertReturning(DriftUserModelCompanion.insert(
          name: name,
          email: email,
        ));
    return getOne(newUser.id);
  }
}
