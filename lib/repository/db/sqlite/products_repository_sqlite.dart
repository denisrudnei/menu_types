import 'package:drift/drift.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/search_input.dart';
import 'package:menu_types/repository/interfaces/product_repository_interface.dart';
import 'package:menu_types/util/my_database.dart';

class ProductsRepositorySqlite implements ProductRepositoryInterface {
  @override
  final MyDatabase db;

  @override
  final PurchasesModel purchasesModel;

  @override
  final ProductsModel model;

  ProductsRepositorySqlite({
    required this.db,
    required this.purchasesModel,
    required this.model,
  });

  @override
  Future<List<Product>> searchProductsByName(
    String text, {
    int page = 1,
    int limit = 10,
  }) async {
    var where = 'WHERE name like ? LIMIT ? OFFSET ?';

    List<Variable<dynamic>> whereVariables = [
      Variable('%$text%'),
      Variable(limit),
      Variable((page - 1) * limit),
    ];

    var countResult = await db.customSelect(
      'select count(*) as total from drift_product_model WHERE name like ?',
      variables: [
        Variable('%$text%'),
      ],
    ).getSingle();

    int total = int.parse(countResult.data['total'].toString());

    var result = await db
        .customSelect(
          'SELECT * FROM drift_product_model $where',
          variables: whereVariables,
        )
        .get();

    List<Product> products = [];

    for (var item in result) {
      var product = item.data;
      products.add(Product(
        id: product['id'],
        name: product['name'],
        description: product['description'] ?? '',
        primaryImage: product['primaryImage'] ?? '',
        amount: product['amount'],
        price: product['price'],
      ));
    }

    model.setSearched(products);

    model.setPage(page);

    model.setPages((total / limit).ceil());
    model.setTotalSearched(total);

    return products;
  }

  @override
  Future<List<Product>> getProductsByBarcode(
    String barcode, {
    int limit = 10,
    int page = 1,
  }) async {
    List<Product> products = [];
    var countResult = await db.customSelect(
      'select count(*) as total from drift_product_model WHERE barcode = ?',
      variables: [
        Variable(barcode),
      ],
    ).getSingle();

    int total = int.parse(countResult.data['total'].toString());

    var result = await (db.select(db.driftProductModel)
          ..where((tbl) => tbl.barcode.equals(barcode))
          ..limit(limit, offset: (page - 1) * limit))
        .get();

    for (var item in result) {
      products.add(Product(
        id: item.id,
        name: item.name,
        price: item.price,
        amount: item.amount,
        description: item.description,
        barcode: item.barcode,
        primaryImage: item.primaryImage,
      ));
    }
    model.setSearched(products);
    model.setPage(page);
    model.setPages((total / limit).ceil());
    model.setTotalSearched(total);
    return products;
  }

  @override
  Future<List<String>> getProductTypes() {
    // TODO: implement getProductTypes
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> searchProducts({int? page, SearchInput? search}) {
    // TODO: implement searchProducts
    throw UnimplementedError();
  }

  @override
  Future<Product> save(Product product) {
    // TODO: implement save
    throw UnimplementedError();
  }
}
