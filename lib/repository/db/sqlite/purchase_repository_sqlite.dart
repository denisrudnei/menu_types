import 'package:drift/drift.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_origin.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/repository/db/sqlite/user_repository_sqlite.dart';
import 'package:menu_types/repository/interfaces/purchase_repository_interface.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseRepositorySqlite implements PurchaseRepositoryInterface {
  @override
  late MyDatabase db;

  PurchaseRepositorySqlite(this.db);

  @override
  Future<Purchase> start({
    required PurchaseType type,
    required User operator,
    required POS pos,
  }) async {
    var result = await db.into(db.driftPurchaseModel).insertReturning(
          DriftPurchaseModelCompanion.insert(
            totalAmount: const Value(0),
            totalPrice: const Value(0),
            operatorId: operator.id!,
            posId: pos.id,
            type: Value(type.toShortString()),
            origin: Value(PurchaseOrigin.POS.toShortString()),
          ),
        );

    Purchase purchase = Purchase(
      id: result.id,
      pos: await POSRepositorySqlite(db: db).getPOSById(result.posId),
    );

    return purchase;
  }

  @override
  Future<Purchase> finish() async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;
    var prefs = await SharedPreferences.getInstance();

    var pos = prefs.getString('pos');
    if (pos == null) throw Exception('POS not configured');

    int id = currentPurchase.id!;

    double totalAmount = (await getCurrentPurchase())!.products.fold<double>(
        0, (previousValue, element) => previousValue + element.amount);

    double totalPrice = (await getCurrentPurchase())!.products.fold<double>(
        0, (previousValue, element) => previousValue + element.total);

    await (db.update(db.driftPurchaseModel)
          ..where(
            (tbl) => tbl.id.equals(currentPurchase.id!),
          ))
        .write(
      DriftPurchaseModelCompanion(
        totalAmount: Value(totalAmount),
        totalPrice: Value(totalPrice),
        status: Value(DeliveryStatus.FINISHED.toShortString()),
      ),
    );

    return (await getPurchase(id.toString()))!;
  }

  @override
  Future<List<DateTime>> getDaysWithPurchases() async {
    Map<String, DateTime> map = {};

    var dates = await db
        .customSelect('select created_at from drift_purchase_model')
        .get();
    for (var date in dates) {
      DateTime parsed = DateTime.fromMillisecondsSinceEpoch(
        (date.data['created_at'] * 1000),
      );
      map.addAll({
        '${parsed.year}-${parsed.month}-${parsed.day}': parsed,
      });
    }
    return map.values.toList();
  }

  @override
  Future<Purchase?> getPurchase(String id) async {
    var result = await (db.select(db.driftPurchaseModel)
          ..where((tbl) => tbl.id.equals(int.parse(id))))
        .getSingleOrNull();
    if (result == null) return null;
    Purchase purchase = Purchase();
    purchase.id = result.id;

    if (result.userId != null) {
      purchase.user = await UserRepositorySqlite(db: db).getOne(result.userId!);
    }

    purchase.operator =
        await UserRepositorySqlite(db: db).getOne(result.operatorId);

    purchase.products = await getProductsForPurchase(int.parse(id));

    purchase.payments = await getPaymentsForPurchase(int.parse(id));

    purchase.payment = Payment.grouped(purchase.payments);

    purchase.type = PurchaseType.values
        .firstWhere((element) => element.toShortString() == result.type);

    purchase.status = DeliveryStatus.values
        .firstWhere((element) => element.toShortString() == result.status);

    purchase.origin = PurchaseOrigin.values
        .firstWhere((element) => element.toShortString() == result.origin);

    return purchase;
  }

  @override
  Future<List<Purchase>> getPurchasesInDate(DateTime date) async {
    List<Purchase> purchases = [];
    var start = DateTime(date.year, date.month, date.day, 0, 0, 0).toLocal();
    var end = DateTime(date.year, date.month, date.day, 23, 59, 59).toLocal();

    var result = await (db.select(db.driftPurchaseModel)
          ..where(
              (tbl) => tbl.createdAt.isBetween(Constant(start), Constant(end))))
        .get();

    for (var item in result) {
      Purchase purchase = await getPurchase(item.id.toString()) as Purchase;
      purchases.add(purchase);
    }

    return purchases;
  }

  @override
  Future<List<Product>> getProductsForPurchase(int purchaseId) async {
    List<Product> products = [];
    var result = await (db.select(db.driftHistoryProductModel)
          ..where((tbl) => tbl.purchaseId.equals(purchaseId)))
        .get();

    for (var item in result) {
      products.add(Product.fromHistoryDrift(item));
    }
    return products;
  }

  @override
  Future<List<Product>> setProducts(List<Product> products) async {
    List<HistoryProduct> historyProducts = [];

    await db.transaction(() async {
      for (var product in products) {
        var newHistoryProduct = await db
            .into(db.driftHistoryProductModel)
            .insertReturning(DriftHistoryProductModelCompanion.insert(
              productId: product.id,
              purchaseId: (await getCurrentPurchase())!.id!,
              dataName: product.name,
              dataBarcode: product.barcode,
              dataDescription: product.description,
              dataAmount: product.amount,
              dataPrice: product.price,
            ));
        historyProducts.add(
          HistoryProduct(
            id: newHistoryProduct.id.toString(),
            productId: product.id,
            data: product,
            purchase: (await getCurrentPurchase())!,
          ),
        );
      }
    });

    return getProductsForPurchase((await getCurrentPurchase())!.id!);
  }

  @override
  Future<List<Payment>> addPayment(Payment payment) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    await db.into(db.driftPaymentModel).insert(
          DriftPaymentModelCompanion.insert(
            value: payment.value,
            paid: payment.value,
            change: 0,
            purchaseId: (await getCurrentPurchase())!.id!,
          ),
        );

    return getPaymentsForPurchase((await getCurrentPurchase())!.id!);
  }

  @override
  Future<List<Payment>> getPaymentsForPurchase(int purchaseId) async {
    List<Payment> payments = [];
    var result = await (db.select(db.driftPaymentModel)
          ..where((tbl) => tbl.purchaseId.equals(purchaseId)))
        .get();

    for (var item in result) {
      payments.add(Payment.fromDrift(item));
    }

    return payments;
  }

  @override
  Future<HistoryProduct> addProduct(Product product) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    var result = await (db
        .into(db.driftHistoryProductModel)
        .insertReturning(DriftHistoryProductModelCompanion.insert(
          productId: product.id,
          purchaseId: (await getCurrentPurchase())!.id!,
          dataName: product.name,
          dataBarcode: product.barcode,
          dataDescription: product.description,
          dataAmount: product.amount,
          dataPrice: product.price,
        )));
    return HistoryProduct(
      id: result.id.toString(),
      productId: result.productId,
      purchase: (await getCurrentPurchase())!,
      data: product,
    );
  }

  @override
  Future<void> removeProduct(Product product) async {
    await (db.delete(db.driftHistoryProductModel)
          ..where((tbl) => tbl.id.equals(product.id)))
        .go();
  }

  @override
  Future<Purchase?> getCurrentPurchase() async {
    UserRepositorySqlite userRepository = UserRepositorySqlite(db: db);
    POSRepositorySqlite posRepository = POSRepositorySqlite(db: db);

    var result = await (db.select(db.driftPurchaseModel)
          ..where((tbl) =>
              tbl.status.equals(DeliveryStatus.REQUIRED.toShortString())))
        .get();

    if (result.isEmpty) return null;

    return Purchase(
        id: result.first.id,
        products: await getProductsForPurchase(result.first.id),
        user: await userRepository.getOne(result.first.id),
        operator: await userRepository.getOne(result.first.operatorId),
        created: result.first.createdAt,
        pos: await posRepository.getPOSById(result.first.posId),
        payments: await getPaymentsForPurchase(result.first.id),
        type: PurchaseType.values.firstWhere(
            (element) => element.toShortString() == result.first.type),
        origin: PurchaseOrigin.values.firstWhere(
            (element) => element.toShortString() == result.first.origin));
  }

  @override
  Future<Purchase> setClient(User client) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;

    db.update(db.driftPurchaseModel)
      ..where(
        (tbl) => tbl.id.equals(currentPurchase.id),
      )
      ..write(DriftPurchaseModelCompanion(
        userId: Value(client.id),
      ));

    return getPurchase((await getCurrentPurchase())!.id.toString())
        as Future<Purchase>;
  }

  @override
  Future<Purchase> setOperator(User operator) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;

    db.update(db.driftPurchaseModel)
      ..where(
        (tbl) => tbl.id.equals(currentPurchase.id),
      )
      ..write(DriftPurchaseModelCompanion(
        operatorId: Value(operator.id!),
      ));

    return getPurchase(currentPurchase.id.toString()) as Future<Purchase>;
  }

  @override
  Future<List<Payment>> setPayment(List<Payment> payment) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }

    for (Payment item in payment) {
      await db
          .into(db.driftPaymentModel)
          .insert(DriftPaymentModelCompanion.insert(
            value: item.value,
            paid: item.paid,
            change: item.change,
            purchaseId: (await getCurrentPurchase())!.id!,
          ));
    }

    return (await getCurrentPurchase())!.payments;
  }

  @override
  Future<DeliveryStatus> updateStatus(DeliveryStatus status) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;
    db.update(db.driftPurchaseModel)
      ..where((tbl) => tbl.id.equals(currentPurchase.id!))
      ..write(DriftPurchaseModelCompanion(
        status: Value(status.toShortString()),
      ));
    return (await getPurchase(currentPurchase.id.toString()) as Purchase)
        .status;
  }

  @override
  Future<Purchase> removeClient() async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    Purchase currentPurchase = (await getCurrentPurchase())!;
    db.update(db.driftPurchaseModel)
      ..where((tbl) => tbl.id.equals(currentPurchase.id!))
      ..write(const DriftPurchaseModelCompanion(
        userId: Value(null),
      ));
    return (await getPurchase((await getCurrentPurchase())!.id!.toString()))!;
  }

  @override
  Future<void> cancel() {
    // TODO: implement cancel
    throw UnimplementedError();
  }

  @override
  Future<void> removePayment(Payment payment) async {
    await db.driftPaymentModel
        .deleteWhere((tbl) => tbl.id.equals(int.parse(payment.id)));
  }

  @override
  Future<HistoryProduct> subtractProduct(Product product) {
    // TODO: implement subtractProduct
    throw UnimplementedError();
  }
}
