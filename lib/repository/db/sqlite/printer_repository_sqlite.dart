import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/repository/interfaces/printer_repository_interface.dart';
import 'package:menu_types/util/my_database.dart';

class PrinterRepositorySqlite implements PrinterRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  PrinterModel model;

  PrinterRepositorySqlite({required this.db, required this.model});

  @override
  Future<List<Printer>> getAllPrinters() async {
    List<Printer> printers = [];
    var result = await db!.select(db!.driftPrinterModel).get();
    for (var item in result) {
      printers.add(await Printer.fromDrift(db!, item));
    }
    model.setPrinters(printers);
    return printers;
  }

  @override
  Future<Printer> create(Printer printer) async {
    await (db!.into(db!.driftPrinterModel).insert(
          DriftPrinterModelCompanion.insert(
            id: printer.name,
            name: printer.id,
            manufacturer: printer.manufacturer,
            model: printer.model,
            type: printer.type.name,
            path: printer.path,
            installedInId: printer.installedIn.id,
            createdAt: DateTime.now(),
            updatedAt: DateTime.now(),
          ),
        ));
    return printer;
  }
}
