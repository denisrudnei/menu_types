abstract class RepositoryInterface<T> {
  late T implementation;
  Future<void> setImplementation();
}
