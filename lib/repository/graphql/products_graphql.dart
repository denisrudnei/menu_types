import 'package:graphql/client.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/search_input.dart';
import 'package:menu_types/repository/interfaces/product_repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';

class ProductGraphql implements ProductRepositoryInterface {
  @override
  final PurchasesModel purchasesModel;

  @override
  final ProductsModel model;

  ProductGraphql({
    required this.purchasesModel,
    required this.model,
  });

  @override
  Future<List<Product>> searchProductsByName(
    String text, {
    int page = 1,
    int limit = 10,
  }) async {
    List<Product> products = [];
    GraphQLClient client = await Client.createClient();
    String query = r'''
    query SearchProducts($search: SearchProductInput!, $page: Int, $limit: Int) {
      SearchProducts(search: $search, page: $page, limit: $limit) {
        total
        edges {
          node {
            id
            name
            price
            amount
            primaryImage
            description
          }
        }
        pageInfo {
          page
          pages
        }
      }
    }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'search': {
          'name': text,
          'type': const ['POS'],
        },
        'page': page,
        'limit': limit,
      },
    );
    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var pageInfo = result.data!['SearchProducts']['pageInfo'];
    model.setTotalSearched(result.data!['SearchProducts']['total']);
    model.setPage(pageInfo['page']);
    model.setPages(pageInfo['pages']);

    for (var edge in result.data!['SearchProducts']['edges']) {
      var product = edge['node'];
      Product item = Product.fromMap(product);

      products.add(item);
    }
    model.setSearched(products);
    return products;
  }

  @override
  MyDatabase get db => throw UnimplementedError();

  @override
  Future<List<Product>> getProductsByBarcode(
    String barcode, {
    int limit = 10,
    int page = 1,
  }) async {
    GraphQLClient client = await Client.createClient();
    List<Product> products = [];
    String query = r'''
      query GetProductsByBarcode($limit: Int, $page: Int, $barcode: String!) {
        GetProductByBarcode (limit: $limit, page: $page, barcode: $barcode) {
          total
          edges {
            node {
              id
              name
              price
              amount
              primaryImage
              description
            }
          }
          pageInfo {
            page
            pages
          }
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'barcode': barcode,
        'limit': limit,
        'page': page,
      },
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    var pageInfo = result.data!['GetProductsByBarcode']['pageInfo'];

    model.setTotalSearched(result.data!['GetProductsByBarcode']['total']);
    model.setPage(pageInfo['page']);
    model.setPages(pageInfo['pages']);

    for (var edge in result.data!['GetProductsByBarcode']['edges']) {
      var product = edge['node'];
      Product item = Product.fromMap(product);

      products.add(item);
    }

    model.setSearched(products);

    return products;
  }

  @override
  Future<List<String>> getProductTypes() {
    // TODO: implement getProductTypes
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> searchProducts({int? page, SearchInput? search}) {
    // TODO: implement searchProducts
    throw UnimplementedError();
  }

  @override
  Future<Product> save(Product product) {
    // TODO: implement save
    throw UnimplementedError();
  }
}
