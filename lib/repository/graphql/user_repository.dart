import 'package:graphql/client.dart';
import 'package:menu_types/models/address.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/interfaces/user_repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

class UserRepositoryGraphql implements UserRepositoryInterface {
  static final _client = Client.createClient();
  @override
  Future<List<User>> getUsers() async {
    var client = await _client;
    List<User> users = [];
    String query = r'''
      query {
        GetUsers {
          id
          name
          image
          email
        }
      }
    ''';

    QueryOptions options = QueryOptions(document: gql(query));
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      return [];
    }

    for (var user in result.data!['GetUsers']) {
      User newUser = User(
        name: user['name'],
        email: user['email'],
        image: user['image'],
      );
      newUser.id = int.parse(user['id']);
      users.add(newUser);
    }

    return users;
  }

  @override
  Future<List<User>> findUsers(String name) async {
    var client = await _client;
    List<User> users = [];
    String query = r'''
      query FindUsers($name: String!) {
        FindUsers (name: $name) {
          id
          name
          image
          email
          active
          addresses {
            id
            zipCode
            country
            city
            street
            number
            district
            state
            fullName
          }
          phones {
            id
            description
            number
          }
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
      fetchPolicy: FetchPolicy.networkOnly,
      variables: {
        'name': name,
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      return [];
    }

    for (var user in result.data!['FindUsers']) {
      User newUser = User(
        name: user['name'],
        email: user['email'],
        image: user['image'],
      );
      newUser.id = int.parse(user['id'].toString());
      for (var phone in user['phones']) {
        newUser.phones.add(Phone(
          id: phone['id'].toString(),
          number: phone['number'],
          description: phone['description'],
        ));
      }

      for (var address in user['addresses']) {
        newUser.addresses.add(Address(
          id: address['id'],
          zipCode: address['zipCode'],
          country: address['country'],
          city: address['city'],
          street: address['street'],
          number: address['number'],
          state: address['state'],
          fullName: address['fullName'],
        ));
      }
      newUser.id = int.parse(user['id'].toString());
      newUser.active = user['active'];
      users.add(newUser);
    }

    return users;
  }

  @override
  Future<Phone> addPhone(Phone phone, int userId) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation CreatePhone($phone: CreatePhoneInput!, $user: ID) {
        CreatePhone(phone: $phone, user: $user) {
          id
          number
          description
        }
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'phone': {
          'description': phone.description,
          'number': phone.number,
        },
        'user': userId,
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    return Phone.fromMap(result.data!['CreatePhone']);
  }

  @override
  Future<void> removePhone(String id, int userId) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation RemovePhone($id: ID!, $user: ID) {
        RemovePhone(id: $id, user: $user)
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'user': userId,
      },
    );
    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
  }

  @override
  Future<void> remove(int id) {
    // TODO: implement remove
    throw UnimplementedError();
  }

  @override
  Future<User> save(User user) {
    // TODO: implement save
    throw UnimplementedError();
  }

  @override
  Future<User?> getOne(int id) async {
    throw UnimplementedError();
  }

  @override
  Future<User?> create(String email, String name) {
    // TODO: implement create
    throw UnimplementedError();
  }
}
