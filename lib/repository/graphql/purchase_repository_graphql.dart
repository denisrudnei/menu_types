import 'package:graphql/client.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/interfaces/purchase_repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseRepositoryGraphql implements PurchaseRepositoryInterface {
  @override
  late MyDatabase db;

  PurchaseRepositoryGraphql();

  @override
  Future<Purchase> finish() async {
    GraphQLClient client = await Client.createClient();
    var prefs = await SharedPreferences.getInstance();
    var pos = prefs.getString('pos');
    if (pos == null) throw Exception('POS not configured');
    String mutation = r'''
      mutation PurchaseFromPOS($user: ID, $pos: ID!,$products: [ProductForPurchaseInput!]!, $payment: [PaymentInput!]!) {
        PurchaseFromPOS(user: $user, pos: $pos, products: $products, payment: $payment) {
          id
          createdAt
          totalPrice
          pos {
            id
            name
            hostname
            createdAt
            updatedAt
          }
          user {
            id
            name
            email
            image
          }
          operator {
            id
            name
            email
            image
          }
          payments {
            id
            type
            value
            paid
            change
          }
          products {
            id
            productId
            data {
              id
              name
              price
              amount
              description
              primaryImage
            }
          }
        }
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'user': (await getCurrentPurchase())?.user?.id,
        'pos': pos,
        'products': (await getCurrentPurchase())!
            .products
            .map(
              (e) => {'id': e.id, 'amount': e.amount},
            )
            .toList(),
        'payment': (await getCurrentPurchase())!
            .payments
            .map(
              (e) => {'type': e.type, 'value': e.value, 'paid': e.value},
            )
            .toList()
      },
    );

    var result = await client.mutate(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var purchaseFromPOS = result.data!['PurchaseFromPOS'];

    return Purchase.fromMap(purchaseFromPOS);
  }

  @override
  Future<Purchase?> getPurchase(String id) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetPurchase($id: ID!) {
        GetPurchase(id: $id) {
          id
          createdAt
          totalPrice
          pos {
            id
            name
            hostname
            createdAt
            updatedAt
          }
          user {
            id
            name
            email
            image
          }
          operator {
            id
            name
            email
            image
          }
          products {
            id
            productId
            data {
              id
              name
              price
              amount
              description
              primaryImage
            }
          }
          payments {
            id
            type
            paid
            change
            value
          }
          payment {
            id
            type
            paid
            change
            value
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'id': id,
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    if (result.data == null) return null;
    return Purchase.fromMap(result.data!['GetPurchase']);
  }

  @override
  Future<List<Purchase>> getPurchasesInDate(DateTime date) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetPurchasesInDate($date: DateTime) {
        GetPurchasesInDate(date: $date) {
          id
          createdAt
          totalPrice
          pos {
            id
            name
            hostname
            createdAt
            updatedAt
          }
          user {
            id
            name
            email
            image
          }
          operator {
            id
            name
            email
            image
          }
          products {
            id
            productId
            data {
              id
              name
              price
              amount
              description
              primaryImage
            }
          }
          payments {
            id
            type
            paid
            change
            value
          }
          payment {
            id
            type
            paid
            change
            value
          }
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'date': date.toUtc().toIso8601String(),
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    if (result.data == null) return [];
    List<Purchase> purchases = [];
    for (var purchase in result.data!['GetPurchasesInDate']) {
      purchases.add(Purchase.fromMap(purchase));
    }

    return purchases;
  }

  @override
  Future<List<DateTime>> getDaysWithPurchases() async {
    GraphQLClient client = await Client.createClient();
    String query = '''
      query {
        DaysWithPurchases
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    List<DateTime> daysWithPurchases = [];

    for (var day in result.data!['DaysWithPurchases']) {
      daysWithPurchases.add(DateTime.parse(day).toUtc());
    }
    return daysWithPurchases;
  }

  @override
  Future<List<Payment>> addPayment(Payment payment) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    throw UnimplementedError();
  }

  @override
  Future<HistoryProduct> addProduct(Product product) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    throw UnimplementedError();
  }

  @override
  Future<Purchase?> getCurrentPurchase() async {
    throw UnimplementedError();
  }

  @override
  Future<List<Payment>> getPaymentsForPurchase(int purchaseId) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getProductsForPurchase(int purchaseId) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    throw UnimplementedError();
  }

  @override
  Future<Purchase> setClient(User client) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }

    (await getCurrentPurchase())!.user = client;
    return (await getCurrentPurchase())!;
  }

  @override
  Future<Purchase> setOperator(User operator) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    (await getCurrentPurchase())!.operator = operator;
    return (await getCurrentPurchase())!;
  }

  @override
  Future<List<Payment>> setPayment(List<Payment> payment) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    (await getCurrentPurchase())!.payments = payment;
    return payment;
  }

  @override
  Future<List<Product>> setProducts(List<Product> products) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    (await getCurrentPurchase())!.products = products;
    return products;
  }

  @override
  Future<Purchase> start({
    required POS pos,
    required User operator,
    required PurchaseType type,
  }) async {
    return (await getCurrentPurchase())!;
  }

  @override
  Future<DeliveryStatus> updateStatus(DeliveryStatus status) async {
    if ((await getCurrentPurchase()) == null) {
      throw Exception('No current purchase');
    }
    throw UnimplementedError();
  }

  @override
  Future<Purchase> removeClient() {
    // TODO: implement removeClient
    throw UnimplementedError();
  }

  @override
  Future<void> cancel() {
    // TODO: implement cancel
    throw UnimplementedError();
  }

  @override
  Future<void> removePayment(Payment payment) {
    // TODO: implement removePayment
    throw UnimplementedError();
  }

  @override
  Future<void> removeProduct(Product product) {
    // TODO: implement removeProduct
    throw UnimplementedError();
  }

  @override
  Future<HistoryProduct> subtractProduct(Product product) {
    // TODO: implement subtractProduct
    throw UnimplementedError();
  }
}
