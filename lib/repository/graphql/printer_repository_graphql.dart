import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/repository/interfaces/printer_repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';

import 'package:graphql/client.dart';
import 'package:menu_types/util/my_database.dart';

class PrinterRepositoryGraphql implements PrinterRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  PrinterModel model;

  PrinterRepositoryGraphql({this.db, required this.model});

  @override
  Future<List<Printer>> getAllPrinters() async {
    List<Printer> printers = [];
    var client = await Client.createClient();
    String query = '''
    query {
      GetAllPrinters {
        id
        name
        manufacturer
        model
        path
        type
        installedIn {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
    }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    for (var printer in result.data!['GetAllPrinters']) {
      printer['installedIn']['purchases'] = [];
      printer['installedIn']['loadLogs'] = [];
      printers.add(Printer.fromJson(printer));
    }

    model.setPrinters(printers);
    return printers;
  }

  @override
  Future<Printer> create(Printer printer) async {
    var client = await Client.createClient();
    String mutation = r'''
      mutation CreatePrinter($printer: CreatePrinterInput!) {
        CreatePrinter(printer: $printer) {
          id
          name
          path
          manufacturer
          model
          type
          installedIn {
            id
            name
            hostname
          }
        }
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'printer': {
          'name': printer.name,
          'path': printer.path,
          'manufacturer': printer.manufacturer,
          'model': printer.model,
          'type': printer.type.name,
          'installedIn': printer.installedIn.id,
        },
      },
    );
    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    return printer;
  }
}
