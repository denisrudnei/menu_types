import 'package:graphql/client.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/interfaces/pos_repository_interface.dart';
import 'package:menu_types/util/graphql/client.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';

class POSRepositoryGraphql implements POSRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  DeviceInfo? deviceInfo;

  POSRepositoryGraphql({this.db, required this.deviceInfo});

  @override
  Future<POS> save(POS pos) {
    // TODO: implement save
    throw UnimplementedError();
  }

  @override
  Future<List<POS>> getPOS() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query {
        GetPOS {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    List<POS> pos = [];

    for (var newPOS in result.data!['GetPOS']) {
      newPOS['purchases'] = [];
      newPOS['loadLogs'] = [];
      pos.add(POS.fromJson(newPOS));
    }

    return pos;
  }

  @override
  Future<POS?> getPOSById(String id) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
    query GetOnePOS($id: ID!) {
      GetOnePOS(id: $id) {
        id
        name
        hostname
        createdAt
        updatedAt
      }
    }
    ''';

    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'id': id,
      },
    );

    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    if (result.data!['GetOnePOS'] == null) return null;
    result.data?['GetOnePOS']['purchases'] = [];
    result.data?['GetOnePOS']['loadLogs'] = [];
    return POS.fromJson(result.data!['GetOnePOS']);
  }

  @override
  Future<POS?> configure(String id, String hostname) async {
    GraphQLClient client = await Client.createClient();
    String mutation = r'''
      mutation ConfigurePOS($id: ID!, $hostname: String!) {
        ConfigurePOS(id: $id, hostname: $hostname) {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
    ''';

    MutationOptions options = MutationOptions(
      document: gql(mutation),
      variables: {
        'id': id,
        'hostname': hostname,
      },
    );

    var result = await client.mutate(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }
    var data = result.data!['ConfigurePOS'];
    data['purchases'] = [];
    data['loadLogs'] = [];
    return POS.fromJson(data);
  }

  @override
  Future<List<POS>> getAvailablePOS() async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetAvailablePOS($hostname: String!) {
        GetAvailablePOS(hostname: $hostname) {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'hostname': deviceInfo?.hostname ?? '',
      },
    );
    var result = await client.query(options);

    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    List<POS> pos = [];

    for (var item in result.data!['GetAvailablePOS']) {
      item['purchases'] = [];
      item['loadLogs'] = [];
      pos.add(POS.fromJson(item));
    }

    return pos;
  }

  @override
  Future<POS?> getPOSByHostname(String hostname) async {
    GraphQLClient client = await Client.createClient();
    String query = r'''
      query GetPOSByHostname($hostname: String!) {
        GetPOSByHostname(hostname: $hostname) {
          id
          name
          hostname
          createdAt
          updatedAt
        }
      }
    ''';
    QueryOptions options = QueryOptions(
      document: gql(query),
      variables: {
        'hostname': hostname,
      },
    );
    var result = await client.query(options);
    if (result.hasException) {
      LoggerUtil.logger.e(result.exception);
      throw Exception(result.exception);
    }

    if (result.data?['GetPOSByHostname'] == null) return null;
    result.data?['GetPOSByHostname']['purchases'] = [];
    result.data?['GetPOSByHostname']['loadLogs'] = [];
    return POS.fromJson(result.data!['GetPOSByHostname']);
  }
}
