import 'dart:io';

import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/graphql/pos_repository_graphql.dart';
import 'package:menu_types/repository/interfaces/pos_repository_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:menu_types/models/configuration/connection_server_model.dart';

class POSRepository implements POSRepositoryInterface {
  @override
  MyDatabase? db;

  @override
  DeviceInfo? deviceInfo;

  POSRepository({required this.db, required this.deviceInfo});

  @override
  Future<POS> save(POS pos) async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool(searchInServerFirstString) ?? false;
    if (searchInServerFirst) {
      return POSRepositoryGraphql(deviceInfo: deviceInfo).save(pos);
    }
    try {
      if (Platform.isAndroid || Platform.isIOS) {
        return POSRepositorySqlite(db: db).save(pos);
      } else {
        return POSRepositoryPostgres().save(pos);
      }
    } catch (e) {
      LoggerUtil.logger.e(e);
      return POSRepositoryGraphql(deviceInfo: deviceInfo).save(pos);
    }
  }

  @override
  Future<List<POS>> getPOS() async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool(searchInServerFirstString) ?? false;
    if (searchInServerFirst) {
      return POSRepositoryGraphql(deviceInfo: deviceInfo).getPOS();
    }

    try {
      if (Platform.isAndroid || Platform.isIOS) {
        return POSRepositorySqlite(db: db).getPOS();
      } else {
        return POSRepositoryPostgres().getPOS();
      }
    } catch (e) {
      LoggerUtil.logger.e(e);

      return POSRepositoryGraphql(deviceInfo: deviceInfo).getPOS();
    }
  }

  @override
  Future<POS?> getPOSById(String id) {
    return POSRepositoryGraphql(deviceInfo: deviceInfo).getPOSById(id);
  }

  @override
  Future<POS?> configure(String id, String hostname) {
    return POSRepositoryGraphql(deviceInfo: deviceInfo).configure(id, hostname);
  }

  @override
  Future<List<POS>> getAvailablePOS() {
    return POSRepositoryGraphql(deviceInfo: deviceInfo).getAvailablePOS();
  }

  @override
  Future<POS?> getPOSByHostname(String hostname) {
    return POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOSByHostname(hostname);
  }
}
