import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/util/my_database.dart';

abstract class PrinterRepositoryInterface {
  late MyDatabase? db;
  final PrinterModel model;
  PrinterRepositoryInterface({this.db, required this.model});
  Future<List<Printer>> getAllPrinters();
  Future<Printer> create(Printer printer);
}
