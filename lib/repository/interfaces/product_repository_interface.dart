import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/models/search_input.dart';
import 'package:menu_types/util/my_database.dart';

abstract class ProductRepositoryInterface {
  final MyDatabase? db;
  final PurchasesModel purchasesModel;
  final ProductsModel model;
  ProductRepositoryInterface({
    required this.db,
    required this.purchasesModel,
    required this.model,
  });

  Future<List<Product>> searchProductsByName(
    String text, {
    int page = 1,
    int limit = 10,
  });

  Future<List<Product>> searchProducts({int? page, SearchInput? search});

  Future<List<Product>> getProductsByBarcode(
    String barcode, {
    int limit = 10,
    int page = 1,
  });

  Future<List<String>> getProductTypes();

  Future<Product> save(Product product);
}
