import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/history_product.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/my_database.dart';

abstract class PurchaseRepositoryInterface {
  late MyDatabase db;

  PurchaseRepositoryInterface();

  /// Validate and make a purchase
  Future<Purchase> start({
    required POS pos,
    required User operator,
    required PurchaseType type,
  });

  Future<DeliveryStatus> updateStatus(DeliveryStatus status);

  Future<Purchase> finish();

  /// Set current operator in this operations
  Future<Purchase> setOperator(User operator);

  /// Set client for purchase
  Future<Purchase> setClient(User client);

  Future<Purchase> removeClient();

  Future<List<Product>> setProducts(List<Product> products);

  Future<HistoryProduct> addProduct(Product product);

  Future<HistoryProduct> subtractProduct(Product product);

  Future<void> removeProduct(Product product);

  Future<List<Payment>> addPayment(Payment payment);

  Future<void> removePayment(Payment payment);

  Future<List<Payment>> setPayment(List<Payment> payment);

  Future<Purchase?> getCurrentPurchase();

  Future<Purchase?> getPurchase(String id);

  Future<List<Purchase>> getPurchasesInDate(DateTime date);

  Future<List<DateTime>> getDaysWithPurchases();

  Future<List<Product>> getProductsForPurchase(int purchaseId);

  Future<List<Payment>> getPaymentsForPurchase(int purchaseId);

  Future<void> cancel();
}
