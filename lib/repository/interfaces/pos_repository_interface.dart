import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/util/my_database.dart';

abstract class POSRepositoryInterface {
  MyDatabase? db;

  DeviceInfo? deviceInfo;

  POSRepositoryInterface({this.db, required this.deviceInfo});

  Future<POS> save(POS pos);

  Future<List<POS>> getPOS();

  Future<POS?> getPOSById(String id);

  Future<List<POS>> getAvailablePOS();

  Future<POS?> configure(String id, String hostname);

  Future<POS?> getPOSByHostname(String hostname);
}
