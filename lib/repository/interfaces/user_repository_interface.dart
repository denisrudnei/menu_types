import 'package:menu_types/models/phone.dart';
import 'package:menu_types/models/user.dart';

abstract class UserRepositoryInterface {
  Future<List<User>> getUsers();
  Future<List<User>> findUsers(String name);
  Future<Phone> addPhone(Phone phone, int id);
  Future<void> removePhone(String phoneId, int id);
  Future<User> save(User user);
  Future<void> remove(int id);
  Future<User?> getOne(int id);
  Future<User?> create(String email, String name);
}
