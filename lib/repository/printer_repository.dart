import 'dart:io';

import 'package:menu_types/models/configuration/connection_server_model.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/repository/db/postgres/printer_repository_postgres.dart';
import 'package:menu_types/repository/db/sqlite/printer_repository_sqlite.dart';
import 'package:menu_types/repository/graphql/printer_repository_graphql.dart';
import 'package:menu_types/repository/interfaces/printer_repository_interface.dart';
import 'package:menu_types/util/logger_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrinterRepository implements PrinterRepositoryInterface {
  @override
  MyDatabase? db;

  PrinterRepository({required this.db, required this.model});

  @override
  PrinterModel model;

  @override
  Future<List<Printer>> getAllPrinters() async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool(searchInServerFirstString) ?? false;
    if (searchInServerFirst) {
      return PrinterRepositoryGraphql(model: model).getAllPrinters();
    }

    try {
      if (Platform.isAndroid || Platform.isIOS) {
        return PrinterRepositorySqlite(db: db!, model: model).getAllPrinters();
      } else {
        return PrinterRepositoryPostgres(model: model).getAllPrinters();
      }
    } catch (e) {
      LoggerUtil.logger.e(e);

      return PrinterRepositoryGraphql(model: model).getAllPrinters();
    }
  }

  @override
  Future<Printer> create(Printer printer) async {
    var prefs = await SharedPreferences.getInstance();
    var searchInServerFirst = prefs.getBool(searchInServerFirstString) ?? false;
    if (searchInServerFirst) {
      return PrinterRepositoryGraphql(model: model).create(printer);
    }

    try {
      if (Platform.isAndroid || Platform.isIOS) {
        return PrinterRepositorySqlite(db: db!, model: model).create(printer);
      } else {
        return PrinterRepositoryPostgres(model: model).create(printer);
      }
    } catch (e) {
      LoggerUtil.logger.e(e);

      return PrinterRepositoryGraphql(model: model).create(printer);
    }
  }
}
