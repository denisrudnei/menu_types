import 'dart:io';
import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/purchase.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/util/pdf/pdf_purchase_util.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

void main() {
  test('Should create a PDF file for purchase', () async {
    var purchase = _getPurchase();

    var base = await getApplicationDocumentsDirectory();
    var directory = p.join(base.path, 'purchases');
    for (var type in SortProductPrint.values) {
      var data = await PdfPurchaseUtil.generateForPurchase(
        purchase,
        sortType: type,
      );

      var name = '${purchase.id} - $type.pdf';

      var file = File(p.join(directory, name));
      file.create(recursive: true);
      file.writeAsBytesSync(data);
    }
  });

  test('Should create a PDF file with a list of purchases', () async {
    List<Purchase> purchases = [];
    for (var i = 0; i < Random().nextInt(100); i++) {
      purchases.add(_getPurchase(id: i + 1));
    }
    var base = await getApplicationDocumentsDirectory();
    var directory = p.join(base.path, 'purchases');
    var data = await PdfPurchaseUtil.generateForList(purchases);

    var file = File(p.join(directory, 'purchases.pdf'));
    file.create(recursive: true);
    file.writeAsBytesSync(data);
  });
}

Purchase _getPurchase({int? id = 1}) {
  Purchase purchase = Purchase();

  purchase.id = id;
  for (var i = 0; i < 200; i++) {
    var amount = Random().nextInt(5).toDouble();

    var product = Product(
      id: 1,
      name: 'Product #$i',
      price: Random().nextDouble() * 100,
      amount: amount == 0 ? 1 : amount,
      primaryImage: '',
      description: 'Description #i',
    );

    if (Random().nextBool()) {
      product.discount = Random().nextDouble() * 100;
    }

    purchase.products.add(product);
  }

  purchase.user = User(
    name: 'User ${Random().nextInt(100)}',
    image: '',
    email: 'client@mail.com',
  );

  purchase.operator = User(
    name: 'Operator ${Random().nextInt(100)}',
    image: '',
    email: 'operator@mail.com',
  );

  purchase.payments = [
    Payment(
      id: '#1',
      type: 'Cartão de crédito',
      value: purchase.totalPrice / 3,
    ),
    Payment(
      id: '#2',
      type: 'Dinheiro',
      value: purchase.totalPrice / 3,
    ),
    Payment(
      id: '#3',
      type: 'Cartão de débito',
      value: purchase.totalPrice / 3,
    ),
  ];

  return purchase;
}
