import 'package:drift/native.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/repository/db/postgres/products_repository_postgres.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;
  late PurchasesModel purchasesModel;
  late ProductsModel model;

  setUp(() async {
    WidgetsFlutterBinding.ensureInitialized();
    db = MyDatabase.test(NativeDatabase.memory());
    model = ProductsModel();
    purchasesModel = PurchasesModel();
  });

  tearDownAll(() async {
    await db.close();
  });

  test('test search using postgres', () async {
    var repository = ProductsRepositoryPostgres(
      purchasesModel: purchasesModel,
      model: model,
    );

    await repository.searchProductsByName('search');
  });
}
