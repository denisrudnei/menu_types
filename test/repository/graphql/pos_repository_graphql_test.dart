import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/graphql/pos_repository_graphql.dart';

void main() {
  late DeviceInfo deviceInfo;
  setUpAll(() async {
    deviceInfo = DeviceInfo();
  });

  test('Should return all POS', () async {
    List<POS> pos = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOS();

    expect(pos.isEmpty, false);
  });

  test('Should return on POS by id', () async {
    List<POS> pos = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOS();

    var result = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOSById(pos.first.id);

    expect(result, isNotNull);
  });

  test('Should return POS by id', () async {
    List<POS> pos = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOS();
    var id = pos.first.id;

    var result = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOSById(id);

    expect(result, isNotNull);
  });

  test('Should return available POS', () async {
    var result = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getAvailablePOS();
    expect(result.isEmpty, false);
  });

  test('Should return pos by hostname', () async {
    var result = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOSByHostname('unknown');
    expect(result, isNull);
  });

  test('Should configure POS', () async {
    var available = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getAvailablePOS();
    var pos = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOS();
    if (available.isEmpty) {
      try {
        await POSRepositoryGraphql(
          deviceInfo: deviceInfo,
        ).configure(pos.first.id, 'test machine');
        fail('Not throw');
      } catch (e) {
        expect(e, isInstanceOf<Exception>());
      }
    } else {
      var result = await POSRepositoryGraphql(
        deviceInfo: deviceInfo,
      ).configure(
        pos.firstWhere((element) => element.name == 'test machine').id,
        'test machine',
      );
      expect(result, isNotNull);
    }
  });
}
