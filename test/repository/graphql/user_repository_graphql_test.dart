import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/repository/graphql/user_repository.dart';

void main() {
  late UserRepositoryGraphql repository;

  setUpAll(() async {
    repository = UserRepositoryGraphql();
  });

  test('Find users', () async {
    var users = await repository.findUsers('operador');
    expect(users.length, greaterThan(0));
  });

  test('Get all users', () async {
    var users = await repository.getUsers();
    expect(users.isNotEmpty, true);
  });
}
