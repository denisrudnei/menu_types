import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/graphql/pos_repository_graphql.dart';
import 'package:menu_types/repository/graphql/printer_repository_graphql.dart';
import 'package:uuid/uuid.dart';

void main() {
  late PrinterModel printerModel;

  setUpAll(() async {
    printerModel = PrinterModel();
  });

  test('Should create a new printer', () async {
    var printer = Printer(
        id: const Uuid().v4(),
        name: 'New printer',
        path: '/path/to/printer',
        type: PrinterType.LASER,
        manufacturer: 'Manufacturer',
        model: 'model',
        installedIn:
            (await POSRepositoryGraphql(deviceInfo: DeviceInfo()).getPOS())
                .first);

    await PrinterRepositoryGraphql(model: printerModel).create(printer);
  });

  test('Should return all printers', () async {
    var printers =
        await PrinterRepositoryGraphql(model: printerModel).getAllPrinters();
    expect(printers.isEmpty, false);
  });
}
