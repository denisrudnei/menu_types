import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/graphql/pos_repository_graphql.dart';
import 'package:menu_types/repository/graphql/purchase_repository_graphql.dart';
import 'package:menu_types/repository/graphql/user_repository.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late PurchaseRepositoryGraphql purchaseRepositoryGraphql;
  late DeviceInfo deviceInfo;

  setUpAll(() async {
    purchaseRepositoryGraphql = PurchaseRepositoryGraphql();
    deviceInfo = DeviceInfo();
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Finish purchase', () async {
    var allPos = await POSRepositoryGraphql(
      deviceInfo: deviceInfo,
    ).getPOS();
    (await SharedPreferences.getInstance()).setString('pos', allPos.first.id);
    List<Product> products = [];

    var users = await UserRepositoryGraphql().getUsers();

    await purchaseRepositoryGraphql.start(
      pos: allPos.first,
      operator: users.first,
      type: PurchaseType.NORMAL,
    );

    products.add(Product(
      id: 1,
      name: 'test',
      price: 0,
      primaryImage: '',
      description: 'test',
    ));

    await purchaseRepositoryGraphql.setProducts(products);

    await purchaseRepositoryGraphql.finish();
  });
}
