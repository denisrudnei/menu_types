import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/postgres/user_repository_postgres.dart';

void main() {
  late UserRepositoryPostgres repository;

  setUp(() async {
    repository = UserRepositoryPostgres();
  });

  test('Should return all users', () async {
    await repository.getUsers();
  });

  test('Should save user in database', () async {
    var user = User(
      name: 'test',
      email: 'test@email.com ${Random().nextInt(99999)}',
      image: '',
    );
    await repository.save(user);
  });

  test('Should not add a user with same email', () async {
    var user = User(
      name: 'User',
      email: 'test@test.com',
      image: '',
    );
    await repository.save(user);
    var users = await repository.getUsers();
    var length = users.length;
    await repository.save(User(
      name: users.first.name,
      email: users.first.email,
      image: users.first.image,
    ));

    users = await repository.getUsers();
    expect(users.length, length);
  });

  test('Should find all users with matching name', () async {
    var user = User(
      name: 'test',
      email: 'test@email.com',
      image: '',
    );

    await repository.save(user);

    var users = await repository.findUsers('test');

    expect(users.length, greaterThanOrEqualTo(1));
  });

  test('Should create a new user', () async {
    var users = await repository.getUsers();
    var total = users.length;
    await repository.create(
        'User ${DateTime.now().millisecondsSinceEpoch}', 'new user');
    users = await repository.getUsers();
    expect(users.length, total + 1);
  });
}
