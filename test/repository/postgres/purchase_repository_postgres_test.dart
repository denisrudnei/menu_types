import 'dart:math';

import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/delivery_status.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/payment/payment.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/repository/db/postgres/purchase_repository_postgres.dart';
import 'package:menu_types/repository/db/postgres/user_repository_postgres.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;
  late PurchaseRepositoryPostgres repository;

  setUpAll(() async {
    db = MyDatabase.test(NativeDatabase.memory());

    repository = PurchaseRepositoryPostgres(
      db,
    );
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Finish purchase [POSTGRES]', () async {
    var users = await UserRepositoryPostgres().getUsers();
    var pos = await POSRepositoryPostgres().getPOS();
    await repository.start(
      pos: pos.first,
      operator: users.first,
      type: PurchaseType.NORMAL,
    );

    List<Product> products = [
      Product(
        id: 1,
        name: 'Test product #1',
        price: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #1',
      ),
      Product(
        id: 1,
        name: 'Test product #2',
        price: Random().nextDouble() * 100,
        amount: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #2',
      ),
      Product(
        id: 1,
        name: 'Test product #3',
        price: Random().nextDouble() * 100,
        amount: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #3',
      ),
      Product(
        id: 1,
        name: 'Test product #4',
        price: Random().nextDouble() * 100,
        amount: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #4',
      )
    ];

    await repository.setProducts(products);

    List<Payment> payment = [
      Payment(
        id: '#1',
        type: 'CREDIT_CARD',
        value: products.fold<double>(
            0, (previousValue, element) => previousValue + element.total),
      )
    ];

    await repository.setPayment(payment);

    var purchase = await repository.finish();

    expect(purchase, isNotNull);
  });

  test('Should return days with purchases', () async {
    var users = await UserRepositoryPostgres().getUsers();
    var pos = await POSRepositoryPostgres().getPOS();
    await repository.start(
      pos: pos.first,
      operator: users.first,
      type: PurchaseType.NORMAL,
    );
    await repository.finish();

    var daysWithPurchases = await repository.getDaysWithPurchases();

    expect(daysWithPurchases.length, greaterThan(0));
  });

  test('Should return all purchases in date', () async {
    var users = await UserRepositoryPostgres().getUsers();
    var pos = await POSRepositoryPostgres().getPOS();
    expect(users.isNotEmpty, true, reason: 'No users');
    expect(pos.isNotEmpty, true, reason: 'No POS');
    await repository.start(
      pos: pos.first,
      operator: users.first,
      type: PurchaseType.NORMAL,
    );
    await repository.finish();

    var result = await repository.getPurchasesInDate(DateTime.now());
    expect(result.isEmpty, false);
  });

  test('Should update status for purchase', () async {
    await UserRepositoryPostgres().save(
      User(
        name: 'name',
        email: 'email',
        image: 'image',
      ),
    );
    var users = await UserRepositoryPostgres().getUsers();
    var pos = await POSRepositoryPostgres().getPOS();
    await repository.start(
      pos: pos.first,
      operator: users.first,
      type: PurchaseType.NORMAL,
    );
    var purchase = await repository.getCurrentPurchase();

    await repository.updateStatus(DeliveryStatus.DELIVERY_PROCESS);
    purchase = await repository.getCurrentPurchase();

    expect(purchase?.status, DeliveryStatus.DELIVERY_PROCESS);
    await repository.finish();
  });
}
