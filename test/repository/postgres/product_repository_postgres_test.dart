import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/product_type.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/repository/db/postgres/products_repository_postgres.dart';

void main() {
  late ProductsRepositoryPostgres repository;
  ProductsModel productsModel = ProductsModel();
  setUpAll(() {
    repository = ProductsRepositoryPostgres(
      purchasesModel: PurchasesModel(),
      model: productsModel,
    );
  });
  test('Should return products by barcode', () async {
    TestWidgetsFlutterBinding.ensureInitialized();
    List<Product> products = await repository.getProductsByBarcode('barcode');

    expect(products, isEmpty);

    Product product = Product(
      id: 0,
      name: 'test product',
      description: '',
      price: 10.0,
      amount: 10,
      primaryImage: '',
    )
      ..type = [ProductType.POS]
      ..barcode = 'barcode';

    await repository.save(product);

    products = await repository.getProductsByBarcode('barcode');

    expect(products.length, greaterThan(0));
  });

  test('Should search products by name', () async {
    await repository.save(Product(
      id: 0,
      name: 'product by name',
      description: 'test',
      primaryImage: '',
      price: 0,
      amount: 0,
    ));
    var products = await repository.searchProductsByName('product by name');
    expect(products, isNotEmpty);
  });
}
