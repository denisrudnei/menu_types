import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/db/postgres/pos_repository_postgres.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:postgres/postgres.dart';

void main() {
  late POSRepositoryPostgres repository;

  setUp(() {
    repository = POSRepositoryPostgres();
  });

  test('Return all POS', () async {
    List<POS> pos = await repository.getPOS();

    expect(pos.isEmpty, false);
  });

  test('Should return one pos', () async {
    List<POS> pos = await repository.getPOS();

    var one = await repository.getPOSById(pos.first.id);

    expect(one, isNotNull);
  });

  test('Should configure POS', () async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    await connection.query("UPDATE pos SET hostname = ''");
    await connection.close();
    var available = await repository.getAvailablePOS();
    int length = available.length;
    expect(available, isNotEmpty);
    await repository.configure(available.first.id, 'test pos');
    available = await repository.getAvailablePOS();
    expect(available.length, length - 1);
  });

  test('Should get POS by hostname', () async {
    PostgreSQLConnection connection = await DbConnectionUtil.getConnection();
    await connection.open();
    await connection.query("UPDATE pos SET hostname = 'test'");
    await connection.close();
    var pos = await repository.getPOS();
    var withHostname = pos.firstWhere((element) => element.hostname != '');
    var result = await repository.getPOSByHostname(withHostname.hostname);
    expect(result, isNotNull);
  });
}
