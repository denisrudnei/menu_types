import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/repository/db/postgres/printer_repository_postgres.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/pos_repository.dart';
import 'package:menu_types/util/setup_tests.dart';
import 'package:uuid/uuid.dart';

void main() {
  late PrinterModel printerModel;
  late DeviceInfo deviceInfo;

  setUpAll(() async {
    printerModel = PrinterModel();
    deviceInfo = DeviceInfo();
  });

  test('Should create a new printer', () async {
    var pos = POS();
    pos.name = 'test';
    var newPos = await POSRepository(db: SetupTests.db, deviceInfo: deviceInfo)
        .save(pos);
    var printer = Printer(
      id: const Uuid().v4(),
      name: 'New printer',
      path: '/path/to/printer',
      type: PrinterType.LASER,
      manufacturer: 'Manufacturer',
      model: 'model',
      installedIn: newPos,
    );

    await PrinterRepositoryPostgres(model: printerModel).create(printer);
  });

  test('Should return all printers', () async {
    var printers =
        await PrinterRepositoryPostgres(model: printerModel).getAllPrinters();
    expect(printers.isEmpty, false);
  });
}
