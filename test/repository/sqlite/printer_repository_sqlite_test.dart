import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/printer_type.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/printing/printer.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/repository/db/sqlite/printer_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:uuid/uuid.dart';

void main() {
  late MyDatabase db;
  late PrinterModel printerModel;

  setUpAll(() async {
    db = MyDatabase.test(NativeDatabase.memory());
    printerModel = PrinterModel();
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Should create a new printer', () async {
    await POSRepositorySqlite(db: db).save(POS());
    var printer = Printer(
        id: const Uuid().v4(),
        name: 'New printer',
        path: '/path/to/printer',
        type: PrinterType.LASER,
        manufacturer: 'Manufacturer',
        model: 'model',
        installedIn: (await POSRepositorySqlite(db: db).getPOS()).first);

    await PrinterRepositorySqlite(db: db, model: printerModel).create(printer);
  });

  test('Should return all printers', () async {
    await POSRepositorySqlite(db: db).save(POS());
    var pos = (await POSRepositorySqlite(db: db).getPOS()).first;
    await PrinterRepositorySqlite(
      db: db,
      model: printerModel,
    ).create(
      Printer(
        name: 'test printer',
        manufacturer: 'testing',
        model: 'testing',
        path: '/home/testing',
        installedIn: pos,
      ),
    );

    var printers = await PrinterRepositorySqlite(
      db: db,
      model: printerModel,
    ).getAllPrinters();

    expect(printers.isEmpty, false);
  });
}
