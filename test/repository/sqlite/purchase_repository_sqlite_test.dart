import 'dart:math';

import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/enums/purchase_type.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/models/product.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/repository/db/sqlite/purchase_repository_sqlite.dart';
import 'package:menu_types/repository/db/sqlite/user_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;

  late PurchaseRepositorySqlite purchaseRepository;
  late User user;
  late POS pos;

  setUpAll(() async {
    db = MyDatabase.test(NativeDatabase.memory());
    purchaseRepository = PurchaseRepositorySqlite(db);
    var posList = await POSRepositorySqlite(db: db).getPOS();
    var users = await UserRepositorySqlite(db: db).getUsers();
    if (users.isEmpty) {
      user = await UserRepositorySqlite(db: db).create('test', 'test') as User;
    } else {
      user = users.first;
    }

    if (posList.isEmpty) {
      var toSave = POS()
        ..name = 'test'
        ..hostname = 'test machine';
      pos = await POSRepositorySqlite(db: db).save(toSave);
    } else {
      pos = posList.first;
    }
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Should register purchase', () async {
    await purchaseRepository.start(
      type: PurchaseType.NORMAL,
      operator: user,
      pos: pos,
    );

    List<Product> products = [
      Product(
        id: 1,
        name: 'Test product #1',
        price: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #1',
      ),
      Product(
        id: 1,
        name: 'Test product #2',
        price: Random().nextDouble() * 100,
        amount: Random().nextDouble() * 100,
        primaryImage: 'https://picsum.photos/500/500',
        description: 'Description #2',
      ),
    ];

    purchaseRepository.setProducts(products);

    await purchaseRepository.finish();

    var purchases = await db.select(db.driftPurchaseModel).get();

    expect(purchases.isEmpty, false);
  });

  test('Should return purchases in date', () async {
    var result =
        await PurchaseRepositorySqlite(db).getPurchasesInDate(DateTime.now());

    expect(result.isNotEmpty, true);
  });

  test('Should return days with purchases', () async {
    var result = await PurchaseRepositorySqlite(db).getDaysWithPurchases();

    expect(result.isEmpty, false);
  });
}
