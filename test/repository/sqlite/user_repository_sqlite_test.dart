import 'dart:math';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/phone.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/db/sqlite/user_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;
  late UserRepositorySqlite repository;

  setUpAll(() async {
    db = MyDatabase.test(NativeDatabase.memory());
    repository = UserRepositorySqlite(db: db);
  });

  tearDownAll(() async {
    await db.close();
  });

  _addUser() async {
    await db.into(db.driftUserModel).insert(
          DriftUserModelCompanion.insert(
            name: 'user to remove',
            email: 'remove@email.com',
            image: const Value(''),
          ),
        );
  }

  test('Should return list of users', () async {
    await db.delete(db.driftUserModel).go();
    List<User> users = await repository.getUsers();

    expect(users.length, 0);

    await db.into(db.driftUserModel).insert(DriftUserModelCompanion.insert(
          name: 'user test 1',
          email: 'user1@email.com',
          image: const Value(''),
        ));

    users = await repository.getUsers();
    expect(users.length, 1);
  });

  test('Should return users with same name', () async {
    await db.delete(db.driftUserModel).go();

    await db.into(db.driftUserModel).insert(DriftUserModelCompanion.insert(
          name: 'user test 1',
          email: 'user1@email.com',
          image: const Value(''),
        ));

    List<User> users = await repository.findUsers('user');
    users = await repository.getUsers();

    expect(users.length, 1);

    await db.into(db.driftUserModel).insert(DriftUserModelCompanion.insert(
          name: 'new user',
          email: 'new@email.com',
          image: const Value(''),
        ));

    users = await repository.findUsers('new');

    expect(users.length, 1);
  });

  test('Should save a new user', () async {
    var users = await repository.getUsers();
    var length = users.length;
    await repository.save(User(
      email: 'Test user ${Random().nextInt(999)}',
      name: 'new user',
      image: '',
      password: '',
    ));
    users = await repository.getUsers();
    expect(users.length, length + 1);
  });

  test('Should remove user', () async {
    await _addUser();
    var users = await repository.getUsers();
    var length = users.length;
    await repository.remove(users.first.id!);
    users = await repository.getUsers();
    expect(users.length, length - 1);
  });

  test('Should add a phone to user', () async {
    await _addUser();
    var users = await repository.getUsers();
    var user = users.first;
    var length = user.phones.length;
    await repository.addPhone(
      Phone(
        number: '(00) 00000-0000',
        description: 'Test number',
      ),
      user.id!,
    );
    user = (await repository.getOne(user.id!))!;
    expect(user.phones.length, length + 1);
  });

  test('Should remove a phone from user', () async {
    await _addUser();
    var users = await repository.getUsers();
    var user = users.first;
    Phone phone = await repository.addPhone(
      Phone(
        number: '(00) 00000-0000',
        description: 'Phone number',
      ),
      user.id!,
    );
    user = (await repository.getOne(user.id!))!;
    int length = user.phones.length;
    await repository.removePhone(phone.id, user.id!);
    user = (await repository.getOne(user.id!))!;
    expect(length, user.phones.length + 1);
  });

  test('Should not add new user with same email', () async {
    await _addUser();
    var users = await repository.getUsers();
    var length = users.length;
    await repository.create(users.first.email, users.first.name);
    users = await repository.getUsers();
    expect(users.length, length);
  });
}
