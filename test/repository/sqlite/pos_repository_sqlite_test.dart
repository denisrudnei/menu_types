import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/pos.dart';
import 'package:menu_types/repository/db/sqlite/pos_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;

  setUpAll(() async {
    db = MyDatabase.test(NativeDatabase.memory());
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Should return all POS', () async {
    List<POS> pos = [];
    await db.into(db.driftPOSModel).insert(
          DriftPOSModelCompanion.insert(
            name: 'test',
            createdAt: DateTime.now(),
            updatedAt: DateTime.now(),
          ),
        );
    pos = await POSRepositorySqlite(db: db).getPOS();
    expect(pos.isEmpty, false);
  });

  test('Should return one POS', () async {
    List<POS> pos = await POSRepositorySqlite(db: db).getPOS();

    var result = await POSRepositorySqlite(db: db).getPOSById(pos.first.id);

    expect(result, isNotNull);
  });
}
