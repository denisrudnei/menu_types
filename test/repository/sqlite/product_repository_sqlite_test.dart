import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/repository/db/sqlite/products_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late MyDatabase db;
  late ProductsRepositorySqlite repository;
  ProductsModel model = ProductsModel();
  PurchasesModel purchasesModel = PurchasesModel();
  setUp(() async {
    db = MyDatabase.test(NativeDatabase.memory());
    repository = ProductsRepositorySqlite(
      db: db,
      purchasesModel: purchasesModel,
      model: model,
    );
  });

  tearDown(() async {
    await db.close();
  });

  test('Should search products by barcode', () async {
    await db.delete(db.driftProductModel).go();

    expect(await repository.getProductsByBarcode('barcode'), isEmpty);
    db.into(db.driftProductModel).insert(
          DriftProductModelCompanion.insert(
            name: 'test',
            barcode: const Value('barcode'),
          ),
        );
    expect(await repository.getProductsByBarcode('barcode'), isNotEmpty);
  });
}
