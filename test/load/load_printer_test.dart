import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/repository/db/postgres/printer_repository_postgres.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/load/printer_load_service.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late DbConnectionUtil connectionUtil;
  DbModel model = DbModel(db: db);
  LogModel logModel = LogModel();
  LoadModel loadModel = LoadModel();
  late LoadLogRepository loadLogRepository;

  late PrinterLoadService printerLoadService;

  setUpAll(() async {
    connectionUtil = DbConnectionUtil(
      model: model,
      logModel: logModel,
      loadModel: loadModel,
    );

    await SetupTests.login();
    await DbConnectionUtil.getConnection();

    loadLogRepository = LoadLogRepository(
      model: model,
      loadModel: loadModel,
      logModel: logModel,
    );

    printerLoadService = PrinterLoadService(
      dbModel: model,
      loadLogRepository: loadLogRepository,
      dbConnectionUtil: connectionUtil,
    );
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Download printers and save in db [SQLITE]', () async {
    await db.delete(db.driftPrinterModel).go();
    await connectionUtil.fullPopulate();
    await printerLoadService.updatePrintersSqlite(connectionUtil.printers);
    var result = await db.select(db.driftPrinterModel).get();
    expect(result.length, greaterThan(0));
  });

  test('Download printers and save in db [POSTGRES]', () async {
    await connectionUtil.fullPopulate();
    printerLoadService.populate(connectionUtil.printers);
    await printerLoadService.updatePrintersPostgres(connectionUtil.printers);
    var result =
        await PrinterRepositoryPostgres(model: PrinterModel()).getAllPrinters();
    expect(result.length, greaterThan(0));
  });

  test('Delete printers [POSTGRES]', () async {
    await connectionUtil.fullPopulate();

    await printerLoadService
        .deletePrintersPostgres(connectionUtil.deletedPrinters);
  });

  test('Delete printers [SQLITE]', () async {
    await connectionUtil.fullPopulate();
    await printerLoadService
        .deletePrintersSqlite(connectionUtil.deletedPrinters);
  });
}
