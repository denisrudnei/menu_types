import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late DbConnectionUtil connectionUtil;
  DbModel model = DbModel(db: db);
  LogModel logModel = LogModel();
  LoadModel loadModel = LoadModel();

  setUpAll(() async {
    WidgetsFlutterBinding.ensureInitialized();
    connectionUtil = DbConnectionUtil(
      model: model,
      logModel: logModel,
      loadModel: loadModel,
    );

    await SetupTests.login();
    await DbConnectionUtil.getConnection();
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Download POS data', () async {
    connectionUtil.pos.clear();

    expect(connectionUtil.pos.isEmpty, true);

    await connectionUtil.fullPopulate();

    expect(connectionUtil.pos.isEmpty, false);
  });

  test('Run load test', () async {
    await connectionUtil.fullLoad();
  });

  test('Run partial load test', () async {
    await connectionUtil.partialLoad();
  });
}
