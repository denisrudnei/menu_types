import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/load/postgres/user_load_service_postgres.dart';
import 'package:menu_types/util/load/sqlite/user_load_service_sqlite.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late DbConnectionUtil connectionUtil;
  DbModel model = DbModel(db: db);
  LogModel logModel = LogModel();
  LoadModel loadModel = LoadModel();
  late LoadLogRepository loadLogRepository;

  late UserLoadServiceSqlite userLoadServiceSqlite;
  late UserLoadServicePostgres userLoadServicePostgres;

  setUpAll(() async {
    connectionUtil = DbConnectionUtil(
      model: model,
      logModel: logModel,
      loadModel: loadModel,
    );

    await SetupTests.login();
    await DbConnectionUtil.getConnection();

    loadLogRepository = LoadLogRepository(
      model: model,
      loadModel: loadModel,
      logModel: logModel,
    );

    userLoadServiceSqlite = UserLoadServiceSqlite(
      dbModel: model,
      loadLogRepository: loadLogRepository,
      dbConnectionUtil: connectionUtil,
    );

    userLoadServicePostgres = UserLoadServicePostgres(
      dbModel: model,
      loadLogRepository: loadLogRepository,
      dbConnectionUtil: connectionUtil,
    );
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Download users and save into sqlite [PARTIAL LOAD]', () async {
    await db.delete(db.driftUserModel).go();

    await connectionUtil.partialPopulate();
    userLoadServiceSqlite.populate(connectionUtil.users);
    await userLoadServiceSqlite.saveData();
    userLoadServicePostgres.populate(connectionUtil.users);
    await userLoadServicePostgres.saveData();

    var result = await db.select(db.driftUserModel).get();

    expect(result.length, greaterThan(0));
  });

  test('Download users and save into sqlite [FULL LOAD]', () async {
    await db.delete(db.driftUserModel).go();
    await connectionUtil.fullLoad();
    userLoadServiceSqlite.populate(connectionUtil.users);
    await userLoadServiceSqlite.saveData();
    var result = await db.select(db.driftUserModel).get();
    expect(result.length, greaterThan(0));
  });

  test('Delete users [SQLITE]', () async {
    await connectionUtil.fullLoad();
    userLoadServiceSqlite.populateDeleted(connectionUtil.deletedUsers);
    await userLoadServiceSqlite.deleteData();

    userLoadServiceSqlite
        .populateDeleted(connectionUtil.users.map((e) => e.id!).toList());
    await userLoadServiceSqlite.deleteData();
  });

  test('Delete users [POSTGRES]', () async {
    await connectionUtil.fullLoad();
    userLoadServicePostgres.populateDeleted(connectionUtil.deletedUsers);
    await userLoadServicePostgres.deleteData();

    userLoadServicePostgres
        .populateDeleted(connectionUtil.users.map((e) => e.id!).toList());
    await userLoadServicePostgres.deleteData();
  });

  test("Don't duplicate users", () async {
    await db.delete(db.driftUserModel).go();
    await connectionUtil.fullLoad();
    userLoadServiceSqlite.populate(connectionUtil.users);
    await userLoadServiceSqlite.saveData();
    var result = await db.select(db.driftUserModel).get();
    var total = result.length;
    expect(result.length, greaterThan(0));

    userLoadServiceSqlite.populate(connectionUtil.users);
    await userLoadServiceSqlite.saveData();

    var newResult = await db.select(db.driftUserModel).get();
    expect(newResult.length, total);
  });
}
