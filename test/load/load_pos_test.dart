import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/load/postgres/pos_load_service_postgres.dart';
import 'package:menu_types/util/load/sqlite/pos_load_service_sqlite.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late DbConnectionUtil connectionUtil;
  DbModel model = DbModel(db: db);
  LogModel logModel = LogModel();
  LoadModel loadModel = LoadModel();
  late LoadLogRepository loadLogRepository;

  late POSLoadServicePostgres posLoadServicePostgres;
  late POSLoadServiceSqlite posLoadServiceSqlite;

  setUpAll(() async {
    connectionUtil = DbConnectionUtil(
      model: model,
      logModel: logModel,
      loadModel: loadModel,
    );

    await SetupTests.login();
    await DbConnectionUtil.getConnection();

    loadLogRepository = LoadLogRepository(
      model: model,
      loadModel: loadModel,
      logModel: logModel,
    );

    posLoadServicePostgres = POSLoadServicePostgres(
      dbConnectionUtil: connectionUtil,
      dbModel: model,
      loadLogRepository: loadLogRepository,
    );

    posLoadServiceSqlite = POSLoadServiceSqlite(
      dbConnectionUtil: connectionUtil,
      dbModel: model,
      loadLogRepository: loadLogRepository,
    );
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Download and save POS [POSTGRES]', () async {
    connectionUtil.pos.clear();

    expect(connectionUtil.pos.isEmpty, true);

    await connectionUtil.fullPopulate();

    expect(connectionUtil.pos.isEmpty, false);
    posLoadServicePostgres.populate(connectionUtil.pos);
    await posLoadServicePostgres.saveData();
  });

  test('Download and save POS [SQLITE]', () async {
    connectionUtil.pos.clear();
    expect(connectionUtil.pos.isEmpty, true);

    await connectionUtil.fullPopulate();

    expect(connectionUtil.pos.isEmpty, false);
    posLoadServiceSqlite.populate(connectionUtil.pos);
    await posLoadServiceSqlite.saveData();
  });

  test('Delete POS [POSTGRES]', () async {
    await connectionUtil.fullPopulate();
    posLoadServicePostgres.populateDeleted(connectionUtil.deletedPOS);
    await posLoadServicePostgres.deleteData();
  });

  test('Delete POS [SQLITE]', () async {
    await connectionUtil.fullPopulate();
    posLoadServiceSqlite.populateDeleted(connectionUtil.deletedPOS);
    await posLoadServiceSqlite.deleteData();
  });
}
