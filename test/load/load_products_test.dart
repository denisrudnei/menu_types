import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/configuration/db_model.dart';
import 'package:menu_types/models/load_model.dart';
import 'package:menu_types/models/log_model.dart';
import 'package:menu_types/repository/load_log_repository.dart';
import 'package:menu_types/util/db_connection_util.dart';
import 'package:menu_types/util/load/product_load_service.dart';
import 'package:menu_types/util/my_database.dart';
import 'package:menu_types/util/setup_tests.dart';

void main() {
  MyDatabase db = SetupTests.db;
  late DbConnectionUtil connectionUtil;
  DbModel model = DbModel(db: db);
  LogModel logModel = LogModel();
  LoadModel loadModel = LoadModel();
  late LoadLogRepository loadLogRepository;

  late ProductLoadService productLoadService;

  setUpAll(() async {
    connectionUtil = DbConnectionUtil(
      model: model,
      logModel: logModel,
      loadModel: loadModel,
    );

    await SetupTests.login();
    await DbConnectionUtil.getConnection();

    loadLogRepository = LoadLogRepository(
      model: model,
      loadModel: loadModel,
      logModel: logModel,
    );

    productLoadService = ProductLoadService(
      dbModel: model,
      loadLogRepository: loadLogRepository,
      dbConnectionUtil: connectionUtil,
    );
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Download products and save in database (sqlite) [FULL LOAD]', () async {
    model.db = db;
    loadModel.setLastSuccessfulUpdate(DateTime(DateTime.now().year - 1));

    await connectionUtil.fullPopulate();

    productLoadService.populate(connectionUtil.products);

    await productLoadService.insertProductsSqlite(connectionUtil.products);

    var result = await db.select(db.driftProductModel).get();

    expect(result.length, greaterThan(1500));
  });

  test('Download products and save in database (postgres) [FULL LOAD]',
      () async {
    model.db = db;
    loadModel.setLastSuccessfulUpdate(DateTime(2020));

    await connectionUtil.fullPopulate();
    productLoadService.populate(connectionUtil.products);
    await productLoadService.insertProductsPostgres(connectionUtil.products);
    String sql = 'select count(*) from product';
    var connection = await DbConnectionUtil.getConnection();
    await connection.open();
    var result = await connection.query(sql);

    expect(result.first[0], greaterThan(1500));
  });

  test('Download products and save in database (sqlite) [PARTIAL LOAD]',
      () async {
    await connectionUtil.partialPopulate();
    productLoadService.populate(connectionUtil.products);
    await productLoadService.insertProductsSqlite(connectionUtil.products);

    var result = await db.select(db.driftProductModel).get();

    expect(result.length, greaterThan(1500));
  });

  test('Download products and save in database (postgres) [PARTIAL LOAD]',
      () async {
    await connectionUtil.partialPopulate();
    productLoadService.populate(connectionUtil.products);
    await productLoadService.insertProductsPostgres(connectionUtil.products);

    String sql = 'select count(*) from product';
    var connection = await DbConnectionUtil.getConnection();
    await connection.open();
    var result = await connection.query(sql);

    expect(result.first[0], greaterThan(1500));
  });

  test('Delete products [POSTGRES]', () async {
    await connectionUtil.fullPopulate();
    productLoadService.populate(connectionUtil.products);
    await productLoadService
        .deleteProductsPostgres(connectionUtil.deletedProducts);
  });

  test('Delete products [SQLITE]', () async {
    await connectionUtil.fullPopulate();
    productLoadService.populate(connectionUtil.products);
    await productLoadService
        .deleteProductsSqlite(connectionUtil.deletedProducts);
  });
}
