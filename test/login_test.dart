import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/auth_model.dart';
import 'package:menu_types/models/user.dart';
import 'package:menu_types/repository/auth_repository.dart';
import 'package:menu_types/util/setup_tests.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  setUp(() async {
    var prefs = await SharedPreferences.getInstance();
    authRepository = AuthRepository(model: AuthModel());
    prefs.clear();
  });

  test('Must save to be able to login offline', () async {
    User user = User(
      name: 'user',
      email: 'user@mail.com',
      image: '',
      password: 'test',
    );

    await authRepository.saveLogin(user.email, user.password);

    var logged = await authRepository.offlineLogin(user.email, user.password);

    expect(logged, true);
  });

  test('Should not allow login', () async {
    var loggedForNonSavedUser = await authRepository.offlineLogin(
      'not saved',
      'not saved',
    );

    expect(loggedForNonSavedUser, false);
  });

  test('Must not login if password is incorrect', () async {
    await authRepository.saveLogin('username', 'password');
    var logged = await authRepository.offlineLogin(
      'username',
      'incorrect password',
    );

    expect(logged, false);
  });

  test('must allow saving more than one login', () async {
    await authRepository.saveLogin('username', 'password');
    await authRepository.saveLogin('username2', 'password2');

    var logged = await authRepository.offlineLogin(
      'username',
      'password',
    );

    expect(logged, true);

    var logged2 = await authRepository.offlineLogin('username2', 'password2');

    expect(logged2, true);
  });
}
