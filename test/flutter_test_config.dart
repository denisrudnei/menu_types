import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:logger/logger.dart';
import 'package:menu_types/util/setup_tests.dart';

Future<void> testExecutable(FutureOr<void> Function() testMain) async {
  setUpAll(() async {
    Logger.level = Level.error;
    await SetupTests.bootstrapTests();
  });

  tearDownAll(() async {
    await SetupTests.db.close();
  });

  await testMain();
}
