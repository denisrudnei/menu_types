import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/controllers/backup_controller.dart';
import 'package:menu_types/models/printing/printer_model.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/repository/device_info.dart';
import 'package:menu_types/repository/pos_repository.dart';
import 'package:menu_types/repository/printer_repository.dart';
import 'package:menu_types/repository/product_repository.dart';
import 'package:menu_types/repository/user_repository.dart';
import 'package:menu_types/util/my_database.dart';

void main() {
  late BackupController controller;

  setUp(() {
    MyDatabase db = MyDatabase.test(NativeDatabase.memory());
    PrinterRepository printerRepository = PrinterRepository(
      db: db,
      model: PrinterModel(),
    );
    POSRepository posRepository = POSRepository(
      db: db,
      deviceInfo: DeviceInfo(),
    );
    UserRepository userRepository = UserRepository(
      db: db,
    );

    ProductRepository productRepository = ProductRepository(
      db: db,
      purchasesModel: PurchasesModel(),
      model: ProductsModel(),
    );

    controller = BackupController(
      printerRepository: printerRepository,
      posRepository: posRepository,
      userRepository: userRepository,
      productRepository: productRepository,
    );
  });

  test('Should create a backup file', () async {
    await controller.backup();
  });
}
