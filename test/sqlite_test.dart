import 'package:drift/native.dart';
import 'package:flutter/material.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:menu_types/models/products_model.dart';
import 'package:menu_types/models/purchases_model.dart';
import 'package:menu_types/repository/db/sqlite/products_repository_sqlite.dart';
import 'package:menu_types/util/my_database.dart';

Future main() async {
  late MyDatabase db;
  late PurchasesModel purchasesModel;
  late ProductsModel model;

  setUpAll(() async {
    WidgetsFlutterBinding.ensureInitialized();
    db = MyDatabase.test(NativeDatabase.memory());
    model = ProductsModel();
    purchasesModel = PurchasesModel();
  });

  tearDownAll(() async {
    await db.close();
  });

  test('Db test', () async {
    await db.delete(db.driftProductModel).go();

    var empty = await db.select(db.driftProductModel).get();

    expect(empty.length, 0);

    db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'test'));
    var result = await db.select(db.driftProductModel).get();

    expect(result.length, 1);
  });

  test('Query for product', () async {
    var repository = ProductsRepositorySqlite(
      db: db,
      purchasesModel: purchasesModel,
      model: model,
    );
    await db.delete(db.driftProductModel).go();

    await repository.searchProductsByName('test');

    expect(model.searchedProducts.length, 0);

    await db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'test'));

    await db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'product 2'));

    await repository.searchProductsByName('test');

    expect(model.searchedProducts.length, 1);
    expect(model.pages, 1);
    expect(model.hasNextPage, false);

    await repository.searchProductsByName('non existing product');

    expect(model.searchedProducts.length, 0);

    await repository.searchProductsByName('2');

    expect(model.searchedProducts.length, 1);
  });

  test('More pages', () async {
    var repository = ProductsRepositorySqlite(
      db: db,
      purchasesModel: purchasesModel,
      model: model,
    );
    await db.delete(db.driftProductModel).go();
    await db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'product 1'));

    await db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'product 2'));

    await db
        .into(db.driftProductModel)
        .insert(DriftProductModelCompanion.insert(name: 'product 3'));
    await repository.searchProductsByName('', limit: 1);

    expect(model.pages, greaterThan(2));

    await repository.searchProductsByName(
      '',
      page: 2,
      limit: 1,
    );

    expect(model.searchedProducts.length, greaterThanOrEqualTo(1));
  });
}
