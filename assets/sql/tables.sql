CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public."order" (
	id serial4 NOT NULL,
	CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.pos (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	name varchar NOT NULL,
	hostname varchar NOT NULL DEFAULT ''::character varying,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	CONSTRAINT "PK_bd71782fccea4003b8088d4b373" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.print_layout (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	name varchar NOT NULL,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	CONSTRAINT "PK_9dcec3c295fa655e57300082bff" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.single_line_item_layout (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL,
	CONSTRAINT "PK_902e9391991b15b0d6e98abcd45" PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS "IDX_107093e1449a93009135ebfb87" ON public.single_line_item_layout USING btree (type);

CREATE TABLE IF NOT EXISTS public.site_settings (
	id serial4 NOT NULL,
	"adSense" bool NOT NULL DEFAULT false,
	logo varchar NOT NULL DEFAULT '/images/not-set.svg'::character varying,
	name varchar NOT NULL DEFAULT 'Shop'::character varying,
	currency varchar NOT NULL DEFAULT 'USD'::character varying,
	locale varchar NOT NULL DEFAULT ''::character varying,
	"isDark" bool NOT NULL DEFAULT true,
	"darkPrimary" varchar NOT NULL DEFAULT '#1976D2'::character varying,
	"darkSecondary" varchar NOT NULL DEFAULT '#FF8F00'::character varying,
	"darkAccent" varchar NOT NULL DEFAULT '#424242'::character varying,
	"lightPrimary" varchar NOT NULL DEFAULT '#1976D2'::character varying,
	"lightSecondary" varchar NOT NULL DEFAULT '#424242'::character varying,
	"lightAccent" varchar NOT NULL DEFAULT '#82B1FF'::character varying,
	address varchar NOT NULL DEFAULT ''::character varying,
	cnpj varchar NOT NULL DEFAULT '00.000.000/0000-00'::character varying,
	CONSTRAINT "PK_e4290e8371a166d7e066d131f6e" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.social_networks (
	id serial4 NOT NULL,
	facebook varchar NULL,
	youtube varchar NULL,
	twitter varchar NULL,
	discord varchar NULL,
	CONSTRAINT "PK_973974c10fd4f3f1625c24178cc" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.synchronization_result (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	CONSTRAINT "PK_9801e4ebd2ada73f73f676175ce" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.typeorm_metadata (
	"type" varchar NOT NULL,
	"database" varchar NULL,
	"schema" varchar NULL,
	"table" varchar NULL,
	name varchar NULL,
	value text NULL
);

CREATE TABLE IF NOT EXISTS public."user" (
	id serial4 NOT NULL,
	email varchar NOT NULL,
	name varchar NOT NULL,
	image varchar NOT NULL DEFAULT '/images/profile.jpg'::character varying,
	active bool NOT NULL DEFAULT false,
	password varchar NOT NULL,
	"tempPassword" varchar NOT NULL,
	"role" varchar NOT NULL DEFAULT 'USER'::character varying,
	"darkTheme" bool NOT NULL DEFAULT true,
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id),
	CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS public.address (
	id serial4 NOT NULL,
	zip_code varchar NOT NULL DEFAULT ''::character varying,
	country varchar NOT NULL DEFAULT ''::character varying,
	city varchar NOT NULL DEFAULT ''::character varying,
	street varchar NOT NULL DEFAULT ''::character varying,
	"number" varchar NOT NULL DEFAULT ''::character varying,
	district varchar NOT NULL DEFAULT ''::character varying,
	state varchar NOT NULL DEFAULT ''::character varying,
	"userId" int4 NULL,
	CONSTRAINT "PK_d92de1f82754668b5f5f5dd4fd5" PRIMARY KEY (id),
	CONSTRAINT "FK_d25f1ea79e282cc8a42bd616aa3" FOREIGN KEY ("userId") REFERENCES public."user"(id)
);

CREATE TABLE IF NOT EXISTS public.category (
	id serial4 NOT NULL,
	name varchar NOT NULL,
	description text NOT NULL DEFAULT ''::text,
	slug varchar NOT NULL,
	image varchar NOT NULL DEFAULT '/images/not-set.svg'::character varying,
	"fatherId" int4 NULL,
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	"productsTypes" _varchar NOT NULL DEFAULT '{ECOMMERCE}'::character varying[],
	CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY (id),
	CONSTRAINT "UQ_23c05c292c439d77b0de816b500" UNIQUE (name),
	CONSTRAINT "FK_01279634d048402041313def384" FOREIGN KEY ("fatherId") REFERENCES public.category(id)
);

CREATE TABLE IF NOT EXISTS public.empty_line_item (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL DEFAULT 'FILL_THE_LINE'::character varying,
	"numberOfLines" int4 NOT NULL DEFAULT 1,
	"mainLayoutId" uuid NULL,
	CONSTRAINT "PK_03807feec6d9276eeb2e8f28b0d" PRIMARY KEY (id),
	CONSTRAINT "FK_0f0336deccc3f502c38da9a865e" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.line_item (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"character" varchar NOT NULL,
	"mainLayoutId" uuid NULL,
	"type" varchar NOT NULL,
	CONSTRAINT "PK_cce6b13e67fa506d1d9618ac68b" PRIMARY KEY (id),
	CONSTRAINT "FK_ceeab1f44a8175060e697070011" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.load_log (
	message varchar NOT NULL,
	"posId" uuid NULL,
	"type" varchar NOT NULL DEFAULT 'INFO'::character varying,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	CONSTRAINT "PK_bfa18bdbbe65f8e2082d83080be" PRIMARY KEY (id),
	CONSTRAINT "FK_3773f6b16e5d6764d400bee8b07" FOREIGN KEY ("posId") REFERENCES public.pos(id)
);

CREATE TABLE IF NOT EXISTS public.notification (
	id serial4 NOT NULL,
	"content" text NOT NULL,
	origin varchar NOT NULL DEFAULT 'SYSTEM'::character varying,
	"date" timestamp NOT NULL DEFAULT now(),
	"read" bool NOT NULL DEFAULT false,
	"userId" int4 NULL,
	CONSTRAINT "PK_705b6c7cdf9b2c2ff7ac7872cb7" PRIMARY KEY (id),
	CONSTRAINT "FK_1ced25315eb974b73391fb1c81b" FOREIGN KEY ("userId") REFERENCES public."user"(id)
);

CREATE TABLE IF NOT EXISTS public.payment_info_item (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL DEFAULT 'FILL_THE_LINE'::character varying,
	"mainLayoutId" uuid NULL,
	CONSTRAINT "PK_7f74b1400cb4e3dd00b1c077c67" PRIMARY KEY (id),
	CONSTRAINT "FK_2f75bb975e9a7800bc13b97ca42" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.phone (
	id serial4 NOT NULL,
	description varchar NOT NULL,
	"number" varchar NOT NULL,
	"userId" int4 NULL,
	CONSTRAINT "PK_f35e6ee6c1232ce6462505c2b25" PRIMARY KEY (id),
	CONSTRAINT "FK_260d7031e6bd9ed4fbcd2dd3ad6" FOREIGN KEY ("userId") REFERENCES public."user"(id)
);

CREATE TABLE IF NOT EXISTS public.print_layout_item (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL,
	"numberOfLines" int4 NULL DEFAULT 1,
	"storeName" bool NULL,
	address bool NULL,
	cnpj bool NULL,
	"character" varchar NULL,
	"printType" varchar NULL DEFAULT 'NORMAL'::character varying,
	"mainLayoutId" uuid NULL,
	"position" int4 NOT NULL DEFAULT 1,
	CONSTRAINT "PK_1f81404eebfef30e427462c3e67" PRIMARY KEY (id),
	CONSTRAINT "FK_d6246b5bafd5a1d8075286a4477" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE INDEX IF NOT EXISTS "IDX_90dd2f7bbe0b2ea1875c757166" ON public.print_layout_item USING btree (type);

CREATE TABLE IF NOT EXISTS public.printer (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	manufacturer varchar NOT NULL,
	model varchar NOT NULL,
	"path" varchar NOT NULL,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	"installedInId" uuid NULL,
	name varchar NOT NULL,
	"type" varchar NOT NULL DEFAULT 'THERMAL'::character varying,
	CONSTRAINT "PK_a07d4f7686a51f38ae237def52b" PRIMARY KEY (id),
	CONSTRAINT "FK_0c21d2dbdc80f57b0130ef2976f" FOREIGN KEY ("installedInId") REFERENCES public.pos(id)
);

CREATE TABLE IF NOT EXISTS public.product (
	name varchar NOT NULL,
	description text NOT NULL,
	amount numeric NOT NULL DEFAULT '0'::numeric,
	images text NOT NULL DEFAULT ''::text,
	price numeric NOT NULL,
	slug varchar NOT NULL DEFAULT ''::character varying,
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	"deletedAt" timestamp NULL,
	"categoryId" int4 NULL,
	"imageUpdatedAt" timestamp NULL,
	"type" _varchar NOT NULL DEFAULT '{ECOMMERCE}'::character varying[],
	barcode varchar NOT NULL DEFAULT ''::character varying,
	id bigserial NOT NULL,
	synchronized bool NOT NULL DEFAULT true,
	CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY (id),
	CONSTRAINT "FK_ff0c0301a95e517153df97f6812" FOREIGN KEY ("categoryId") REFERENCES public.category(id)
);

CREATE TABLE IF NOT EXISTS public.product_table (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"printType" varchar NOT NULL DEFAULT 'NORMAL'::character varying,
	"mainLayoutId" uuid NULL,
	"type" varchar NOT NULL,
	CONSTRAINT "PK_20953c75b0440a27fea68c10e9a" PRIMARY KEY (id),
	CONSTRAINT "FK_e5b6c31e3cf86e1883b451ffee0" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.purchase_information (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL,
	"mainLayoutId" uuid NULL,
	"position" int4 NOT NULL DEFAULT 1,
	CONSTRAINT "PK_d9a4e41b7a98e32b74ae2e677f9" PRIMARY KEY (id),
	CONSTRAINT "FK_488fddd60f44b8ae6a640748868" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.synchronization_item_result (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	status varchar NOT NULL,
	"type" varchar NOT NULL,
	reason varchar NOT NULL,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	"synchronizationId" uuid NULL,
	"itemId" varchar NOT NULL,
	CONSTRAINT "PK_44af8d36d7f35d0b6afc144ddf9" PRIMARY KEY (id),
	CONSTRAINT "FK_071ff51f99325ed07042e2814e7" FOREIGN KEY ("synchronizationId") REFERENCES public.synchronization_result(id)
);

CREATE TABLE IF NOT EXISTS public.text_item (
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
	"type" varchar NOT NULL,
	"mainLayoutId" uuid NULL,
	"position" int4 NOT NULL DEFAULT 1,
	"text" varchar NOT NULL,
	CONSTRAINT "PK_611a20e3f26ad021f6ff4b5b07e" PRIMARY KEY (id),
	CONSTRAINT "FK_8723a3b1acd261c018f80905e2f" FOREIGN KEY ("mainLayoutId") REFERENCES public.print_layout(id)
);

CREATE TABLE IF NOT EXISTS public.establishment_table (
	id serial4 NOT NULL,
	name varchar NOT NULL,
	"inUse" bool NOT NULL DEFAULT false,
	"activeOrderId" int4 NULL,
	"deletedAt" timestamp NULL,
	CONSTRAINT "PK_cf358ea7997eb69deb59f20d5de" PRIMARY KEY (id),
	CONSTRAINT "UQ_4c865ab5a6f040c07d9a067cb72" UNIQUE ("activeOrderId")
);

CREATE TABLE IF NOT EXISTS public.history_product (
	id serial4 NOT NULL,
	"productId" int4 NOT NULL,
	"purchaseId" int4 NULL,
	"categoryId" int4 NULL,
	"dataName" varchar NOT NULL,
	"dataDescription" text NOT NULL,
	"dataAmount" numeric NOT NULL DEFAULT '0'::numeric,
	"dataImages" text NOT NULL DEFAULT ''::text,
	"dataPrice" numeric NOT NULL,
	"dataSlug" varchar NOT NULL DEFAULT ''::character varying,
	"dataUpdatedat" timestamp NOT NULL DEFAULT now(),
	"dataDeletedat" timestamp NULL,
	"dataImageupdatedat" timestamp NULL,
	"dataType" _varchar NOT NULL DEFAULT '{ECOMMERCE}'::character varying[],
	"dataBarcode" varchar NOT NULL DEFAULT ''::character varying,
	"dataId" bigserial NOT NULL,
	"dataSynchronized" bool NOT NULL DEFAULT true,
	CONSTRAINT "PK_1ca76d2ee38187aa5d1bd12a6b2" PRIMARY KEY (id, "dataId")
);

CREATE TABLE IF NOT EXISTS public.payment (
	id serial4 NOT NULL,
	"type" varchar NOT NULL DEFAULT 'MONEY'::character varying,
	value numeric NOT NULL,
	paid numeric NOT NULL,
	"change" numeric NOT NULL,
	"purchaseId" int4 NULL,
	CONSTRAINT "PK_fcaec7df5adf9cac408c686b2ab" PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.purchase (
	id serial4 NOT NULL,
	"createdAt" timestamp NOT NULL DEFAULT now(),
	"userId" int4 NULL,
	"type" varchar NOT NULL DEFAULT 'NORMAL'::character varying,
	status varchar NOT NULL DEFAULT 'REQUIRED'::character varying,
	"establishmentTableId" int4 NULL,
	"operatorId" int4 NULL,
	"posId" uuid NULL,
	origin varchar NOT NULL DEFAULT 'ECOMMERCE'::character varying,
	CONSTRAINT "PK_86cc2ebeb9e17fc9c0774b05f69" PRIMARY KEY (id)
);

ALTER TABLE public.establishment_table DROP CONSTRAINT IF EXISTS "FK_4c865ab5a6f040c07d9a067cb72";
ALTER TABLE public.establishment_table ADD CONSTRAINT "FK_4c865ab5a6f040c07d9a067cb72" FOREIGN KEY ("activeOrderId") REFERENCES public.purchase(id);

ALTER TABLE public.history_product DROP CONSTRAINT IF EXISTS "FK_5dd679b1fd54284cd9c217c5cfe";
ALTER TABLE public.history_product ADD CONSTRAINT "FK_5dd679b1fd54284cd9c217c5cfe" FOREIGN KEY ("categoryId") REFERENCES public.category(id);

ALTER TABLE public.history_product DROP CONSTRAINT IF EXISTS "FK_cfb68985a330db8e4f02c599809";
ALTER TABLE public.history_product ADD CONSTRAINT "FK_cfb68985a330db8e4f02c599809" FOREIGN KEY ("purchaseId") REFERENCES public.purchase(id);

ALTER TABLE public.payment DROP CONSTRAINT IF EXISTS "FK_885283e0709e1a736ecca80cf3c";
ALTER TABLE public.payment ADD CONSTRAINT "FK_885283e0709e1a736ecca80cf3c" FOREIGN KEY ("purchaseId") REFERENCES public.purchase(id);

ALTER TABLE public.purchase DROP CONSTRAINT IF EXISTS "FK_0f7f68f4ee35535a074bb7c50ef";
ALTER TABLE public.purchase ADD CONSTRAINT "FK_0f7f68f4ee35535a074bb7c50ef" FOREIGN KEY ("operatorId") REFERENCES public."user"(id);

ALTER TABLE public.purchase DROP CONSTRAINT IF EXISTS "FK_33520b6c46e1b3971c0a649d38b";
ALTER TABLE public.purchase ADD CONSTRAINT "FK_33520b6c46e1b3971c0a649d38b" FOREIGN KEY ("userId") REFERENCES public."user"(id);

ALTER TABLE public.purchase DROP CONSTRAINT IF EXISTS "FK_63754c69cf0affd431d1a9c9acd";
ALTER TABLE public.purchase ADD CONSTRAINT "FK_63754c69cf0affd431d1a9c9acd" FOREIGN KEY ("establishmentTableId") REFERENCES public.establishment_table(id);

ALTER TABLE public.purchase DROP CONSTRAINT IF EXISTS "FK_fd2d6cc60b623ba14b206a2fb1b";
ALTER TABLE public.purchase ADD CONSTRAINT "FK_fd2d6cc60b623ba14b206a2fb1b" FOREIGN KEY ("posId") REFERENCES public.pos(id);

ALTER TABLE public.printer ADD COLUMN IF NOT EXISTS "type" varchar NOT NULL DEFAULT 'THERMAL'::character varying